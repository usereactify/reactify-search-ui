# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.1.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v6.0.0...release-v6.1.0) (2023-09-13)


### Features

* move results/resultStats data out of context and into custom events ([734bddb](https://bitbucket.org/usereactify/reactify-search-ui/commit/734bddb9baf778ef6620f00c5c177e1201642328))

## [6.0.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.32.0...release-v6.0.0) (2023-09-12)


### Features

* use endpoint from config ([f41806d](https://bitbucket.org/usereactify/reactify-search-ui/commit/f41806ded0ce06e09e0f856aa038ce32bf509948))

## [5.32.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.31.0...release-v5.32.0) (2023-09-07)


### Features

* add setFilterValue method to useFilters hook ([db1a701](https://bitbucket.org/usereactify/reactify-search-ui/commit/db1a7014665979a4fbc66d4cbfc3d7bc098fb0f4))

## [5.31.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.30.0...release-v5.31.0) (2023-09-04)


### Features

* add support for includeFields and excludeFields api ([9ee6251](https://bitbucket.org/usereactify/reactify-search-ui/commit/9ee625142759ea316e7f54f44e8a8051d31679b0))

## [5.30.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.29.2...release-v5.30.0) (2023-08-25)

## [5.30.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.28.0-beta.0...beta-v5.30.0-beta.0) (2023-08-25)


### Features

* add loading field to useResults ([3441e12](https://bitbucket.org/usereactify/reactify-search-ui/commit/3441e128198720b0eb27ca332b5e3b98af21d2fb))

### [5.29.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.29.1...release-v5.29.2) (2023-08-17)

### [5.29.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.29.0...release-v5.29.1) (2023-08-17)

## [5.29.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.28.0...release-v5.29.0) (2023-08-16)


### Features

* add loading field to useResults ([3441e12](https://bitbucket.org/usereactify/reactify-search-ui/commit/3441e128198720b0eb27ca332b5e3b98af21d2fb))

## [5.28.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.27.1...release-v5.28.0) (2023-08-16)


### Bug Fixes

* add parent doc count into nested aggs ([c324cf8](https://bitbucket.org/usereactify/reactify-search-ui/commit/c324cf831842dcb16a9a89d1a0b7ef962f45afa0))

## [5.28.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.12.0-beta.0...beta-v5.28.0-beta.0) (2023-08-15)


### Features

* add clientId prop to ReactifySearchProvider, add telemetry ([4090696](https://bitbucket.org/usereactify/reactify-search-ui/commit/4090696a5e5d547b961fe9c4feea9c79be3580bc))
* add filter value sorting for bedding sizes ([3bcf4b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/3bcf4b0d8b1da312c586e3ac99482bdbe4ced28b))
* add flags for synonyms ([cfbc94a](https://bitbucket.org/usereactify/reactify-search-ui/commit/cfbc94ab16cb7a91e570134e7033c1dca8874e71))
* add flags system to toggle sensors and features ([9f08881](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f0888196b56a12cfdb423e43cd493fb6a9c8d11))
* add Suggestions component ([21514bb](https://bitbucket.org/usereactify/reactify-search-ui/commit/21514bb7d643647b1ac2a19b57ec649e5a0684b3))
* add support for controlling flags via provider ([0b5d97e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0b5d97e87e67e04d0be6acca335bc1e16d347d98))
* add support for separate global search and global collection curations ([7ff7fbd](https://bitbucket.org/usereactify/reactify-search-ui/commit/7ff7fbd18abda4588429ddd9b02b26ddac12985e))
* add support for settingsFilterLogic ([4c3b00f](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c3b00f18cb216a4c454e0cf89e2520562973a13))
* add useIndices helper and support index override via url param ([cfd73ae](https://bitbucket.org/usereactify/reactify-search-ui/commit/cfd73ae45c374dadf90c27f5257e59c3a0e5168f))
* allow custom filter sort order ([e7d9b0b](https://bitbucket.org/usereactify/reactify-search-ui/commit/e7d9b0bed3b8df5cf47a12968c915b9c2e492e2e))
* allow removing one or more (not just all) filters within SelectedFilters component ([4c486a4](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c486a4d6adf64b39776cda8d074c92d5dc92f44))
* hide slider when range between min and max is 0 ([2e45a94](https://bitbucket.org/usereactify/reactify-search-ui/commit/2e45a9490642b4c996ea3de76e362f4a35c5405c))
* implement ga-gtag ([ff0bcb2](https://bitbucket.org/usereactify/reactify-search-ui/commit/ff0bcb2d8490f6a428769c21b8f9472207c8792f))
* improve elasticsearch types and add related field ([dd88e8a](https://bitbucket.org/usereactify/reactify-search-ui/commit/dd88e8a3ee5e1dce990495aa03b8fe814e35d6c5))
* improve flags logic and expose setFlag helper ([60b5c33](https://bitbucket.org/usereactify/reactify-search-ui/commit/60b5c336000dcd8c066d9e0af9667642c6dbe8cf))
* remove javascript cache ttl behaviour for config and rely on rs cache option and browser cache-control ([730a9c5](https://bitbucket.org/usereactify/reactify-search-ui/commit/730a9c541e57a7336015c5d93b7b23371cf64bef))
* remove option for noreactivebase ([a2df18e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a2df18e067322367b29b9a054c2774e7b44bf7c8))


### Bug Fixes

* add dependencies back into sort memo ([cd5f039](https://bitbucket.org/usereactify/reactify-search-ui/commit/cd5f0395e81c826ce2c821596b88298670f3caab))
* add inventory sensor to instant search, disable callouts for instant search ([1cbf550](https://bitbucket.org/usereactify/reactify-search-ui/commit/1cbf550b83b59aea8df2f081e3027403b8451a6c))
* add parent doc count into nested aggs ([c324cf8](https://bitbucket.org/usereactify/reactify-search-ui/commit/c324cf831842dcb16a9a89d1a0b7ef962f45afa0))
* allow boosted sorting/grouping within all modes ([d276060](https://bitbucket.org/usereactify/reactify-search-ui/commit/d276060096359a1563c64da43e0a2f6d31192aad))
* bump lockfile ([949edd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/949edd710fa1bfba38f2fbe96091da74589af9bc))
* check if result count is zero rather than falsey ([7d01cef](https://bitbucket.org/usereactify/reactify-search-ui/commit/7d01cef98f5e42439b764ce46aeca2a3b7efd9b0))
* check window exists before initializing ga ([19d0f48](https://bitbucket.org/usereactify/reactify-search-ui/commit/19d0f485374f78a9c567cc130e9ec289fe59ae46))
* clear range filter values when values are cleared within underlying reactivesearch component ([720917f](https://bitbucket.org/usereactify/reactify-search-ui/commit/720917f6e3b2c017cfd05a1d2bacf1692212f37e))
* correct sort option ordering ([c48ecdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/c48ecdd99166259966c97616a64ec0950e64172f))
* disable sentry release step ([9e7cce9](https://bitbucket.org/usereactify/reactify-search-ui/commit/9e7cce9732661d4c32ca2d3a9ca70d1e7588ccd1))
* exclude selectable sort option from instant search logic ([988427d](https://bitbucket.org/usereactify/reactify-search-ui/commit/988427d72ee6525cf20c4a3f8a900d3bf9db398a))
* hide results in search modes when search term is empty ([1d600fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/1d600fb44c1880255f11bbd3d04cb86c7db164ca))
* improve support for removing slider filter values ([4b5f05c](https://bitbucket.org/usereactify/reactify-search-ui/commit/4b5f05c90b8220a31e8096d80fc4e35016ab7153))
* improve types for range values ([597a6f3](https://bitbucket.org/usereactify/reactify-search-ui/commit/597a6f35dd069b5b7f18f6b248049c319981774a))
* misc fixes to types and values for FiltersSelected component ([35619de](https://bitbucket.org/usereactify/reactify-search-ui/commit/35619de4d25eafe105753fe7a54caaef8452be88))
* move gtag initializer into provider and include relevant user properties ([72ca81e](https://bitbucket.org/usereactify/reactify-search-ui/commit/72ca81e5556f07f1c7fd4a0c630698ed4e70f6fa))
* only apply global boosting rules in collection mode ([731009c](https://bitbucket.org/usereactify/reactify-search-ui/commit/731009c9d6df2dee80d30bbef678b7c7a68bdd55))
* propagate changes to slider filter values throughout components ([05d612d](https://bitbucket.org/usereactify/reactify-search-ui/commit/05d612d43f69bbd798e95ceaec91bf1a7450894d))
* properly encode search query when submitting ([3ecc44f](https://bitbucket.org/usereactify/reactify-search-ui/commit/3ecc44fd72ebe4ea3bc919bf1d823b0a49bc0830))
* remove duplicate sensors from storybook ([c41aab2](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41aab22be3dcb16ea0fe31ede1a232b28f6ced3))
* sort by collection position after curation pin and boost sorting rather than before ([593ee9d](https://bitbucket.org/usereactify/reactify-search-ui/commit/593ee9d2b9e46726a99e20e1e7a9782bf0bc4590))
* trim whitespace around search terms for tracking within analytics ([257d99a](https://bitbucket.org/usereactify/reactify-search-ui/commit/257d99aa916210d120c0a17e670d34efcd1979f6))
* unset slider and multi range filters when using ClearAll component ([185a995](https://bitbucket.org/usereactify/reactify-search-ui/commit/185a9954bbc9e3ea367584bb4a6b5bb42da95775))
* use filter group when mode is instant-search ([0d5cb86](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d5cb86766257387060b4c7515b660908464906f))
* use shared ga cookie ([b406fe1](https://bitbucket.org/usereactify/reactify-search-ui/commit/b406fe11a120326bb90d0452dc1a23c0c8643b9a))

### [5.27.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.27.0...release-v5.27.1) (2023-08-14)


### Bug Fixes

* add dependencies back into sort memo ([cd5f039](https://bitbucket.org/usereactify/reactify-search-ui/commit/cd5f0395e81c826ce2c821596b88298670f3caab))

## [5.27.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.26.5...release-v5.27.0) (2023-08-10)


### Features

* add support for controlling flags via provider ([0b5d97e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0b5d97e87e67e04d0be6acca335bc1e16d347d98))

### [5.26.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.26.4...release-v5.26.5) (2023-08-09)


### Bug Fixes

* bump lockfile ([949edd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/949edd710fa1bfba38f2fbe96091da74589af9bc))

### [5.26.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.26.3...release-v5.26.4) (2023-08-09)


### Bug Fixes

* add inventory sensor to instant search, disable callouts for instant search ([1cbf550](https://bitbucket.org/usereactify/reactify-search-ui/commit/1cbf550b83b59aea8df2f081e3027403b8451a6c))

### [5.26.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.26.2...release-v5.26.3) (2023-08-09)


### Bug Fixes

* use filter group when mode is instant-search ([0d5cb86](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d5cb86766257387060b4c7515b660908464906f))

### [5.26.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.26.1...release-v5.26.2) (2023-08-09)


### Bug Fixes

* correct sort option ordering ([c48ecdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/c48ecdd99166259966c97616a64ec0950e64172f))

## [5.26.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.25.0...release-v5.26.0) (2023-08-08)


### Features

* add flags for synonyms ([cfbc94a](https://bitbucket.org/usereactify/reactify-search-ui/commit/cfbc94ab16cb7a91e570134e7033c1dca8874e71))

## [5.25.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.24.0...release-v5.25.0) (2023-08-02)


### Features

* add useIndices helper and support index override via url param ([cfd73ae](https://bitbucket.org/usereactify/reactify-search-ui/commit/cfd73ae45c374dadf90c27f5257e59c3a0e5168f))

## [5.24.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.23.0...release-v5.24.0) (2023-07-30)


### Features

* improve flags logic and expose setFlag helper ([60b5c33](https://bitbucket.org/usereactify/reactify-search-ui/commit/60b5c336000dcd8c066d9e0af9667642c6dbe8cf))

## [5.23.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.22.1...release-v5.23.0) (2023-07-30)


### Features

* add flags system to toggle sensors and features ([9f08881](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f0888196b56a12cfdb423e43cd493fb6a9c8d11))

### [5.22.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.22.0...release-v5.22.1) (2023-06-27)


### Bug Fixes

* check if result count is zero rather than falsey ([7d01cef](https://bitbucket.org/usereactify/reactify-search-ui/commit/7d01cef98f5e42439b764ce46aeca2a3b7efd9b0))

## [5.22.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.21.1...release-v5.22.0) (2023-05-31)


### Features

* hide slider when range between min and max is 0 ([2e45a94](https://bitbucket.org/usereactify/reactify-search-ui/commit/2e45a9490642b4c996ea3de76e362f4a35c5405c))


### Bug Fixes

* unset slider and multi range filters when using ClearAll component ([185a995](https://bitbucket.org/usereactify/reactify-search-ui/commit/185a9954bbc9e3ea367584bb4a6b5bb42da95775))

### [5.21.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.21.0...release-v5.21.1) (2023-05-15)


### Bug Fixes

* disable sentry release step ([9e7cce9](https://bitbucket.org/usereactify/reactify-search-ui/commit/9e7cce9732661d4c32ca2d3a9ca70d1e7588ccd1))
* propagate changes to slider filter values throughout components ([05d612d](https://bitbucket.org/usereactify/reactify-search-ui/commit/05d612d43f69bbd798e95ceaec91bf1a7450894d))

## [5.21.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.20.4...release-v5.21.0) (2023-05-12)


### Features

* add clientId prop to ReactifySearchProvider, add telemetry ([4090696](https://bitbucket.org/usereactify/reactify-search-ui/commit/4090696a5e5d547b961fe9c4feea9c79be3580bc))
* remove option for noreactivebase ([a2df18e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a2df18e067322367b29b9a054c2774e7b44bf7c8))

### [5.20.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.20.3...release-v5.20.4) (2023-05-09)


### Bug Fixes

* improve support for removing slider filter values ([4b5f05c](https://bitbucket.org/usereactify/reactify-search-ui/commit/4b5f05c90b8220a31e8096d80fc4e35016ab7153))

### [5.20.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.20.2...release-v5.20.3) (2023-05-09)

### [5.20.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.20.1...release-v5.20.2) (2023-05-09)


### Bug Fixes

* misc fixes to types and values for FiltersSelected component ([35619de](https://bitbucket.org/usereactify/reactify-search-ui/commit/35619de4d25eafe105753fe7a54caaef8452be88))

### [5.20.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.20.0...release-v5.20.1) (2023-05-09)


### Bug Fixes

* improve types for range values ([597a6f3](https://bitbucket.org/usereactify/reactify-search-ui/commit/597a6f35dd069b5b7f18f6b248049c319981774a))

## [5.20.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.19.3...release-v5.20.0) (2023-05-09)


### Features

* allow removing one or more (not just all) filters within SelectedFilters component ([4c486a4](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c486a4d6adf64b39776cda8d074c92d5dc92f44))

### [5.19.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.19.2...release-v5.19.3) (2023-05-07)


### Bug Fixes

* allow boosted sorting/grouping within all modes ([d276060](https://bitbucket.org/usereactify/reactify-search-ui/commit/d276060096359a1563c64da43e0a2f6d31192aad))

### [5.19.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.19.1...release-v5.19.2) (2023-05-05)


### Bug Fixes

* hide results in search modes when search term is empty ([1d600fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/1d600fb44c1880255f11bbd3d04cb86c7db164ca))

### [5.19.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.19.0...release-v5.19.1) (2023-05-01)

## [5.19.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.18.0...release-v5.19.0) (2023-05-01)


### Features

* add support for separate global search and global collection curations ([7ff7fbd](https://bitbucket.org/usereactify/reactify-search-ui/commit/7ff7fbd18abda4588429ddd9b02b26ddac12985e))

## [5.18.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.17.2...release-v5.18.0) (2023-03-17)


### Features

* add Suggestions component ([21514bb](https://bitbucket.org/usereactify/reactify-search-ui/commit/21514bb7d643647b1ac2a19b57ec649e5a0684b3))
* improve elasticsearch types and add related field ([dd88e8a](https://bitbucket.org/usereactify/reactify-search-ui/commit/dd88e8a3ee5e1dce990495aa03b8fe814e35d6c5))

### [5.17.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.17.1...release-v5.17.2) (2023-01-23)


### Bug Fixes

* remove duplicate sensors from storybook ([c41aab2](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41aab22be3dcb16ea0fe31ede1a232b28f6ced3))
* trim whitespace around search terms for tracking within analytics ([257d99a](https://bitbucket.org/usereactify/reactify-search-ui/commit/257d99aa916210d120c0a17e670d34efcd1979f6))

### [5.17.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.17.0...release-v5.17.1) (2023-01-15)


### Bug Fixes

* clear range filter values when values are cleared within underlying reactivesearch component ([720917f](https://bitbucket.org/usereactify/reactify-search-ui/commit/720917f6e3b2c017cfd05a1d2bacf1692212f37e))

## [5.17.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.16.3...release-v5.17.0) (2022-12-15)


### Features

* remove javascript cache ttl behaviour for config and rely on rs cache option and browser cache-control ([730a9c5](https://bitbucket.org/usereactify/reactify-search-ui/commit/730a9c541e57a7336015c5d93b7b23371cf64bef))

### [5.16.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.16.2...release-v5.16.3) (2022-12-08)


### Bug Fixes

* move gtag initializer into provider and include relevant user properties ([72ca81e](https://bitbucket.org/usereactify/reactify-search-ui/commit/72ca81e5556f07f1c7fd4a0c630698ed4e70f6fa))

### [5.16.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.16.1...release-v5.16.2) (2022-12-08)


### Bug Fixes

* check window exists before initializing ga ([19d0f48](https://bitbucket.org/usereactify/reactify-search-ui/commit/19d0f485374f78a9c567cc130e9ec289fe59ae46))

### [5.16.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.16.0...release-v5.16.1) (2022-12-08)


### Bug Fixes

* use shared ga cookie ([b406fe1](https://bitbucket.org/usereactify/reactify-search-ui/commit/b406fe11a120326bb90d0452dc1a23c0c8643b9a))

## [5.16.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.15.2...release-v5.16.0) (2022-12-08)


### Features

* implement ga-gtag ([ff0bcb2](https://bitbucket.org/usereactify/reactify-search-ui/commit/ff0bcb2d8490f6a428769c21b8f9472207c8792f))

### [5.15.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.15.1...release-v5.15.2) (2022-12-07)

### [5.15.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.15.0...release-v5.15.1) (2022-12-07)


### Bug Fixes

* sort by collection position after curation pin and boost sorting rather than before ([593ee9d](https://bitbucket.org/usereactify/reactify-search-ui/commit/593ee9d2b9e46726a99e20e1e7a9782bf0bc4590))

## [5.15.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.14.1...release-v5.15.0) (2022-12-06)


### Features

* allow custom filter sort order ([e7d9b0b](https://bitbucket.org/usereactify/reactify-search-ui/commit/e7d9b0bed3b8df5cf47a12968c915b9c2e492e2e))

### [5.14.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.14.0...release-v5.14.1) (2022-12-01)


### Bug Fixes

* properly encode search query when submitting ([3ecc44f](https://bitbucket.org/usereactify/reactify-search-ui/commit/3ecc44fd72ebe4ea3bc919bf1d823b0a49bc0830))

## [5.14.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.13.0...release-v5.14.0) (2022-11-28)


### Features

* add support for settingsFilterLogic ([4c3b00f](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c3b00f18cb216a4c454e0cf89e2520562973a13))

## [5.13.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.12.2...release-v5.13.0) (2022-11-24)


### Features

* add filter value sorting for bedding sizes ([3bcf4b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/3bcf4b0d8b1da312c586e3ac99482bdbe4ced28b))

### [5.12.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.12.1...release-v5.12.2) (2022-11-21)


### Bug Fixes

* exclude selectable sort option from instant search logic ([988427d](https://bitbucket.org/usereactify/reactify-search-ui/commit/988427d72ee6525cf20c4a3f8a900d3bf9db398a))

### [5.12.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.12.0...release-v5.12.1) (2022-11-21)


### Bug Fixes

* only apply global boosting rules in collection mode ([731009c](https://bitbucket.org/usereactify/reactify-search-ui/commit/731009c9d6df2dee80d30bbef678b7c7a68bdd55))

## [5.12.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.11.0...release-v5.12.0) (2022-11-20)


### Features

* exclude all keyword fields from span_first query ([a479ba7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a479ba7b6be8cedaf96618fe04e6e0d50dce01df))
* improve logic and codeflow for sorting ([80dcb8f](https://bitbucket.org/usereactify/reactify-search-ui/commit/80dcb8f0190d1c88c43efe41bb6c06c48f2e36ef))

## [5.12.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.10.0-beta.1...beta-v5.12.0-beta.0) (2022-11-20)


### Features

* exclude all keyword fields from span_first query ([a479ba7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a479ba7b6be8cedaf96618fe04e6e0d50dce01df))
* improve example slider styling ([02763a6](https://bitbucket.org/usereactify/reactify-search-ui/commit/02763a6f6f9f2ce6103bf76a343f1d29d0aa484c))
* improve logic and codeflow for sorting ([80dcb8f](https://bitbucket.org/usereactify/reactify-search-ui/commit/80dcb8f0190d1c88c43efe41bb6c06c48f2e36ef))
* sort based on _score when mode is search or instant search ([9f82218](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f82218a02c010d9b1ecb286f1b20b765c27929b))


### Bug Fixes

* simplify Filters component ([49467dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/49467ddb5f05ca685cc862426f924d9e78ed22a2))
* update ExampleFilters ([f324434](https://bitbucket.org/usereactify/reactify-search-ui/commit/f324434c000f5bcf2e26ebc19e163bd46565f0d0))
* update pnpm-lock.yaml ([86a53a2](https://bitbucket.org/usereactify/reactify-search-ui/commit/86a53a28f4d6a9806b35bc71c92254d9164205e6))

## [5.11.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.10.3...release-v5.11.0) (2022-11-15)


### Features

* sort based on _score when mode is search or instant search ([9f82218](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f82218a02c010d9b1ecb286f1b20b765c27929b))

### [5.10.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.10.2...release-v5.10.3) (2022-11-06)


### Bug Fixes

* update pnpm-lock.yaml ([86a53a2](https://bitbucket.org/usereactify/reactify-search-ui/commit/86a53a28f4d6a9806b35bc71c92254d9164205e6))

### [5.10.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.10.1...release-v5.10.2) (2022-11-06)


### Bug Fixes

* simplify Filters component ([49467dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/49467ddb5f05ca685cc862426f924d9e78ed22a2))
* update ExampleFilters ([f324434](https://bitbucket.org/usereactify/reactify-search-ui/commit/f324434c000f5bcf2e26ebc19e163bd46565f0d0))

### [5.10.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.10.0...release-v5.10.1) (2022-10-21)

## [5.10.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.9.0...release-v5.10.0) (2022-10-21)


### Features

* add prefix and suffix to filter slider labels ([0eba7c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/0eba7c29dd5590c1dbee7dbcaefaaf9b3cb1c886))
* get intial slider value from URL search params ([98e5ec1](https://bitbucket.org/usereactify/reactify-search-ui/commit/98e5ec1c50735e33aea56a299557823bdec8f659))
* implement custom slider filter logic ([f5ecc3a](https://bitbucket.org/usereactify/reactify-search-ui/commit/f5ecc3ae8c5e19db61b7ffbe9be4de3401c4e85c))
* improve example slider styling ([02763a6](https://bitbucket.org/usereactify/reactify-search-ui/commit/02763a6f6f9f2ce6103bf76a343f1d29d0aa484c))
* improve selected filter labels ([0612f46](https://bitbucket.org/usereactify/reactify-search-ui/commit/0612f46cc136ac447552586251bcb71a7853eea3))
* improve styling of example slider component ([1dd42dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/1dd42dd6c5c4b409e538600add1074f6a114ab8e))
* use zillow react-slider as example slider ([3be008f](https://bitbucket.org/usereactify/reactify-search-ui/commit/3be008f69d75ed5620c9b1894aa103ad5a263493))

## [5.10.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.10.0-beta.0...beta-v5.10.0-beta.1) (2022-10-21)


### Features

* add prefix and suffix to filter slider labels ([0eba7c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/0eba7c29dd5590c1dbee7dbcaefaaf9b3cb1c886))
* get intial slider value from URL search params ([98e5ec1](https://bitbucket.org/usereactify/reactify-search-ui/commit/98e5ec1c50735e33aea56a299557823bdec8f659))
* improve selected filter labels ([0612f46](https://bitbucket.org/usereactify/reactify-search-ui/commit/0612f46cc136ac447552586251bcb71a7853eea3))

## [5.10.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.9.0-beta.1...beta-v5.10.0-beta.0) (2022-10-20)


### Features

* implement custom slider filter logic ([f5ecc3a](https://bitbucket.org/usereactify/reactify-search-ui/commit/f5ecc3ae8c5e19db61b7ffbe9be4de3401c4e85c))
* improve styling of example slider component ([1dd42dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/1dd42dd6c5c4b409e538600add1074f6a114ab8e))
* use zillow react-slider as example slider ([3be008f](https://bitbucket.org/usereactify/reactify-search-ui/commit/3be008f69d75ed5620c9b1894aa103ad5a263493))

## [5.9.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.8.2...release-v5.9.0) (2022-10-16)


### Features

* add hook to access results ([80e19f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/80e19f6afe2a516e2b7592555e5fdb6769912fa9))


### Bug Fixes

* correct typings for result stats ([d01b918](https://bitbucket.org/usereactify/reactify-search-ui/commit/d01b91806d4ef276930fa6395589c84127149ced))

## [5.9.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.9.0-beta.0...beta-v5.9.0-beta.1) (2022-10-16)


### Bug Fixes

* correct typings for result stats ([d01b918](https://bitbucket.org/usereactify/reactify-search-ui/commit/d01b91806d4ef276930fa6395589c84127149ced))

## [5.9.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.6.1-beta.0...beta-v5.9.0-beta.0) (2022-10-16)


### Features

* add hook to access results ([80e19f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/80e19f6afe2a516e2b7592555e5fdb6769912fa9))
* add support for multiple select ranges ([7c35973](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c3597332bfd68088dbd454c4f0a2dd80ce1cd95))
* support filter option for displaying empty values ([9e66934](https://bitbucket.org/usereactify/reactify-search-ui/commit/9e66934714ef4ceb6563ce3d9be76e72453e7557))
* update examples for range and selected filters ([b264aa2](https://bitbucket.org/usereactify/reactify-search-ui/commit/b264aa2af8a8bbddd76e48c22575c6b082777ec6))


### Bug Fixes

* exclude exact tag fields from span_term filtering ([be206ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/be206ca0f0015e556ffceb4df2139a3d8b8e660b))
* prevent initial query without search term and sort option ([44976fd](https://bitbucket.org/usereactify/reactify-search-ui/commit/44976fd5b8861d55ebe9aaf269a014c5c3ca1cc6))

### [5.8.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.8.1...release-v5.8.2) (2022-10-13)


### Bug Fixes

* prevent initial query without search term and sort option ([44976fd](https://bitbucket.org/usereactify/reactify-search-ui/commit/44976fd5b8861d55ebe9aaf269a014c5c3ca1cc6))

### [5.8.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.8.0...release-v5.8.1) (2022-10-13)


### Bug Fixes

* exclude exact tag fields from span_term filtering ([be206ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/be206ca0f0015e556ffceb4df2139a3d8b8e660b))

## [5.8.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.7.0...release-v5.8.0) (2022-10-12)


### Features

* support filter option for displaying empty values ([9e66934](https://bitbucket.org/usereactify/reactify-search-ui/commit/9e66934714ef4ceb6563ce3d9be76e72453e7557))

## [5.7.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.6.1...release-v5.7.0) (2022-10-12)


### Features

* add support for multiple select ranges ([7c35973](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c3597332bfd68088dbd454c4f0a2dd80ce1cd95))
* update examples for range and selected filters ([b264aa2](https://bitbucket.org/usereactify/reactify-search-ui/commit/b264aa2af8a8bbddd76e48c22575c6b082777ec6))

### [5.6.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.6.0...release-v5.6.1) (2022-10-11)


### Bug Fixes

* add all dependencies to sort and query memo ([b60c9bc](https://bitbucket.org/usereactify/reactify-search-ui/commit/b60c9bc1e22b6f277a0cf2716fa4d0adcdafdbc0))
* remove collection position sorting when curation exists ([913fac8](https://bitbucket.org/usereactify/reactify-search-ui/commit/913fac865be62d24aa5e625c0fe0887bada5197b))

### [5.6.1-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.6.0-beta.2...beta-v5.6.1-beta.0) (2022-10-11)


### Bug Fixes

* add all dependencies to sort and query memo ([b60c9bc](https://bitbucket.org/usereactify/reactify-search-ui/commit/b60c9bc1e22b6f277a0cf2716fa4d0adcdafdbc0))
* remove collection position sorting when curation exists ([913fac8](https://bitbucket.org/usereactify/reactify-search-ui/commit/913fac865be62d24aa5e625c0fe0887bada5197b))

## [5.6.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.5.0...release-v5.6.0) (2022-10-11)


### Features

* align queries and sensors for both search and instant search modes ([481c9c6](https://bitbucket.org/usereactify/reactify-search-ui/commit/481c9c62a0c6f3088dd2b0bc2f0fcbb94cbebf9a))


### Bug Fixes

* allow global curation to apply to all search modes ([2de5ab1](https://bitbucket.org/usereactify/reactify-search-ui/commit/2de5ab15d322ac8f98ddb4fdf2fa84c6ca4bc089))
* skip sort option logic for instant search ([4ef922f](https://bitbucket.org/usereactify/reactify-search-ui/commit/4ef922ffe553c51ee90cd5ae6de2f5292bbf6734))

## [5.6.0-beta.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.6.0-beta.1...beta-v5.6.0-beta.2) (2022-10-11)


### Bug Fixes

* allow global curation to apply to all search modes ([2de5ab1](https://bitbucket.org/usereactify/reactify-search-ui/commit/2de5ab15d322ac8f98ddb4fdf2fa84c6ca4bc089))

## [5.6.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.6.0-beta.0...beta-v5.6.0-beta.1) (2022-10-10)


### Bug Fixes

* skip sort option logic for instant search ([4ef922f](https://bitbucket.org/usereactify/reactify-search-ui/commit/4ef922ffe553c51ee90cd5ae6de2f5292bbf6734))

## [5.6.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.2-beta.0...beta-v5.6.0-beta.0) (2022-10-10)


### Features

* add scrollPosition prop to Results to trigger load more ([c2dc2a4](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2dc2a44c853f52691f11ffc8459e64e6ea439ed))
* add scrollTarget prop and infinite scroll pagination rendered ([11f4d02](https://bitbucket.org/usereactify/reactify-search-ui/commit/11f4d02b43058e65a1d1ffa5f96a311f102ef6cf))
* align queries and sensors for both search and instant search modes ([481c9c6](https://bitbucket.org/usereactify/reactify-search-ui/commit/481c9c62a0c6f3088dd2b0bc2f0fcbb94cbebf9a))
* switch from npm to pnpm ([1166f9a](https://bitbucket.org/usereactify/reactify-search-ui/commit/1166f9ae5eab18e4519c3b5be7c9fd5526e6c35e))
* upgrade to react 18 ([f059591](https://bitbucket.org/usereactify/reactify-search-ui/commit/f059591a752fa4cf3a3bfd3709c1053224e1fcdf))


### Bug Fixes

* add debounce and tracking to search term sensor ([7563319](https://bitbucket.org/usereactify/reactify-search-ui/commit/75633197026554b1835ab1f683edc5cc8fcb35c7))
* add lib options to tsconfig ([11da357](https://bitbucket.org/usereactify/reactify-search-ui/commit/11da3571dce6bf8039863fd49bf403c68b8f168d))
* correctly apply weights for phrase-prefix ([88dbac1](https://bitbucket.org/usereactify/reactify-search-ui/commit/88dbac1c902691f6fba99e4a1b2dfab883f00dd7))
* minor clean up for react 18 ([c8a4775](https://bitbucket.org/usereactify/reactify-search-ui/commit/c8a477585acb01960a69734688fe6eac6ae6b672))
* switch from usePaginationLoadMore to usePaginationLoadable ([1337749](https://bitbucket.org/usereactify/reactify-search-ui/commit/1337749b0806d82a5026600387edd91886f3f4c5))
* track zeroResults event when the result data changes rather than the search term ([c10bb44](https://bitbucket.org/usereactify/reactify-search-ui/commit/c10bb4427b77e1cb6581ca35f43b56fd7e715670))
* use pnpm during cicd build process ([fb7c399](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb7c39922afe269af4d93bf59da16874fe671ec7))
* weight search matches higher when field starts with term ([db1d430](https://bitbucket.org/usereactify/reactify-search-ui/commit/db1d43024dc97fb2547c7656b109d34becee6e45))

## [5.5.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.4.0...release-v5.5.0) (2022-10-06)


### Features

* add scrollPosition prop to Results to trigger load more ([c2dc2a4](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2dc2a44c853f52691f11ffc8459e64e6ea439ed))

## [5.4.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.5...release-v5.4.0) (2022-10-06)


### Features

* add scrollTarget prop and infinite scroll pagination rendered ([11f4d02](https://bitbucket.org/usereactify/reactify-search-ui/commit/11f4d02b43058e65a1d1ffa5f96a311f102ef6cf))


### Bug Fixes

* switch from usePaginationLoadMore to usePaginationLoadable ([1337749](https://bitbucket.org/usereactify/reactify-search-ui/commit/1337749b0806d82a5026600387edd91886f3f4c5))

### [5.3.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.4...release-v5.3.5) (2022-09-28)


### Bug Fixes

* track zeroResults event when the result data changes rather than the search term ([c10bb44](https://bitbucket.org/usereactify/reactify-search-ui/commit/c10bb4427b77e1cb6581ca35f43b56fd7e715670))

### [5.3.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.3...release-v5.3.4) (2022-09-25)


### Bug Fixes

* add lib options to tsconfig ([11da357](https://bitbucket.org/usereactify/reactify-search-ui/commit/11da3571dce6bf8039863fd49bf403c68b8f168d))

### [5.3.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.2...release-v5.3.3) (2022-09-25)


### Bug Fixes

* use pnpm during cicd build process ([fb7c399](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb7c39922afe269af4d93bf59da16874fe671ec7))

### [5.3.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.1...release-v5.3.2) (2022-09-25)


### Bug Fixes

* add debounce and tracking to search term sensor ([7563319](https://bitbucket.org/usereactify/reactify-search-ui/commit/75633197026554b1835ab1f683edc5cc8fcb35c7))

### [5.3.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.3.0...release-v5.3.1) (2022-09-15)


### Bug Fixes

* weight search matches higher when field starts with term ([db1d430](https://bitbucket.org/usereactify/reactify-search-ui/commit/db1d43024dc97fb2547c7656b109d34becee6e45))

## [5.3.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.2.3...release-v5.3.0) (2022-09-15)


### Features

* switch from npm to pnpm ([1166f9a](https://bitbucket.org/usereactify/reactify-search-ui/commit/1166f9ae5eab18e4519c3b5be7c9fd5526e6c35e))
* upgrade to react 18 ([f059591](https://bitbucket.org/usereactify/reactify-search-ui/commit/f059591a752fa4cf3a3bfd3709c1053224e1fcdf))


### Bug Fixes

* correctly apply weights for phrase-prefix ([88dbac1](https://bitbucket.org/usereactify/reactify-search-ui/commit/88dbac1c902691f6fba99e4a1b2dfab883f00dd7))
* minor clean up for react 18 ([c8a4775](https://bitbucket.org/usereactify/reactify-search-ui/commit/c8a477585acb01960a69734688fe6eac6ae6b672))

### [5.2.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.2.2...release-v5.2.3) (2022-09-11)

### [5.2.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.2.1...release-v5.2.2) (2022-09-08)


### Bug Fixes

* fix storybook config ([b748e78](https://bitbucket.org/usereactify/reactify-search-ui/commit/b748e7845e17c52cd141401bb24bc3fe85e1a3bd))
* rs-306 add includeFields and excludeFields to ReactiveListProps ([5531b81](https://bitbucket.org/usereactify/reactify-search-ui/commit/5531b81f8305eee862d3124d691619203162f7bd))
* rs-306 teeny cleanup when getting fields from context options ([a4bae10](https://bitbucket.org/usereactify/reactify-search-ui/commit/a4bae10652702a3328a50a56cea106befda749fe))
* switch from reactify-apps to usereactify-demo ([deb36dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/deb36dddd964f89173983f7eca07e9e802c3050e))

### [5.2.2-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.1-beta.1...beta-v5.2.2-beta.0) (2022-09-08)


### Bug Fixes

* fix storybook config ([b748e78](https://bitbucket.org/usereactify/reactify-search-ui/commit/b748e7845e17c52cd141401bb24bc3fe85e1a3bd))
* rs-306 add includeFields and excludeFields to ReactiveListProps ([5531b81](https://bitbucket.org/usereactify/reactify-search-ui/commit/5531b81f8305eee862d3124d691619203162f7bd))
* rs-306 teeny cleanup when getting fields from context options ([a4bae10](https://bitbucket.org/usereactify/reactify-search-ui/commit/a4bae10652702a3328a50a56cea106befda749fe))
* search results not updating when removing or clearing search term ([bc2a947](https://bitbucket.org/usereactify/reactify-search-ui/commit/bc2a947193c61b8dc202d2ca301b36c7498b9d52))
* switch from reactify-apps to usereactify-demo ([deb36dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/deb36dddd964f89173983f7eca07e9e802c3050e))

### [5.2.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.2.0...release-v5.2.1) (2022-08-11)


### Bug Fixes

* search results not updating when removing or clearing search term ([bc2a947](https://bitbucket.org/usereactify/reactify-search-ui/commit/bc2a947193c61b8dc202d2ca301b36c7498b9d52))

### [5.2.1-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.1-beta.0...beta-v5.2.1-beta.1) (2022-08-11)

### [5.2.1-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.0-beta.4...beta-v5.2.1-beta.0) (2022-08-11)


### Bug Fixes

* collection handle was include query string ([cbb2b4f](https://bitbucket.org/usereactify/reactify-search-ui/commit/cbb2b4f772410f9a23ced7b339ab6d12fa350ede))

## [5.2.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.5...release-v5.2.0) (2022-08-10)


### Features

* add getData method to liquid utility ([bf3b75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf3b75c0b3a9f6ece11120ff190f0e740021a58f))
* add utilities for liquid consumers ([e8d738c](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8d738c2a242e4437f05ab0832e86cabed756f75))


### Bug Fixes

* collection handle was include query string ([cbb2b4f](https://bitbucket.org/usereactify/reactify-search-ui/commit/cbb2b4f772410f9a23ced7b339ab6d12fa350ede))
* export liquid utility ([8df10b2](https://bitbucket.org/usereactify/reactify-search-ui/commit/8df10b2dd09658abf41d4404102db8eb20e4b63b))
* improve liquid utilities ([b54a8ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/b54a8caa6c498d3798068fe47527ac5bcde74aca))

## [5.2.0-beta.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.0-beta.3...beta-v5.2.0-beta.4) (2022-08-10)

## [5.2.0-beta.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.0-beta.2...beta-v5.2.0-beta.3) (2022-08-10)


### Bug Fixes

* export liquid utility ([8df10b2](https://bitbucket.org/usereactify/reactify-search-ui/commit/8df10b2dd09658abf41d4404102db8eb20e4b63b))

## [5.2.0-beta.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.0-beta.1...beta-v5.2.0-beta.2) (2022-08-10)


### Features

* add getData method to liquid utility ([bf3b75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf3b75c0b3a9f6ece11120ff190f0e740021a58f))

## [5.2.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.2.0-beta.0...beta-v5.2.0-beta.1) (2022-08-10)


### Bug Fixes

* improve liquid utilities ([b54a8ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/b54a8caa6c498d3798068fe47527ac5bcde74aca))

## [5.2.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.6-beta.0...beta-v5.2.0-beta.0) (2022-08-10)


### Features

* add utilities for liquid consumers ([e8d738c](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8d738c2a242e4437f05ab0832e86cabed756f75))

### [5.1.6-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.4-beta.1...beta-v5.1.6-beta.0) (2022-08-10)


### Bug Fixes

* filter searchTerm curations in search mode only ([57b6359](https://bitbucket.org/usereactify/reactify-search-ui/commit/57b6359aabbacce95c724be96df27cdc8d9eb2f4))
* remove unnecessary check for collectionHandle ([1a634e5](https://bitbucket.org/usereactify/reactify-search-ui/commit/1a634e517fee6a9db8b6deeb43eba7c012070a0a))

### [5.1.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.4...release-v5.1.5) (2022-08-09)


### Bug Fixes

* filter searchTerm curations in search mode only ([57b6359](https://bitbucket.org/usereactify/reactify-search-ui/commit/57b6359aabbacce95c724be96df27cdc8d9eb2f4))
* remove unnecessary check for collectionHandle ([1a634e5](https://bitbucket.org/usereactify/reactify-search-ui/commit/1a634e517fee6a9db8b6deeb43eba7c012070a0a))

### [5.1.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.3...release-v5.1.4) (2022-08-09)


### Bug Fixes

* search for curation based on collectionHandle or searchTerm value existing ([541901f](https://bitbucket.org/usereactify/reactify-search-ui/commit/541901fe4e53fa51b161c8253bc5b00c1f516a0c))
* use curation sort position for search term curations ([adace67](https://bitbucket.org/usereactify/reactify-search-ui/commit/adace67c1b0e1d0bafab417a3535c61e853f2890))

### [5.1.4-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.4-beta.0...beta-v5.1.4-beta.1) (2022-08-09)


### Bug Fixes

* use curation sort position for search term curations ([adace67](https://bitbucket.org/usereactify/reactify-search-ui/commit/adace67c1b0e1d0bafab417a3535c61e853f2890))

### [5.1.4-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.3-beta.0...beta-v5.1.4-beta.0) (2022-08-09)


### Bug Fixes

* search for curation based on collectionHandle or searchTerm value existing ([541901f](https://bitbucket.org/usereactify/reactify-search-ui/commit/541901fe4e53fa51b161c8253bc5b00c1f516a0c))

### [5.1.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.2...release-v5.1.3) (2022-08-09)


### Bug Fixes

* merge sensor queries for curations ([118bd40](https://bitbucket.org/usereactify/reactify-search-ui/commit/118bd408d9a0b0e518cefcced28f276d66b17655))

### [5.1.3-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.1-beta.1...beta-v5.1.3-beta.0) (2022-08-09)


### Bug Fixes

* merge sensor queries for curations ([118bd40](https://bitbucket.org/usereactify/reactify-search-ui/commit/118bd408d9a0b0e518cefcced28f276d66b17655))
* rename "additonalComponentHandles" to "additonalComponentIds" ([8dc5a3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/8dc5a3ec11d5d831ccaf84f6621a8d483d88a95f))

### [5.1.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.1...release-v5.1.2) (2022-08-08)


### Bug Fixes

* rename "additonalComponentHandles" to "additonalComponentIds" ([8dc5a3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/8dc5a3ec11d5d831ccaf84f6621a8d483d88a95f))

### [5.1.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.1.0...release-v5.1.1) (2022-08-08)


### Bug Fixes

* prevent sensorsort using global curation ([55cdfaa](https://bitbucket.org/usereactify/reactify-search-ui/commit/55cdfaa8e5fa8b32c3bce111d7a681eb7d7577ae))

### [5.1.1-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.1-beta.0...beta-v5.1.1-beta.1) (2022-08-08)

### [5.1.1-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.1.0-beta.0...beta-v5.1.1-beta.0) (2022-08-08)


### Bug Fixes

* prevent sensorsort using global curation ([55cdfaa](https://bitbucket.org/usereactify/reactify-search-ui/commit/55cdfaa8e5fa8b32c3bce111d7a681eb7d7577ae))

## [5.1.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/release-v5.0.1...release-v5.1.0) (2022-08-07)


### Features

* separate searchTerm sensor for instant-search and search ([37a767a](https://bitbucket.org/usereactify/reactify-search-ui/commit/37a767a8b90d89ca9f7b16cce9783ce045671882))

## [5.1.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/beta-v5.0.0-beta.16...beta-v5.1.0-beta.0) (2022-08-07)


### Features

* separate searchTerm sensor for instant-search and search ([37a767a](https://bitbucket.org/usereactify/reactify-search-ui/commit/37a767a8b90d89ca9f7b16cce9783ce045671882))

### 5.0.1 (2022-08-05)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5
* switch to using "handle" instead of "id" for sort option
* switch to using "handle" instead of "id" for filter and filter option
* move index to a dedicated prop, make credentials optional and set default

### Features

* add additionalComponentIds prop to provider ([1db51a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1db51a0f905475d2d3399f9de7ff6f025860dce5))
* add breadcrumb for ES search result errors ([22ae301](https://bitbucket.org/usereactify/reactify-search-ui/commit/22ae301bbb7618aec14499970ffd2578ab569a7a))
* add breadcrumb for search query update ([6a2e442](https://bitbucket.org/usereactify/reactify-search-ui/commit/6a2e44269396353fced1d597d48502ef5b1a6bb2))
* add classnames for styling to default components ([75997e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/75997e25756595401401de42e9aa29b235581645))
* add debug output to provider ([c45de1a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c45de1ae681ede827ae899252758171309bac09b))
* add debug output to SensorSort ([0b3d125](https://bitbucket.org/usereactify/reactify-search-ui/commit/0b3d125f29dfe678318d91390d3fbfd13a932947))
* add esbuild for production builds ([9b0a8d5](https://bitbucket.org/usereactify/reactify-search-ui/commit/9b0a8d5daa4a90b96f8760e083431da7e44f6ab5))
* add example components for filter range, filter slider, custom, stats, clear all, filters active ([d5e56fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/d5e56fb114edc3e134c3ce2e90f7f2df67c5d097))
* add example filter list unstyled, radio, checkbox, swatch and box ([a6ebcd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6ebcd0b606f9a8381d1e082dd4935e4a4d83c2f))
* add Filter and FilterList components with hooks ([9cd1be1](https://bitbucket.org/usereactify/reactify-search-ui/commit/9cd1be1b1796962be340fd4a119e21d7d0a04e0e))
* add filterStackId prop to provider ([#2](https://bitbucket.org/usereactify/reactify-search-ui/issues/2)) ([043e9f0](https://bitbucket.org/usereactify/reactify-search-ui/commit/043e9f0745ad1d6e15ec99400df099320fecf7cf))
* add grid gap to result list component props ([0d65bd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d65bd032cc834060fca5702195566a36ef8fb33))
* add hook to get cached or remote config ([cb77750](https://bitbucket.org/usereactify/reactify-search-ui/commit/cb77750f4839134e65afb2eee685755169bfcd5d))
* add image to default result card render ([c50319e](https://bitbucket.org/usereactify/reactify-search-ui/commit/c50319e1011a7465c07b509b5164ea04b9f38684))
* add listClassName prop to ResultList component ([f28be3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/f28be3e4d0f8c74d5e7e20e214aec0ee7867ce7c))
* add local bundle serve ([318443e](https://bitbucket.org/usereactify/reactify-search-ui/commit/318443efc082a5714e99221d4647a91f60d9ac91))
* add onRedirect prop + fix caching issue when searching from search page ([59b32f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/59b32f65c8ede64cbd42d297ed7a4e131b8650eb))
* add pagination, sort, filter and promotion analytics events ([5968814](https://bitbucket.org/usereactify/reactify-search-ui/commit/596881486f05189d90823f0ec1b093580d79a786))
* add renderBooting prop for rendering while config is loading ([3da876e](https://bitbucket.org/usereactify/reactify-search-ui/commit/3da876e5a87ecd98f2abe462994461b7cd39acda))
* add renderResultCard prop to ResultList component ([889255f](https://bitbucket.org/usereactify/reactify-search-ui/commit/889255fc8cb2f1db0a6d86be2159276a0da384b7))
* add ResultPagination component and usePages hook ([#4](https://bitbucket.org/usereactify/reactify-search-ui/issues/4)) ([e8dcf61](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8dcf610ec1eac9b328935a713732284a1f4d7f8))
* add ResultStateProvider component for accessing page state ([ec4cc6f](https://bitbucket.org/usereactify/reactify-search-ui/commit/ec4cc6f08ef2aeae46fe864e61d71ec36e50a0e3))
* add SearchInput component and associated instant search support ([98a8af7](https://bitbucket.org/usereactify/reactify-search-ui/commit/98a8af726f9e781bd03c5ed2dd815c0976b19f21))
* add SensorInventoryAvailable and rename SensorStack ([18d7aaa](https://bitbucket.org/usereactify/reactify-search-ui/commit/18d7aaa4a2d7e82d34014ccc888c624cffba4f03))
* add SensorStackInstantSearch ([e93308f](https://bitbucket.org/usereactify/reactify-search-ui/commit/e93308f06532e7877300c84820e5019b152c895b))
* add sentry error boundary ([c6dbbab](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6dbbabb6e130eb395cf984df655951981ef7496))
* add sentry release tracking to ci ([67c813d](https://bitbucket.org/usereactify/reactify-search-ui/commit/67c813d2c8812cf1b6c4e2ad1ce9b0a38ebce24b))
* add shop name in context, ([13b54e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/13b54e28715b7789cc5cf47d82cc205d175ce1ee))
* add sort option into url params ([f78ec9b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f78ec9b69555df9cb297d818a06d08d6587889c3))
* add support for additional datasearch input props ([a6c6382](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6c63825f6d3e616d57dc0f0af91895475175cc0))
* add support for collections assigned to filter groups ([afe0711](https://bitbucket.org/usereactify/reactify-search-ui/commit/afe071115aa7633f49d5f95673c544989e80f011))
* add support for filter option settingsUppercase ([99d822b](https://bitbucket.org/usereactify/reactify-search-ui/commit/99d822b611ec0302fe314cbe0b1c24653b1ae72c))
* add support for global curation / default boosting rules ([b476e75](https://bitbucket.org/usereactify/reactify-search-ui/commit/b476e75233202bd3ab190d7806cad0b8077463a8))
* add support for pinning products in curations that don't exist within the resultset ([3ceadf6](https://bitbucket.org/usereactify/reactify-search-ui/commit/3ceadf621ab78b216bf355d3e2ec648a6d9fee44))
* add support for settingsHideUnavailable at filter level for variant nested fields ([5ca63e1](https://bitbucket.org/usereactify/reactify-search-ui/commit/5ca63e1dcd35c9f6f80c83d49ab6f8bb87ecaa7d))
* add support for valuesManual in filter option filtering ([c2cef69](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2cef69e491f9852a1250e7218550ac6c1e1671e))
* add totalSelected to filter render props (RS-16) ([7949200](https://bitbucket.org/usereactify/reactify-search-ui/commit/7949200f0307637dda424875e7ca394aa179cdb6))
* add warning and prevent tracking request if "shopifyPermanentDomain" is missing ([bcbf378](https://bitbucket.org/usereactify/reactify-search-ui/commit/bcbf378ac057755347556a60bae58684df8f65c1))
* add zero results and view product (impressions) events ([db4f310](https://bitbucket.org/usereactify/reactify-search-ui/commit/db4f3102c592bc295534bcb5de9523d1f0b26557))
* additional result props in ResultList ([821b022](https://bitbucket.org/usereactify/reactify-search-ui/commit/821b02281baa07b844ce3c6dbba1f8a1ef42dee7))
* adjust page size to accommodate callouts ([0365896](https://bitbucket.org/usereactify/reactify-search-ui/commit/03658965e65ba451a5c419b14599a71296e589c3))
* allow custom grid styles to be provided and override defaults ([25ce405](https://bitbucket.org/usereactify/reactify-search-ui/commit/25ce405300777b9d55a0e806672787bd552438a6))
* allow provider theme to be configured ([33eb10f](https://bitbucket.org/usereactify/reactify-search-ui/commit/33eb10f50dbdaaf2f399ea0e95b673222da868b1))
* **app:** add provider prop "configId" to support multi-config shops ([2ef6af5](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ef6af5a6d46331690bdd3c35ee72193dca8f587))
* apply defaultQuery to nested variant filters to check inventory available ([a348522](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3485220bb3c682d70a6efcd9b520972430a00d7))
* call track after trigger query in SearhInput ([8a09111](https://bitbucket.org/usereactify/reactify-search-ui/commit/8a0911165ea4484bf7bcf65b12c687a0d8b674ca))
* collapse state optional in renderFilterList, rename filterListProps ([9eef3d3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eef3d35ffeaefd568a1d1839bc13b87705e7cb1))
* collection default query ([154e3d2](https://bitbucket.org/usereactify/reactify-search-ui/commit/154e3d2ceadce6625427aa6d7d0cd98ac671a6d2))
* combined results in result list ([a85f39e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a85f39e8d425c51b8108df488145ade8eec1cdf5))
* currency formatting, brand logos and hover image improvements ([9c63fe3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9c63fe33b3d67b2507e2c6f05d7a5450c88132f2))
* default query and sort state ([f20dc5c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f20dc5cf5751ce106bb5fce821e648d1cd1a10c0))
* disable callouts unless enableCallouts prop is set ([c2ac79a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2ac79abffc87d050b766b6786977de43920af17))
* drop SensorStackInstantsearch in favor of single SensorStack with context ([f31a60b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f31a60b9ba6bf2c61d88529b3ceec0b5aa188e1f))
* enable url params for filters ([6f2675f](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f2675f0328de1e3c0cd20172d2053b2776dfb0f))
* filter column titles ([05c41ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/05c41ba8985f2de317fffd9fafc3d883c6dced0c))
* filter matrix and hook ([278d8ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/278d8adfcce10575b90a8b8c16797f167961843b))
* filter options ([106387a](https://bitbucket.org/usereactify/reactify-search-ui/commit/106387aeb38cafa1fdb7132b4c5cf97d08949a5c))
* filter titles and collapse logic ([5469941](https://bitbucket.org/usereactify/reactify-search-ui/commit/546994160fb291b1e15c522730d72d895216033f))
* firebase config ([1038df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1038df0bbc729ca268c715a0c8de6d4667adb4c6))
* hide callouts when not sorting by "_score" or "collections.position" ([bf2d57a](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf2d57aa5b682375dc06995bb54d90ece330f5e3))
* ignore cached config if "nocache" url search param is set ([38823c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/38823c2272fd0a2ae6d45401df35a2148107fe8e))
* import debut theme collection to playground page ([c137569](https://bitbucket.org/usereactify/reactify-search-ui/commit/c137569902b69636e53c6f9cdf297088e18c548a))
* improve classnames ([19fcfa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/19fcfa152c555cb2e78ec62371f1758e2d244557))
* improve default components ([81bb427](https://bitbucket.org/usereactify/reactify-search-ui/commit/81bb427367987125eeea352189dd9f411cb4850f))
* improve dx for filter wrapper, map checked in hook ([3873238](https://bitbucket.org/usereactify/reactify-search-ui/commit/38732380e0a790c92709308b30815d6c0170b27d))
* improve performance when debugging by reusing debugger instances ([aa88917](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa88917b0aedf2abe66439d430587f18dcb72aae))
* improve sentry release process ([4237771](https://bitbucket.org/usereactify/reactify-search-ui/commit/423777120041228dcb6f819f71a00abd08d43987))
* improve sentry stacktraces and logging ([7f34b30](https://bitbucket.org/usereactify/reactify-search-ui/commit/7f34b3064f41ad9a0e5a3ddc3fa071e5981d0141))
* improve sorting of filter list options ([c41eedc](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41eedc900f500d3ddf9434cb14a7397c6385707))
* initial docs ([016fd2b](https://bitbucket.org/usereactify/reactify-search-ui/commit/016fd2ba36d7992bc6c63f9c75315befbfddd625))
* initial ui library components ([803d586](https://bitbucket.org/usereactify/reactify-search-ui/commit/803d586af75217401c34f7df1b2613eb9944c710))
* instant search redirects and sort score ([55d85cc](https://bitbucket.org/usereactify/reactify-search-ui/commit/55d85cc7e60803825a338008c52da95b6d035ab8))
* instantsearch component ([8256cdc](https://bitbucket.org/usereactify/reactify-search-ui/commit/8256cdc9ba0b281bd4135d62240c93180cca8210))
* instantsearch with reactivelist component ([1564b0d](https://bitbucket.org/usereactify/reactify-search-ui/commit/1564b0dd21b25244c635ea1ac903b177aad3bc29))
* merge global boosting rules with curation when empty ([248cbdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/248cbdda01864c0d536f5e971f81e331de4c827d))
* move filter logic to hook, add render props ([333da46](https://bitbucket.org/usereactify/reactify-search-ui/commit/333da4683d93bfeec2c683a43e094179ce5cc39d))
* move index to a dedicated prop, make credentials optional and set default ([a0696ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/a0696ba239b7b036b37a2764a939ce2f26e992b4))
* multilist filter ([34be9bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/34be9bf081c0f24520e634ae2ada863deb8f238b))
* optional collapse state in filter wrapper, add noWrapper prop ([2ea7d1f](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ea7d1feabe26d2c70826b517b94ddac9a972e87))
* pagination ([e50965a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e50965a1cba32ae30bc1545789d551ce94ba4556))
* pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([fb93473](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb93473fa5723a71cfad264f4c4fd09c541ea278))
* product card breakpoints ([657d1b1](https://bitbucket.org/usereactify/reactify-search-ui/commit/657d1b1452f81898552133732086d7e7938f16ca))
* remove combined results logic, discriminate product and callout ([ee1b38e](https://bitbucket.org/usereactify/reactify-search-ui/commit/ee1b38eeddad8eedec473c5b49364b67fa6ae311))
* remove defaultQuery in favor of SensorCollection ([f33c1b2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f33c1b2d1eaf3dfbccd6b8a786e70a56a788bb3e))
* remove event handling overrides in useAnalytics track function ([3cb1bc5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3cb1bc50318f64c2d11013697152d187108d603c))
* rename FilterMatrix to FilterStack ([62788cd](https://bitbucket.org/usereactify/reactify-search-ui/commit/62788cd135e424ab81426ad7e1ae7a3d825d4e28))
* render callouts in result list, implement grid ([e8cbaad](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8cbaad07db3daf2c4a59a2de565774db213d0c6))
* renderAfter, renderBefore and renderPagination props on ResultList ([05d2203](https://bitbucket.org/usereactify/reactify-search-ui/commit/05d2203447d4ca9fb7843dc3ad0b8b8f111b67e6))
* renderError prop on ResultList component ([d95d6df](https://bitbucket.org/usereactify/reactify-search-ui/commit/d95d6df10059badc661eeb1041db76531e44231c))
* renderLoading prop on ResultList component with initial search logic ([1568d2e](https://bitbucket.org/usereactify/reactify-search-ui/commit/1568d2e8729b455b50d42c532cb3bcb91c4e4130))
* renderLoadMoreButton prop on ResultList with default button ([bd5af24](https://bitbucket.org/usereactify/reactify-search-ui/commit/bd5af24daf4fe1e3c41afa5749f10df0d8b64bb2))
* renderPaginationNextPrev prop on ResultList with default component ([d587303](https://bitbucket.org/usereactify/reactify-search-ui/commit/d58730367916629770240c8e85549003ce8ddc28))
* result card component, list logic and hooks ([7c419e9](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c419e9fd1a5fdcdb00ed3289fbcf069d3646710))
* result card promo component ([357e228](https://bitbucket.org/usereactify/reactify-search-ui/commit/357e2281b8fa2d3ace97c3ab8e90e55de87e0e02))
* result list component and hook ([78834ce](https://bitbucket.org/usereactify/reactify-search-ui/commit/78834ce65eb804395c3ea303d3e00f4364c0cdfc))
* search components ([e744e5a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e744e5ae38dfb8bc54603eadf9443c0aaddb1f1e))
* search fields from reactify config, redirect to search page on enter ([dc47d45](https://bitbucket.org/usereactify/reactify-search-ui/commit/dc47d45f39f49bdc386006c238fa823c6fc07cbb))
* **search:** allow searchQuery to be provided to submitSearch ([116e5d6](https://bitbucket.org/usereactify/reactify-search-ui/commit/116e5d60084ae20ccac3bb07d9e17d2158888479))
* **search:** onBlur, onFocus events in SearchInput component ([c6799be](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6799be75c667a47c9c6aac84696040ff0f12acc))
* see all results button in instant search results ([90c6596](https://bitbucket.org/usereactify/reactify-search-ui/commit/90c659646423bc871e37f071682c6e5569cb13f9))
* SensorPublished and SensorSearch ([6c735c1](https://bitbucket.org/usereactify/reactify-search-ui/commit/6c735c11525b4d33e9bca611e82b97ec37ec0b8c))
* SensorSortScore component ([a3050f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3050f6a7c9f1c727ad920c2b68d8e4fbdf933d9))
* set default reset theme ([0661294](https://bitbucket.org/usereactify/reactify-search-ui/commit/0661294a03eae7e70d29a2415df968da38bb0bf8))
* sort context and sensor ([8dde395](https://bitbucket.org/usereactify/reactify-search-ui/commit/8dde3953cc022723fbc58e67d75cdcf3da95d023))
* sort of _score always be desc ([f51e75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f51e75c4f21acb54fbe64e444b26dd02f452c6ac))
* sort sensor and selector component ([b52bcfa](https://bitbucket.org/usereactify/reactify-search-ui/commit/b52bcfa0e796ed8ed5e7e81a068100162243932e))
* support hidden curation products in SensorSort ([c80a52f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c80a52f6e515bb023dd99caedc20ace38444b725))
* switch to using "handle" instead of "id" for filter and filter option ([a92bdd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a92bdd75e68c6291b9f321cec5e77f3934152a9a))
* switch to using "handle" instead of "id" for sort option ([c2be1fa](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2be1fabd4ba1634e47715393c2065a1ca2ba9e7))
* **theme-debut:** add instant search component ([3017fb2](https://bitbucket.org/usereactify/reactify-search-ui/commit/3017fb2d2b2da6413d148e7853e47f7cdf30e3e2))
* **theme-debut:** add styled filters ([467b255](https://bitbucket.org/usereactify/reactify-search-ui/commit/467b2551723aa413696f4060d9b20777a40298e4))
* **theme-debut:** add styling for product cards ([1397aa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/1397aa193eec73592e17b32a5a12d4806a7f808b))
* **theme-debut:** add tailwind jit styles ([6e28c52](https://bitbucket.org/usereactify/reactify-search-ui/commit/6e28c520a2113f68fb0fc37bbedd2083c39ad39f))
* update build commands ([b084e0c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b084e0cf5574e65d0aa3263ddb862901e0a63a06))
* update callout card for new ElasticCallout type ([8db23b5](https://bitbucket.org/usereactify/reactify-search-ui/commit/8db23b5e5b240b45ada520b30a43ff988bcff9d5))
* update provider to use live config hook ([d898df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/d898df01cb21538d9cd5dada9d343967ad91cdd6))
* use emotion cache key to support multiple apps in page ([07a5067](https://bitbucket.org/usereactify/reactify-search-ui/commit/07a5067b339b7c555e3b939b09758df00598698f))
* useAnalytics hook ([17fb749](https://bitbucket.org/usereactify/reactify-search-ui/commit/17fb749e763870a2888c3cf68ceeff3cd0d8fc21))
* useReactiveBaseProps hook and restructure ReactiveBase inclusion ([51873b3](https://bitbucket.org/usereactify/reactify-search-ui/commit/51873b39abe927115487d24c6140be21a2b82c8a))


### Bug Fixes

* add missing lower case to existing sort sensor ([c7099a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/c7099a0edf57658fa006ac018b017807891a44dd))
* add missing next/prev pagination export ([6b13902](https://bitbucket.org/usereactify/reactify-search-ui/commit/6b139027cc1b5038add4d688a9301add2f6e75ee))
* add missing package "axios" ([eaa4763](https://bitbucket.org/usereactify/reactify-search-ui/commit/eaa476367ef0947b20c24fe4001efb4550184516))
* add missing sensors to result list react prop ([2293c43](https://bitbucket.org/usereactify/reactify-search-ui/commit/2293c4391917a536e31fc3f3fce08703ab134e6b))
* add missing spread operator ([acc51cb](https://bitbucket.org/usereactify/reactify-search-ui/commit/acc51cb5c414831e3d0e919185d076895903d837))
* add nested attribute to collection position sort clause (RS-124) ([0932b3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0932b3e6e64dd2a0ef9df592c431f0dc5340fcc1))
* add timeout to blur event so results remain clickable ([fe98040](https://bitbucket.org/usereactify/reactify-search-ui/commit/fe980407427256246051bee8be152048ae841cf2))
* **app:** fix access to undefined sort option ID ([f93bbce](https://bitbucket.org/usereactify/reactify-search-ui/commit/f93bbcec82ee61d4a0bf19d7ab18894339f8a27c))
* case insensitive redirect match ([#11](https://bitbucket.org/usereactify/reactify-search-ui/issues/11)) ([096c7fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/096c7fb84d77ae0d985b2e7cf4ade6c80e3171e7))
* check compare_at_price exists before formatting in useProductPrice ([acba76c](https://bitbucket.org/usereactify/reactify-search-ui/commit/acba76c6d8e0c5ba1482fe230e20fcd286f11e69))
* check for existence of window before using (node render) ([df7f9b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/df7f9b0c0c29d561e69107c8086a5d0fa023aaee))
* check that window is defined before accessing ([e1ec0aa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1ec0aae22cab7eb86363ed09d47e422d08ff5c1))
* correct logic for calculating search fields when config is empty or updates ([ae67498](https://bitbucket.org/usereactify/reactify-search-ui/commit/ae6749875f295db99f012b54cc06e10346f702d0))
* correct tsconfig ([e82defa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e82defac47d754df543fc276319ba759de607507))
* correct tsconfig ([01a796b](https://bitbucket.org/usereactify/reactify-search-ui/commit/01a796bfc03f4153d8fbbea86a0597ee3c665a94))
* correct typings for selected sort option within context ([f363fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f363fc2c8ea1ad88e65844c382620b927614c403))
* define nested field prop for variants ([2af7ebe](https://bitbucket.org/usereactify/reactify-search-ui/commit/2af7ebe6b33c1ca19faf27ca0f525fe1f1669ced))
* disable scrollOnChange prop ([50592e0](https://bitbucket.org/usereactify/reactify-search-ui/commit/50592e0db250bfae5809b0a92e9c5f72e43b4d88))
* dom warnings ([555fb78](https://bitbucket.org/usereactify/reactify-search-ui/commit/555fb783e44d28261c701e2b5896533b5b2f6743))
* enable typescript inlinesources ([6998e8e](https://bitbucket.org/usereactify/reactify-search-ui/commit/6998e8e3e884d62cf08b08c43ae7dbc73db7f471))
* export Filter and FilterList components ([7a297dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/7a297dd630c3582d5bf9ae32eb6d31663ee2654d))
* filter currentId from all filters in useReactiveReactProp ([645fe73](https://bitbucket.org/usereactify/reactify-search-ui/commit/645fe7373f469255d15bbe12c05d478193053ac6))
* filter styling updates ([c5c3b8f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c5c3b8f9e4fa349fac05e288bd146053802396a1))
* fix request payload ([aa33e95](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa33e95ba1162ec0bbb84cd25c2e4483f3822455))
* fix search event payload ([5bd8c03](https://bitbucket.org/usereactify/reactify-search-ui/commit/5bd8c03e024f3ff0fb14135b9dbc60a9c709fb38))
* force lower case curation search term ([09d73bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/09d73bf8b86942f8fb67d9283e55bee6e99ae963))
* force value to be an array in filter customQuery ([454c110](https://bitbucket.org/usereactify/reactify-search-ui/commit/454c11023a3386c00ad4bcec33ba04d94c86c330))
* include sorting by collection position when using global curation ([141c463](https://bitbucket.org/usereactify/reactify-search-ui/commit/141c46351711c3cff65275e47fdf8e9ed64b0ad8))
* incorrect hasSelected calculation from filter value ([bf1e8c8](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf1e8c84a54164bd905931743356d301b3b2b5d6))
* instant search sorting ([b6ce036](https://bitbucket.org/usereactify/reactify-search-ui/commit/b6ce036d06d72dbbe28fb1892537ee399ee77c0c))
* make collection nodes optional ([3b2afe5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3b2afe5432d50ba2f8ee57f62478708e113d11d1))
* match useAnalytics endpoint signature ([b94a70c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b94a70c9b1a1f8acc9a11bbff8e6414e77d34657))
* normalise collection handle and search term when resolving curation ([6bd1aec](https://bitbucket.org/usereactify/reactify-search-ui/commit/6bd1aecd163743bdcfc9582389f35a4dc579930c))
* only apply inventory sensor to products, not callouts ([7c186b8](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c186b83c9f403f0987ef088714db1438f27cd84))
* only record select event when link clicked ([ba95992](https://bitbucket.org/usereactify/reactify-search-ui/commit/ba959923785837ed54053257012a1cfdcc69be59))
* only request live config if shop is present ([a9ab343](https://bitbucket.org/usereactify/reactify-search-ui/commit/a9ab343aef7790650635517a83fe4889e87427a3))
* only run tracking event when data exists ([a768c19](https://bitbucket.org/usereactify/reactify-search-ui/commit/a768c1931fd52e69d5538760547c50d610bcb8cf))
* pagination spacing ([945f23a](https://bitbucket.org/usereactify/reactify-search-ui/commit/945f23acfb9e9f39b343b51cad53bf57c8df8522))
* pass on nocache url param to config endpoint to bust caching on worker side ([51b9dcf](https://bitbucket.org/usereactify/reactify-search-ui/commit/51b9dcf0b8145691a2dd00aedbe951f71bbc04b3))
* pass render prop to ResultCardCalloutInner, add pagePosition prop ([3983db4](https://bitbucket.org/usereactify/reactify-search-ui/commit/3983db4c471a3172259662f03e400c3f8ed14b0d))
* product card image margins ([8417270](https://bitbucket.org/usereactify/reactify-search-ui/commit/8417270a3a829e525c6d4b330018f893dbe4fb01))
* product card negative margin ([0573eb0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0573eb02b9bdac3ada0c9ad6f58d91980cba4ef2))
* product card text styling and alignment ([ef16770](https://bitbucket.org/usereactify/reactify-search-ui/commit/ef167700cccf3f094fed8329a6c3471f9af54c6a))
* re-pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([77bbe4c](https://bitbucket.org/usereactify/reactify-search-ui/commit/77bbe4cb340d0afc2e76c6f584675db5322fee29))
* react results component to SensorCollection ([c2833e6](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2833e6188958b054c39fce7cceefc154cc3e66c))
* regenerate yarn.lock in release script ([b97ddef](https://bitbucket.org/usereactify/reactify-search-ui/commit/b97ddefae363a25303afc90042044f0692fac3c5))
* remove console error for range filter ([9eeeb5d](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eeeb5df6b1e71ce1d9ce784259b141988c450fb))
* remove name and variant from select_item event ([a87037d](https://bitbucket.org/usereactify/reactify-search-ui/commit/a87037d493a269fd402f68431fa4fd0c90911f70))
* remove problematic css color "inherit" ([aa304bd](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa304bde76528d54722255b0e38ef732f58218b6))
* render issues with expired config cache ([2c408ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/2c408adce950da369db77ff8ece5a8d08cd92d5f))
* result card image alignment ([46aedd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/46aedd030f4e623bee3d28647f97c100648cd8ef))
* **search:** correct type for context submitSearch ([8b3e647](https://bitbucket.org/usereactify/reactify-search-ui/commit/8b3e64716b6d82e14efc8abb88ff1f6013d5c182))
* **search:** include _score in sort state eligible for curations ([2839929](https://bitbucket.org/usereactify/reactify-search-ui/commit/2839929be960aabd0b45e4aad0ce6a5abdc776e6))
* **search:** ssr build issue due to window ([aada68a](https://bitbucket.org/usereactify/reactify-search-ui/commit/aada68a20cdb8cffe43abbde487229819fdd39e3))
* select_item event, add questions in comments ([8693a78](https://bitbucket.org/usereactify/reactify-search-ui/commit/8693a781049ee44d429f4bf6149532737d9ccbb8))
* set base url for docs ([11fca33](https://bitbucket.org/usereactify/reactify-search-ui/commit/11fca33ea6e9a2b137b758f67206194b9b4e574b))
* set product images to large via cdn ([d1c311f](https://bitbucket.org/usereactify/reactify-search-ui/commit/d1c311fcb093b2ba0ec31adf3677da079328c833))
* show all filter options unless displaySize is defined ([6887ed7](https://bitbucket.org/usereactify/reactify-search-ui/commit/6887ed70737b864cf87cf97433626b9e5ecb7c98))
* size and colour filter targets in docs ([735c12e](https://bitbucket.org/usereactify/reactify-search-ui/commit/735c12ed29683d324651382f4fad55c16941e716))
* ssr playground issues ([5cdb445](https://bitbucket.org/usereactify/reactify-search-ui/commit/5cdb4458ca5db1526a3e00eeff99b7e071dd3ae2))
* support missing compare_at_price on price sets ([dbe3fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/dbe3fc20db23e1969f0d0c9ee75e646cce3f1858))
* support v1 indexes in ResultList component ([4c23ef3](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c23ef3757daeb505823f4af6399746a29404f7a))
* switch to reactify sentry org dsn ([cae3cf0](https://bitbucket.org/usereactify/reactify-search-ui/commit/cae3cf02721ee8b40ddf13854e7aa3cea88034f4))
* text styling for filter titles ([7c0d15c](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c0d15cbd05ace9df056b260c2978146109270a7))
* **theme-debut:** browser warning for svg property ([4fd7948](https://bitbucket.org/usereactify/reactify-search-ui/commit/4fd794848b4b15becb601718f635005407db6d79))
* **theme-debut:** fix flex width on results grid ([049da94](https://bitbucket.org/usereactify/reactify-search-ui/commit/049da942aa87c7a887d689f78f77da02b9b63422))
* **theme-debut:** use new SensorStack component ([da59639](https://bitbucket.org/usereactify/reactify-search-ui/commit/da5963989b5012a231df3ea572f02095c4cc8489))
* tighten up instantsearch debounce solution ([6f811ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f811ca9350165c8858ae3388eac09af56a32cfd))
* update filter resolution to return first filter if none found with type ([e7990d1](https://bitbucket.org/usereactify/reactify-search-ui/commit/e7990d1727dd019fba19f9a8132109c992a9a57e))
* update provider props to suit merged changes ([f014e87](https://bitbucket.org/usereactify/reactify-search-ui/commit/f014e87e14776577d0f83d24a5c94aa6d3b15818))
* use american color variation ([57f9b0e](https://bitbucket.org/usereactify/reactify-search-ui/commit/57f9b0e6de4482fd451bd36a1b49e47bb8d28f8e))
* use collection for filter for both filter and search until configurable ([007def5](https://bitbucket.org/usereactify/reactify-search-ui/commit/007def512972b88ae1750157184436e11aca2691))
* use commonjs ([b742b8b](https://bitbucket.org/usereactify/reactify-search-ui/commit/b742b8b268c0a663361147efb93bf120a208a708))
* use correct glob pattern for artifacts ([9f35c7c](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f35c7ccea4f957a94b172fe7ac62b53faf73b32))
* use correct names of components and imports ([e180a75](https://bitbucket.org/usereactify/reactify-search-ui/commit/e180a75b0c4be555a61d72e48a54434b7aa27960))
* use docusaurus router for index docs redirect ([167403b](https://bitbucket.org/usereactify/reactify-search-ui/commit/167403b4aa2f53c57f4cbc69c0a00f78fb46afd2))
* use variants.available in SensorInventoryAvailable ([ffaa237](https://bitbucket.org/usereactify/reactify-search-ui/commit/ffaa237663ad3a44ed798539c5c5306f11533ec8))
* use yarn to fix monorepo issues, upgrade docusaurus ([aa4ee48](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa4ee4851b72acc30718974b3642c0932aa56f35))
* white line ([2a5a191](https://bitbucket.org/usereactify/reactify-search-ui/commit/2a5a191872531e8ccdd63ff0135d6474324f3f43))


* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([4a85878](https://bitbucket.org/usereactify/reactify-search-ui/commit/4a858787ad547e87e10d3c3859ce41f87a4c3bbe))

## 5.0.0 (2022-08-05)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5
* switch to using "handle" instead of "id" for sort option
* switch to using "handle" instead of "id" for filter and filter option
* move index to a dedicated prop, make credentials optional and set default

### Features

* add additionalComponentIds prop to provider ([1db51a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1db51a0f905475d2d3399f9de7ff6f025860dce5))
* add breadcrumb for ES search result errors ([22ae301](https://bitbucket.org/usereactify/reactify-search-ui/commit/22ae301bbb7618aec14499970ffd2578ab569a7a))
* add breadcrumb for search query update ([6a2e442](https://bitbucket.org/usereactify/reactify-search-ui/commit/6a2e44269396353fced1d597d48502ef5b1a6bb2))
* add classnames for styling to default components ([75997e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/75997e25756595401401de42e9aa29b235581645))
* add debug output to provider ([c45de1a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c45de1ae681ede827ae899252758171309bac09b))
* add debug output to SensorSort ([0b3d125](https://bitbucket.org/usereactify/reactify-search-ui/commit/0b3d125f29dfe678318d91390d3fbfd13a932947))
* add esbuild for production builds ([9b0a8d5](https://bitbucket.org/usereactify/reactify-search-ui/commit/9b0a8d5daa4a90b96f8760e083431da7e44f6ab5))
* add example components for filter range, filter slider, custom, stats, clear all, filters active ([d5e56fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/d5e56fb114edc3e134c3ce2e90f7f2df67c5d097))
* add example filter list unstyled, radio, checkbox, swatch and box ([a6ebcd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6ebcd0b606f9a8381d1e082dd4935e4a4d83c2f))
* add Filter and FilterList components with hooks ([9cd1be1](https://bitbucket.org/usereactify/reactify-search-ui/commit/9cd1be1b1796962be340fd4a119e21d7d0a04e0e))
* add filterStackId prop to provider ([#2](https://bitbucket.org/usereactify/reactify-search-ui/issues/2)) ([043e9f0](https://bitbucket.org/usereactify/reactify-search-ui/commit/043e9f0745ad1d6e15ec99400df099320fecf7cf))
* add grid gap to result list component props ([0d65bd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d65bd032cc834060fca5702195566a36ef8fb33))
* add hook to get cached or remote config ([cb77750](https://bitbucket.org/usereactify/reactify-search-ui/commit/cb77750f4839134e65afb2eee685755169bfcd5d))
* add image to default result card render ([c50319e](https://bitbucket.org/usereactify/reactify-search-ui/commit/c50319e1011a7465c07b509b5164ea04b9f38684))
* add listClassName prop to ResultList component ([f28be3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/f28be3e4d0f8c74d5e7e20e214aec0ee7867ce7c))
* add local bundle serve ([318443e](https://bitbucket.org/usereactify/reactify-search-ui/commit/318443efc082a5714e99221d4647a91f60d9ac91))
* add onRedirect prop + fix caching issue when searching from search page ([59b32f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/59b32f65c8ede64cbd42d297ed7a4e131b8650eb))
* add pagination, sort, filter and promotion analytics events ([5968814](https://bitbucket.org/usereactify/reactify-search-ui/commit/596881486f05189d90823f0ec1b093580d79a786))
* add renderBooting prop for rendering while config is loading ([3da876e](https://bitbucket.org/usereactify/reactify-search-ui/commit/3da876e5a87ecd98f2abe462994461b7cd39acda))
* add renderResultCard prop to ResultList component ([889255f](https://bitbucket.org/usereactify/reactify-search-ui/commit/889255fc8cb2f1db0a6d86be2159276a0da384b7))
* add ResultPagination component and usePages hook ([#4](https://bitbucket.org/usereactify/reactify-search-ui/issues/4)) ([e8dcf61](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8dcf610ec1eac9b328935a713732284a1f4d7f8))
* add ResultStateProvider component for accessing page state ([ec4cc6f](https://bitbucket.org/usereactify/reactify-search-ui/commit/ec4cc6f08ef2aeae46fe864e61d71ec36e50a0e3))
* add SearchInput component and associated instant search support ([98a8af7](https://bitbucket.org/usereactify/reactify-search-ui/commit/98a8af726f9e781bd03c5ed2dd815c0976b19f21))
* add SensorInventoryAvailable and rename SensorStack ([18d7aaa](https://bitbucket.org/usereactify/reactify-search-ui/commit/18d7aaa4a2d7e82d34014ccc888c624cffba4f03))
* add SensorStackInstantSearch ([e93308f](https://bitbucket.org/usereactify/reactify-search-ui/commit/e93308f06532e7877300c84820e5019b152c895b))
* add sentry error boundary ([c6dbbab](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6dbbabb6e130eb395cf984df655951981ef7496))
* add sentry release tracking to ci ([67c813d](https://bitbucket.org/usereactify/reactify-search-ui/commit/67c813d2c8812cf1b6c4e2ad1ce9b0a38ebce24b))
* add shop name in context, ([13b54e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/13b54e28715b7789cc5cf47d82cc205d175ce1ee))
* add sort option into url params ([f78ec9b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f78ec9b69555df9cb297d818a06d08d6587889c3))
* add support for additional datasearch input props ([a6c6382](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6c63825f6d3e616d57dc0f0af91895475175cc0))
* add support for collections assigned to filter groups ([afe0711](https://bitbucket.org/usereactify/reactify-search-ui/commit/afe071115aa7633f49d5f95673c544989e80f011))
* add support for filter option settingsUppercase ([99d822b](https://bitbucket.org/usereactify/reactify-search-ui/commit/99d822b611ec0302fe314cbe0b1c24653b1ae72c))
* add support for global curation / default boosting rules ([b476e75](https://bitbucket.org/usereactify/reactify-search-ui/commit/b476e75233202bd3ab190d7806cad0b8077463a8))
* add support for pinning products in curations that don't exist within the resultset ([3ceadf6](https://bitbucket.org/usereactify/reactify-search-ui/commit/3ceadf621ab78b216bf355d3e2ec648a6d9fee44))
* add support for settingsHideUnavailable at filter level for variant nested fields ([5ca63e1](https://bitbucket.org/usereactify/reactify-search-ui/commit/5ca63e1dcd35c9f6f80c83d49ab6f8bb87ecaa7d))
* add support for valuesManual in filter option filtering ([c2cef69](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2cef69e491f9852a1250e7218550ac6c1e1671e))
* add totalSelected to filter render props (RS-16) ([7949200](https://bitbucket.org/usereactify/reactify-search-ui/commit/7949200f0307637dda424875e7ca394aa179cdb6))
* add warning and prevent tracking request if "shopifyPermanentDomain" is missing ([bcbf378](https://bitbucket.org/usereactify/reactify-search-ui/commit/bcbf378ac057755347556a60bae58684df8f65c1))
* add zero results and view product (impressions) events ([db4f310](https://bitbucket.org/usereactify/reactify-search-ui/commit/db4f3102c592bc295534bcb5de9523d1f0b26557))
* additional result props in ResultList ([821b022](https://bitbucket.org/usereactify/reactify-search-ui/commit/821b02281baa07b844ce3c6dbba1f8a1ef42dee7))
* adjust page size to accommodate callouts ([0365896](https://bitbucket.org/usereactify/reactify-search-ui/commit/03658965e65ba451a5c419b14599a71296e589c3))
* allow custom grid styles to be provided and override defaults ([25ce405](https://bitbucket.org/usereactify/reactify-search-ui/commit/25ce405300777b9d55a0e806672787bd552438a6))
* allow provider theme to be configured ([33eb10f](https://bitbucket.org/usereactify/reactify-search-ui/commit/33eb10f50dbdaaf2f399ea0e95b673222da868b1))
* **app:** add provider prop "configId" to support multi-config shops ([2ef6af5](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ef6af5a6d46331690bdd3c35ee72193dca8f587))
* apply defaultQuery to nested variant filters to check inventory available ([a348522](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3485220bb3c682d70a6efcd9b520972430a00d7))
* call track after trigger query in SearhInput ([8a09111](https://bitbucket.org/usereactify/reactify-search-ui/commit/8a0911165ea4484bf7bcf65b12c687a0d8b674ca))
* collapse state optional in renderFilterList, rename filterListProps ([9eef3d3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eef3d35ffeaefd568a1d1839bc13b87705e7cb1))
* collection default query ([154e3d2](https://bitbucket.org/usereactify/reactify-search-ui/commit/154e3d2ceadce6625427aa6d7d0cd98ac671a6d2))
* combined results in result list ([a85f39e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a85f39e8d425c51b8108df488145ade8eec1cdf5))
* currency formatting, brand logos and hover image improvements ([9c63fe3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9c63fe33b3d67b2507e2c6f05d7a5450c88132f2))
* default query and sort state ([f20dc5c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f20dc5cf5751ce106bb5fce821e648d1cd1a10c0))
* disable callouts unless enableCallouts prop is set ([c2ac79a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2ac79abffc87d050b766b6786977de43920af17))
* drop SensorStackInstantsearch in favor of single SensorStack with context ([f31a60b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f31a60b9ba6bf2c61d88529b3ceec0b5aa188e1f))
* enable url params for filters ([6f2675f](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f2675f0328de1e3c0cd20172d2053b2776dfb0f))
* filter column titles ([05c41ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/05c41ba8985f2de317fffd9fafc3d883c6dced0c))
* filter matrix and hook ([278d8ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/278d8adfcce10575b90a8b8c16797f167961843b))
* filter options ([106387a](https://bitbucket.org/usereactify/reactify-search-ui/commit/106387aeb38cafa1fdb7132b4c5cf97d08949a5c))
* filter titles and collapse logic ([5469941](https://bitbucket.org/usereactify/reactify-search-ui/commit/546994160fb291b1e15c522730d72d895216033f))
* firebase config ([1038df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1038df0bbc729ca268c715a0c8de6d4667adb4c6))
* hide callouts when not sorting by "_score" or "collections.position" ([bf2d57a](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf2d57aa5b682375dc06995bb54d90ece330f5e3))
* ignore cached config if "nocache" url search param is set ([38823c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/38823c2272fd0a2ae6d45401df35a2148107fe8e))
* import debut theme collection to playground page ([c137569](https://bitbucket.org/usereactify/reactify-search-ui/commit/c137569902b69636e53c6f9cdf297088e18c548a))
* improve classnames ([19fcfa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/19fcfa152c555cb2e78ec62371f1758e2d244557))
* improve default components ([81bb427](https://bitbucket.org/usereactify/reactify-search-ui/commit/81bb427367987125eeea352189dd9f411cb4850f))
* improve dx for filter wrapper, map checked in hook ([3873238](https://bitbucket.org/usereactify/reactify-search-ui/commit/38732380e0a790c92709308b30815d6c0170b27d))
* improve performance when debugging by reusing debugger instances ([aa88917](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa88917b0aedf2abe66439d430587f18dcb72aae))
* improve sentry release process ([4237771](https://bitbucket.org/usereactify/reactify-search-ui/commit/423777120041228dcb6f819f71a00abd08d43987))
* improve sentry stacktraces and logging ([7f34b30](https://bitbucket.org/usereactify/reactify-search-ui/commit/7f34b3064f41ad9a0e5a3ddc3fa071e5981d0141))
* improve sorting of filter list options ([c41eedc](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41eedc900f500d3ddf9434cb14a7397c6385707))
* initial docs ([016fd2b](https://bitbucket.org/usereactify/reactify-search-ui/commit/016fd2ba36d7992bc6c63f9c75315befbfddd625))
* initial ui library components ([803d586](https://bitbucket.org/usereactify/reactify-search-ui/commit/803d586af75217401c34f7df1b2613eb9944c710))
* instant search redirects and sort score ([55d85cc](https://bitbucket.org/usereactify/reactify-search-ui/commit/55d85cc7e60803825a338008c52da95b6d035ab8))
* instantsearch component ([8256cdc](https://bitbucket.org/usereactify/reactify-search-ui/commit/8256cdc9ba0b281bd4135d62240c93180cca8210))
* instantsearch with reactivelist component ([1564b0d](https://bitbucket.org/usereactify/reactify-search-ui/commit/1564b0dd21b25244c635ea1ac903b177aad3bc29))
* merge global boosting rules with curation when empty ([248cbdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/248cbdda01864c0d536f5e971f81e331de4c827d))
* move filter logic to hook, add render props ([333da46](https://bitbucket.org/usereactify/reactify-search-ui/commit/333da4683d93bfeec2c683a43e094179ce5cc39d))
* move index to a dedicated prop, make credentials optional and set default ([a0696ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/a0696ba239b7b036b37a2764a939ce2f26e992b4))
* multilist filter ([34be9bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/34be9bf081c0f24520e634ae2ada863deb8f238b))
* optional collapse state in filter wrapper, add noWrapper prop ([2ea7d1f](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ea7d1feabe26d2c70826b517b94ddac9a972e87))
* pagination ([e50965a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e50965a1cba32ae30bc1545789d551ce94ba4556))
* pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([fb93473](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb93473fa5723a71cfad264f4c4fd09c541ea278))
* product card breakpoints ([657d1b1](https://bitbucket.org/usereactify/reactify-search-ui/commit/657d1b1452f81898552133732086d7e7938f16ca))
* remove combined results logic, discriminate product and callout ([ee1b38e](https://bitbucket.org/usereactify/reactify-search-ui/commit/ee1b38eeddad8eedec473c5b49364b67fa6ae311))
* remove defaultQuery in favor of SensorCollection ([f33c1b2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f33c1b2d1eaf3dfbccd6b8a786e70a56a788bb3e))
* remove event handling overrides in useAnalytics track function ([3cb1bc5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3cb1bc50318f64c2d11013697152d187108d603c))
* rename FilterMatrix to FilterStack ([62788cd](https://bitbucket.org/usereactify/reactify-search-ui/commit/62788cd135e424ab81426ad7e1ae7a3d825d4e28))
* render callouts in result list, implement grid ([e8cbaad](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8cbaad07db3daf2c4a59a2de565774db213d0c6))
* renderAfter, renderBefore and renderPagination props on ResultList ([05d2203](https://bitbucket.org/usereactify/reactify-search-ui/commit/05d2203447d4ca9fb7843dc3ad0b8b8f111b67e6))
* renderError prop on ResultList component ([d95d6df](https://bitbucket.org/usereactify/reactify-search-ui/commit/d95d6df10059badc661eeb1041db76531e44231c))
* renderLoading prop on ResultList component with initial search logic ([1568d2e](https://bitbucket.org/usereactify/reactify-search-ui/commit/1568d2e8729b455b50d42c532cb3bcb91c4e4130))
* renderLoadMoreButton prop on ResultList with default button ([bd5af24](https://bitbucket.org/usereactify/reactify-search-ui/commit/bd5af24daf4fe1e3c41afa5749f10df0d8b64bb2))
* renderPaginationNextPrev prop on ResultList with default component ([d587303](https://bitbucket.org/usereactify/reactify-search-ui/commit/d58730367916629770240c8e85549003ce8ddc28))
* result card component, list logic and hooks ([7c419e9](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c419e9fd1a5fdcdb00ed3289fbcf069d3646710))
* result card promo component ([357e228](https://bitbucket.org/usereactify/reactify-search-ui/commit/357e2281b8fa2d3ace97c3ab8e90e55de87e0e02))
* result list component and hook ([78834ce](https://bitbucket.org/usereactify/reactify-search-ui/commit/78834ce65eb804395c3ea303d3e00f4364c0cdfc))
* search components ([e744e5a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e744e5ae38dfb8bc54603eadf9443c0aaddb1f1e))
* search fields from reactify config, redirect to search page on enter ([dc47d45](https://bitbucket.org/usereactify/reactify-search-ui/commit/dc47d45f39f49bdc386006c238fa823c6fc07cbb))
* **search:** allow searchQuery to be provided to submitSearch ([116e5d6](https://bitbucket.org/usereactify/reactify-search-ui/commit/116e5d60084ae20ccac3bb07d9e17d2158888479))
* **search:** onBlur, onFocus events in SearchInput component ([c6799be](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6799be75c667a47c9c6aac84696040ff0f12acc))
* see all results button in instant search results ([90c6596](https://bitbucket.org/usereactify/reactify-search-ui/commit/90c659646423bc871e37f071682c6e5569cb13f9))
* SensorPublished and SensorSearch ([6c735c1](https://bitbucket.org/usereactify/reactify-search-ui/commit/6c735c11525b4d33e9bca611e82b97ec37ec0b8c))
* SensorSortScore component ([a3050f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3050f6a7c9f1c727ad920c2b68d8e4fbdf933d9))
* set default reset theme ([0661294](https://bitbucket.org/usereactify/reactify-search-ui/commit/0661294a03eae7e70d29a2415df968da38bb0bf8))
* sort context and sensor ([8dde395](https://bitbucket.org/usereactify/reactify-search-ui/commit/8dde3953cc022723fbc58e67d75cdcf3da95d023))
* sort of _score always be desc ([f51e75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f51e75c4f21acb54fbe64e444b26dd02f452c6ac))
* sort sensor and selector component ([b52bcfa](https://bitbucket.org/usereactify/reactify-search-ui/commit/b52bcfa0e796ed8ed5e7e81a068100162243932e))
* support hidden curation products in SensorSort ([c80a52f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c80a52f6e515bb023dd99caedc20ace38444b725))
* switch to using "handle" instead of "id" for filter and filter option ([a92bdd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a92bdd75e68c6291b9f321cec5e77f3934152a9a))
* switch to using "handle" instead of "id" for sort option ([c2be1fa](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2be1fabd4ba1634e47715393c2065a1ca2ba9e7))
* **theme-debut:** add instant search component ([3017fb2](https://bitbucket.org/usereactify/reactify-search-ui/commit/3017fb2d2b2da6413d148e7853e47f7cdf30e3e2))
* **theme-debut:** add styled filters ([467b255](https://bitbucket.org/usereactify/reactify-search-ui/commit/467b2551723aa413696f4060d9b20777a40298e4))
* **theme-debut:** add styling for product cards ([1397aa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/1397aa193eec73592e17b32a5a12d4806a7f808b))
* **theme-debut:** add tailwind jit styles ([6e28c52](https://bitbucket.org/usereactify/reactify-search-ui/commit/6e28c520a2113f68fb0fc37bbedd2083c39ad39f))
* update build commands ([b084e0c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b084e0cf5574e65d0aa3263ddb862901e0a63a06))
* update callout card for new ElasticCallout type ([8db23b5](https://bitbucket.org/usereactify/reactify-search-ui/commit/8db23b5e5b240b45ada520b30a43ff988bcff9d5))
* update provider to use live config hook ([d898df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/d898df01cb21538d9cd5dada9d343967ad91cdd6))
* use emotion cache key to support multiple apps in page ([07a5067](https://bitbucket.org/usereactify/reactify-search-ui/commit/07a5067b339b7c555e3b939b09758df00598698f))
* useAnalytics hook ([17fb749](https://bitbucket.org/usereactify/reactify-search-ui/commit/17fb749e763870a2888c3cf68ceeff3cd0d8fc21))
* useReactiveBaseProps hook and restructure ReactiveBase inclusion ([51873b3](https://bitbucket.org/usereactify/reactify-search-ui/commit/51873b39abe927115487d24c6140be21a2b82c8a))


### Bug Fixes

* add missing lower case to existing sort sensor ([c7099a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/c7099a0edf57658fa006ac018b017807891a44dd))
* add missing next/prev pagination export ([6b13902](https://bitbucket.org/usereactify/reactify-search-ui/commit/6b139027cc1b5038add4d688a9301add2f6e75ee))
* add missing package "axios" ([eaa4763](https://bitbucket.org/usereactify/reactify-search-ui/commit/eaa476367ef0947b20c24fe4001efb4550184516))
* add missing sensors to result list react prop ([2293c43](https://bitbucket.org/usereactify/reactify-search-ui/commit/2293c4391917a536e31fc3f3fce08703ab134e6b))
* add missing spread operator ([acc51cb](https://bitbucket.org/usereactify/reactify-search-ui/commit/acc51cb5c414831e3d0e919185d076895903d837))
* add nested attribute to collection position sort clause (RS-124) ([0932b3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0932b3e6e64dd2a0ef9df592c431f0dc5340fcc1))
* add timeout to blur event so results remain clickable ([fe98040](https://bitbucket.org/usereactify/reactify-search-ui/commit/fe980407427256246051bee8be152048ae841cf2))
* **app:** fix access to undefined sort option ID ([f93bbce](https://bitbucket.org/usereactify/reactify-search-ui/commit/f93bbcec82ee61d4a0bf19d7ab18894339f8a27c))
* case insensitive redirect match ([#11](https://bitbucket.org/usereactify/reactify-search-ui/issues/11)) ([096c7fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/096c7fb84d77ae0d985b2e7cf4ade6c80e3171e7))
* check compare_at_price exists before formatting in useProductPrice ([acba76c](https://bitbucket.org/usereactify/reactify-search-ui/commit/acba76c6d8e0c5ba1482fe230e20fcd286f11e69))
* check for existence of window before using (node render) ([df7f9b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/df7f9b0c0c29d561e69107c8086a5d0fa023aaee))
* check that window is defined before accessing ([e1ec0aa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1ec0aae22cab7eb86363ed09d47e422d08ff5c1))
* correct logic for calculating search fields when config is empty or updates ([ae67498](https://bitbucket.org/usereactify/reactify-search-ui/commit/ae6749875f295db99f012b54cc06e10346f702d0))
* correct tsconfig ([e82defa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e82defac47d754df543fc276319ba759de607507))
* correct tsconfig ([01a796b](https://bitbucket.org/usereactify/reactify-search-ui/commit/01a796bfc03f4153d8fbbea86a0597ee3c665a94))
* correct typings for selected sort option within context ([f363fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f363fc2c8ea1ad88e65844c382620b927614c403))
* define nested field prop for variants ([2af7ebe](https://bitbucket.org/usereactify/reactify-search-ui/commit/2af7ebe6b33c1ca19faf27ca0f525fe1f1669ced))
* disable scrollOnChange prop ([50592e0](https://bitbucket.org/usereactify/reactify-search-ui/commit/50592e0db250bfae5809b0a92e9c5f72e43b4d88))
* dom warnings ([555fb78](https://bitbucket.org/usereactify/reactify-search-ui/commit/555fb783e44d28261c701e2b5896533b5b2f6743))
* enable typescript inlinesources ([6998e8e](https://bitbucket.org/usereactify/reactify-search-ui/commit/6998e8e3e884d62cf08b08c43ae7dbc73db7f471))
* export Filter and FilterList components ([7a297dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/7a297dd630c3582d5bf9ae32eb6d31663ee2654d))
* filter currentId from all filters in useReactiveReactProp ([645fe73](https://bitbucket.org/usereactify/reactify-search-ui/commit/645fe7373f469255d15bbe12c05d478193053ac6))
* filter styling updates ([c5c3b8f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c5c3b8f9e4fa349fac05e288bd146053802396a1))
* fix request payload ([aa33e95](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa33e95ba1162ec0bbb84cd25c2e4483f3822455))
* fix search event payload ([5bd8c03](https://bitbucket.org/usereactify/reactify-search-ui/commit/5bd8c03e024f3ff0fb14135b9dbc60a9c709fb38))
* force lower case curation search term ([09d73bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/09d73bf8b86942f8fb67d9283e55bee6e99ae963))
* force value to be an array in filter customQuery ([454c110](https://bitbucket.org/usereactify/reactify-search-ui/commit/454c11023a3386c00ad4bcec33ba04d94c86c330))
* include sorting by collection position when using global curation ([141c463](https://bitbucket.org/usereactify/reactify-search-ui/commit/141c46351711c3cff65275e47fdf8e9ed64b0ad8))
* incorrect hasSelected calculation from filter value ([bf1e8c8](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf1e8c84a54164bd905931743356d301b3b2b5d6))
* instant search sorting ([b6ce036](https://bitbucket.org/usereactify/reactify-search-ui/commit/b6ce036d06d72dbbe28fb1892537ee399ee77c0c))
* make collection nodes optional ([3b2afe5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3b2afe5432d50ba2f8ee57f62478708e113d11d1))
* match useAnalytics endpoint signature ([b94a70c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b94a70c9b1a1f8acc9a11bbff8e6414e77d34657))
* normalise collection handle and search term when resolving curation ([6bd1aec](https://bitbucket.org/usereactify/reactify-search-ui/commit/6bd1aecd163743bdcfc9582389f35a4dc579930c))
* only apply inventory sensor to products, not callouts ([7c186b8](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c186b83c9f403f0987ef088714db1438f27cd84))
* only record select event when link clicked ([ba95992](https://bitbucket.org/usereactify/reactify-search-ui/commit/ba959923785837ed54053257012a1cfdcc69be59))
* only request live config if shop is present ([a9ab343](https://bitbucket.org/usereactify/reactify-search-ui/commit/a9ab343aef7790650635517a83fe4889e87427a3))
* only run tracking event when data exists ([a768c19](https://bitbucket.org/usereactify/reactify-search-ui/commit/a768c1931fd52e69d5538760547c50d610bcb8cf))
* pagination spacing ([945f23a](https://bitbucket.org/usereactify/reactify-search-ui/commit/945f23acfb9e9f39b343b51cad53bf57c8df8522))
* pass on nocache url param to config endpoint to bust caching on worker side ([51b9dcf](https://bitbucket.org/usereactify/reactify-search-ui/commit/51b9dcf0b8145691a2dd00aedbe951f71bbc04b3))
* pass render prop to ResultCardCalloutInner, add pagePosition prop ([3983db4](https://bitbucket.org/usereactify/reactify-search-ui/commit/3983db4c471a3172259662f03e400c3f8ed14b0d))
* product card image margins ([8417270](https://bitbucket.org/usereactify/reactify-search-ui/commit/8417270a3a829e525c6d4b330018f893dbe4fb01))
* product card negative margin ([0573eb0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0573eb02b9bdac3ada0c9ad6f58d91980cba4ef2))
* product card text styling and alignment ([ef16770](https://bitbucket.org/usereactify/reactify-search-ui/commit/ef167700cccf3f094fed8329a6c3471f9af54c6a))
* re-pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([77bbe4c](https://bitbucket.org/usereactify/reactify-search-ui/commit/77bbe4cb340d0afc2e76c6f584675db5322fee29))
* react results component to SensorCollection ([c2833e6](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2833e6188958b054c39fce7cceefc154cc3e66c))
* regenerate yarn.lock in release script ([b97ddef](https://bitbucket.org/usereactify/reactify-search-ui/commit/b97ddefae363a25303afc90042044f0692fac3c5))
* remove console error for range filter ([9eeeb5d](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eeeb5df6b1e71ce1d9ce784259b141988c450fb))
* remove name and variant from select_item event ([a87037d](https://bitbucket.org/usereactify/reactify-search-ui/commit/a87037d493a269fd402f68431fa4fd0c90911f70))
* remove problematic css color "inherit" ([aa304bd](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa304bde76528d54722255b0e38ef732f58218b6))
* render issues with expired config cache ([2c408ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/2c408adce950da369db77ff8ece5a8d08cd92d5f))
* result card image alignment ([46aedd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/46aedd030f4e623bee3d28647f97c100648cd8ef))
* **search:** correct type for context submitSearch ([8b3e647](https://bitbucket.org/usereactify/reactify-search-ui/commit/8b3e64716b6d82e14efc8abb88ff1f6013d5c182))
* **search:** include _score in sort state eligible for curations ([2839929](https://bitbucket.org/usereactify/reactify-search-ui/commit/2839929be960aabd0b45e4aad0ce6a5abdc776e6))
* **search:** ssr build issue due to window ([aada68a](https://bitbucket.org/usereactify/reactify-search-ui/commit/aada68a20cdb8cffe43abbde487229819fdd39e3))
* select_item event, add questions in comments ([8693a78](https://bitbucket.org/usereactify/reactify-search-ui/commit/8693a781049ee44d429f4bf6149532737d9ccbb8))
* set base url for docs ([11fca33](https://bitbucket.org/usereactify/reactify-search-ui/commit/11fca33ea6e9a2b137b758f67206194b9b4e574b))
* set product images to large via cdn ([d1c311f](https://bitbucket.org/usereactify/reactify-search-ui/commit/d1c311fcb093b2ba0ec31adf3677da079328c833))
* show all filter options unless displaySize is defined ([6887ed7](https://bitbucket.org/usereactify/reactify-search-ui/commit/6887ed70737b864cf87cf97433626b9e5ecb7c98))
* size and colour filter targets in docs ([735c12e](https://bitbucket.org/usereactify/reactify-search-ui/commit/735c12ed29683d324651382f4fad55c16941e716))
* ssr playground issues ([5cdb445](https://bitbucket.org/usereactify/reactify-search-ui/commit/5cdb4458ca5db1526a3e00eeff99b7e071dd3ae2))
* support missing compare_at_price on price sets ([dbe3fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/dbe3fc20db23e1969f0d0c9ee75e646cce3f1858))
* support v1 indexes in ResultList component ([4c23ef3](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c23ef3757daeb505823f4af6399746a29404f7a))
* switch to reactify sentry org dsn ([cae3cf0](https://bitbucket.org/usereactify/reactify-search-ui/commit/cae3cf02721ee8b40ddf13854e7aa3cea88034f4))
* text styling for filter titles ([7c0d15c](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c0d15cbd05ace9df056b260c2978146109270a7))
* **theme-debut:** browser warning for svg property ([4fd7948](https://bitbucket.org/usereactify/reactify-search-ui/commit/4fd794848b4b15becb601718f635005407db6d79))
* **theme-debut:** fix flex width on results grid ([049da94](https://bitbucket.org/usereactify/reactify-search-ui/commit/049da942aa87c7a887d689f78f77da02b9b63422))
* **theme-debut:** use new SensorStack component ([da59639](https://bitbucket.org/usereactify/reactify-search-ui/commit/da5963989b5012a231df3ea572f02095c4cc8489))
* tighten up instantsearch debounce solution ([6f811ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f811ca9350165c8858ae3388eac09af56a32cfd))
* update filter resolution to return first filter if none found with type ([e7990d1](https://bitbucket.org/usereactify/reactify-search-ui/commit/e7990d1727dd019fba19f9a8132109c992a9a57e))
* update provider props to suit merged changes ([f014e87](https://bitbucket.org/usereactify/reactify-search-ui/commit/f014e87e14776577d0f83d24a5c94aa6d3b15818))
* use american color variation ([57f9b0e](https://bitbucket.org/usereactify/reactify-search-ui/commit/57f9b0e6de4482fd451bd36a1b49e47bb8d28f8e))
* use collection for filter for both filter and search until configurable ([007def5](https://bitbucket.org/usereactify/reactify-search-ui/commit/007def512972b88ae1750157184436e11aca2691))
* use commonjs ([b742b8b](https://bitbucket.org/usereactify/reactify-search-ui/commit/b742b8b268c0a663361147efb93bf120a208a708))
* use correct glob pattern for artifacts ([9f35c7c](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f35c7ccea4f957a94b172fe7ac62b53faf73b32))
* use correct names of components and imports ([e180a75](https://bitbucket.org/usereactify/reactify-search-ui/commit/e180a75b0c4be555a61d72e48a54434b7aa27960))
* use docusaurus router for index docs redirect ([167403b](https://bitbucket.org/usereactify/reactify-search-ui/commit/167403b4aa2f53c57f4cbc69c0a00f78fb46afd2))
* use variants.available in SensorInventoryAvailable ([ffaa237](https://bitbucket.org/usereactify/reactify-search-ui/commit/ffaa237663ad3a44ed798539c5c5306f11533ec8))
* use yarn to fix monorepo issues, upgrade docusaurus ([aa4ee48](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa4ee4851b72acc30718974b3642c0932aa56f35))
* white line ([2a5a191](https://bitbucket.org/usereactify/reactify-search-ui/commit/2a5a191872531e8ccdd63ff0135d6474324f3f43))


* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([4a85878](https://bitbucket.org/usereactify/reactify-search-ui/commit/4a858787ad547e87e10d3c3859ce41f87a4c3bbe))

## 5.0.0-beta.16 (2022-08-05)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5
* switch to using "handle" instead of "id" for sort option
* switch to using "handle" instead of "id" for filter and filter option
* move index to a dedicated prop, make credentials optional and set default

### Features

* add additionalComponentIds prop to provider ([1db51a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1db51a0f905475d2d3399f9de7ff6f025860dce5))
* add breadcrumb for ES search result errors ([22ae301](https://bitbucket.org/usereactify/reactify-search-ui/commit/22ae301bbb7618aec14499970ffd2578ab569a7a))
* add breadcrumb for search query update ([6a2e442](https://bitbucket.org/usereactify/reactify-search-ui/commit/6a2e44269396353fced1d597d48502ef5b1a6bb2))
* add classnames for styling to default components ([75997e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/75997e25756595401401de42e9aa29b235581645))
* add debug output to provider ([c45de1a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c45de1ae681ede827ae899252758171309bac09b))
* add debug output to SensorSort ([0b3d125](https://bitbucket.org/usereactify/reactify-search-ui/commit/0b3d125f29dfe678318d91390d3fbfd13a932947))
* add esbuild for production builds ([9b0a8d5](https://bitbucket.org/usereactify/reactify-search-ui/commit/9b0a8d5daa4a90b96f8760e083431da7e44f6ab5))
* add example components for filter range, filter slider, custom, stats, clear all, filters active ([d5e56fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/d5e56fb114edc3e134c3ce2e90f7f2df67c5d097))
* add example filter list unstyled, radio, checkbox, swatch and box ([a6ebcd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6ebcd0b606f9a8381d1e082dd4935e4a4d83c2f))
* add Filter and FilterList components with hooks ([9cd1be1](https://bitbucket.org/usereactify/reactify-search-ui/commit/9cd1be1b1796962be340fd4a119e21d7d0a04e0e))
* add filterStackId prop to provider ([#2](https://bitbucket.org/usereactify/reactify-search-ui/issues/2)) ([043e9f0](https://bitbucket.org/usereactify/reactify-search-ui/commit/043e9f0745ad1d6e15ec99400df099320fecf7cf))
* add grid gap to result list component props ([0d65bd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d65bd032cc834060fca5702195566a36ef8fb33))
* add hook to get cached or remote config ([cb77750](https://bitbucket.org/usereactify/reactify-search-ui/commit/cb77750f4839134e65afb2eee685755169bfcd5d))
* add image to default result card render ([c50319e](https://bitbucket.org/usereactify/reactify-search-ui/commit/c50319e1011a7465c07b509b5164ea04b9f38684))
* add listClassName prop to ResultList component ([f28be3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/f28be3e4d0f8c74d5e7e20e214aec0ee7867ce7c))
* add local bundle serve ([318443e](https://bitbucket.org/usereactify/reactify-search-ui/commit/318443efc082a5714e99221d4647a91f60d9ac91))
* add onRedirect prop + fix caching issue when searching from search page ([59b32f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/59b32f65c8ede64cbd42d297ed7a4e131b8650eb))
* add pagination, sort, filter and promotion analytics events ([5968814](https://bitbucket.org/usereactify/reactify-search-ui/commit/596881486f05189d90823f0ec1b093580d79a786))
* add renderBooting prop for rendering while config is loading ([3da876e](https://bitbucket.org/usereactify/reactify-search-ui/commit/3da876e5a87ecd98f2abe462994461b7cd39acda))
* add renderResultCard prop to ResultList component ([889255f](https://bitbucket.org/usereactify/reactify-search-ui/commit/889255fc8cb2f1db0a6d86be2159276a0da384b7))
* add ResultPagination component and usePages hook ([#4](https://bitbucket.org/usereactify/reactify-search-ui/issues/4)) ([e8dcf61](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8dcf610ec1eac9b328935a713732284a1f4d7f8))
* add ResultStateProvider component for accessing page state ([ec4cc6f](https://bitbucket.org/usereactify/reactify-search-ui/commit/ec4cc6f08ef2aeae46fe864e61d71ec36e50a0e3))
* add SearchInput component and associated instant search support ([98a8af7](https://bitbucket.org/usereactify/reactify-search-ui/commit/98a8af726f9e781bd03c5ed2dd815c0976b19f21))
* add SensorInventoryAvailable and rename SensorStack ([18d7aaa](https://bitbucket.org/usereactify/reactify-search-ui/commit/18d7aaa4a2d7e82d34014ccc888c624cffba4f03))
* add SensorStackInstantSearch ([e93308f](https://bitbucket.org/usereactify/reactify-search-ui/commit/e93308f06532e7877300c84820e5019b152c895b))
* add sentry error boundary ([c6dbbab](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6dbbabb6e130eb395cf984df655951981ef7496))
* add sentry release tracking to ci ([67c813d](https://bitbucket.org/usereactify/reactify-search-ui/commit/67c813d2c8812cf1b6c4e2ad1ce9b0a38ebce24b))
* add shop name in context, ([13b54e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/13b54e28715b7789cc5cf47d82cc205d175ce1ee))
* add sort option into url params ([f78ec9b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f78ec9b69555df9cb297d818a06d08d6587889c3))
* add support for additional datasearch input props ([a6c6382](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6c63825f6d3e616d57dc0f0af91895475175cc0))
* add support for collections assigned to filter groups ([afe0711](https://bitbucket.org/usereactify/reactify-search-ui/commit/afe071115aa7633f49d5f95673c544989e80f011))
* add support for filter option settingsUppercase ([99d822b](https://bitbucket.org/usereactify/reactify-search-ui/commit/99d822b611ec0302fe314cbe0b1c24653b1ae72c))
* add support for global curation / default boosting rules ([b476e75](https://bitbucket.org/usereactify/reactify-search-ui/commit/b476e75233202bd3ab190d7806cad0b8077463a8))
* add support for pinning products in curations that don't exist within the resultset ([3ceadf6](https://bitbucket.org/usereactify/reactify-search-ui/commit/3ceadf621ab78b216bf355d3e2ec648a6d9fee44))
* add support for settingsHideUnavailable at filter level for variant nested fields ([5ca63e1](https://bitbucket.org/usereactify/reactify-search-ui/commit/5ca63e1dcd35c9f6f80c83d49ab6f8bb87ecaa7d))
* add support for valuesManual in filter option filtering ([c2cef69](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2cef69e491f9852a1250e7218550ac6c1e1671e))
* add totalSelected to filter render props (RS-16) ([7949200](https://bitbucket.org/usereactify/reactify-search-ui/commit/7949200f0307637dda424875e7ca394aa179cdb6))
* add warning and prevent tracking request if "shopifyPermanentDomain" is missing ([bcbf378](https://bitbucket.org/usereactify/reactify-search-ui/commit/bcbf378ac057755347556a60bae58684df8f65c1))
* add zero results and view product (impressions) events ([db4f310](https://bitbucket.org/usereactify/reactify-search-ui/commit/db4f3102c592bc295534bcb5de9523d1f0b26557))
* additional result props in ResultList ([821b022](https://bitbucket.org/usereactify/reactify-search-ui/commit/821b02281baa07b844ce3c6dbba1f8a1ef42dee7))
* adjust page size to accommodate callouts ([0365896](https://bitbucket.org/usereactify/reactify-search-ui/commit/03658965e65ba451a5c419b14599a71296e589c3))
* allow custom grid styles to be provided and override defaults ([25ce405](https://bitbucket.org/usereactify/reactify-search-ui/commit/25ce405300777b9d55a0e806672787bd552438a6))
* allow provider theme to be configured ([33eb10f](https://bitbucket.org/usereactify/reactify-search-ui/commit/33eb10f50dbdaaf2f399ea0e95b673222da868b1))
* **app:** add provider prop "configId" to support multi-config shops ([2ef6af5](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ef6af5a6d46331690bdd3c35ee72193dca8f587))
* apply defaultQuery to nested variant filters to check inventory available ([a348522](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3485220bb3c682d70a6efcd9b520972430a00d7))
* call track after trigger query in SearhInput ([8a09111](https://bitbucket.org/usereactify/reactify-search-ui/commit/8a0911165ea4484bf7bcf65b12c687a0d8b674ca))
* collapse state optional in renderFilterList, rename filterListProps ([9eef3d3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eef3d35ffeaefd568a1d1839bc13b87705e7cb1))
* collection default query ([154e3d2](https://bitbucket.org/usereactify/reactify-search-ui/commit/154e3d2ceadce6625427aa6d7d0cd98ac671a6d2))
* combined results in result list ([a85f39e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a85f39e8d425c51b8108df488145ade8eec1cdf5))
* currency formatting, brand logos and hover image improvements ([9c63fe3](https://bitbucket.org/usereactify/reactify-search-ui/commit/9c63fe33b3d67b2507e2c6f05d7a5450c88132f2))
* default query and sort state ([f20dc5c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f20dc5cf5751ce106bb5fce821e648d1cd1a10c0))
* disable callouts unless enableCallouts prop is set ([c2ac79a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2ac79abffc87d050b766b6786977de43920af17))
* drop SensorStackInstantsearch in favor of single SensorStack with context ([f31a60b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f31a60b9ba6bf2c61d88529b3ceec0b5aa188e1f))
* enable url params for filters ([6f2675f](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f2675f0328de1e3c0cd20172d2053b2776dfb0f))
* filter column titles ([05c41ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/05c41ba8985f2de317fffd9fafc3d883c6dced0c))
* filter matrix and hook ([278d8ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/278d8adfcce10575b90a8b8c16797f167961843b))
* filter options ([106387a](https://bitbucket.org/usereactify/reactify-search-ui/commit/106387aeb38cafa1fdb7132b4c5cf97d08949a5c))
* filter titles and collapse logic ([5469941](https://bitbucket.org/usereactify/reactify-search-ui/commit/546994160fb291b1e15c522730d72d895216033f))
* firebase config ([1038df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1038df0bbc729ca268c715a0c8de6d4667adb4c6))
* hide callouts when not sorting by "_score" or "collections.position" ([bf2d57a](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf2d57aa5b682375dc06995bb54d90ece330f5e3))
* ignore cached config if "nocache" url search param is set ([38823c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/38823c2272fd0a2ae6d45401df35a2148107fe8e))
* import debut theme collection to playground page ([c137569](https://bitbucket.org/usereactify/reactify-search-ui/commit/c137569902b69636e53c6f9cdf297088e18c548a))
* improve classnames ([19fcfa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/19fcfa152c555cb2e78ec62371f1758e2d244557))
* improve default components ([81bb427](https://bitbucket.org/usereactify/reactify-search-ui/commit/81bb427367987125eeea352189dd9f411cb4850f))
* improve dx for filter wrapper, map checked in hook ([3873238](https://bitbucket.org/usereactify/reactify-search-ui/commit/38732380e0a790c92709308b30815d6c0170b27d))
* improve performance when debugging by reusing debugger instances ([aa88917](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa88917b0aedf2abe66439d430587f18dcb72aae))
* improve sentry release process ([4237771](https://bitbucket.org/usereactify/reactify-search-ui/commit/423777120041228dcb6f819f71a00abd08d43987))
* improve sentry stacktraces and logging ([7f34b30](https://bitbucket.org/usereactify/reactify-search-ui/commit/7f34b3064f41ad9a0e5a3ddc3fa071e5981d0141))
* improve sorting of filter list options ([c41eedc](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41eedc900f500d3ddf9434cb14a7397c6385707))
* initial docs ([016fd2b](https://bitbucket.org/usereactify/reactify-search-ui/commit/016fd2ba36d7992bc6c63f9c75315befbfddd625))
* initial ui library components ([803d586](https://bitbucket.org/usereactify/reactify-search-ui/commit/803d586af75217401c34f7df1b2613eb9944c710))
* instant search redirects and sort score ([55d85cc](https://bitbucket.org/usereactify/reactify-search-ui/commit/55d85cc7e60803825a338008c52da95b6d035ab8))
* instantsearch component ([8256cdc](https://bitbucket.org/usereactify/reactify-search-ui/commit/8256cdc9ba0b281bd4135d62240c93180cca8210))
* instantsearch with reactivelist component ([1564b0d](https://bitbucket.org/usereactify/reactify-search-ui/commit/1564b0dd21b25244c635ea1ac903b177aad3bc29))
* merge global boosting rules with curation when empty ([248cbdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/248cbdda01864c0d536f5e971f81e331de4c827d))
* move filter logic to hook, add render props ([333da46](https://bitbucket.org/usereactify/reactify-search-ui/commit/333da4683d93bfeec2c683a43e094179ce5cc39d))
* move index to a dedicated prop, make credentials optional and set default ([a0696ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/a0696ba239b7b036b37a2764a939ce2f26e992b4))
* multilist filter ([34be9bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/34be9bf081c0f24520e634ae2ada863deb8f238b))
* optional collapse state in filter wrapper, add noWrapper prop ([2ea7d1f](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ea7d1feabe26d2c70826b517b94ddac9a972e87))
* pagination ([e50965a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e50965a1cba32ae30bc1545789d551ce94ba4556))
* pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([fb93473](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb93473fa5723a71cfad264f4c4fd09c541ea278))
* product card breakpoints ([657d1b1](https://bitbucket.org/usereactify/reactify-search-ui/commit/657d1b1452f81898552133732086d7e7938f16ca))
* remove combined results logic, discriminate product and callout ([ee1b38e](https://bitbucket.org/usereactify/reactify-search-ui/commit/ee1b38eeddad8eedec473c5b49364b67fa6ae311))
* remove defaultQuery in favor of SensorCollection ([f33c1b2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f33c1b2d1eaf3dfbccd6b8a786e70a56a788bb3e))
* remove event handling overrides in useAnalytics track function ([3cb1bc5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3cb1bc50318f64c2d11013697152d187108d603c))
* rename FilterMatrix to FilterStack ([62788cd](https://bitbucket.org/usereactify/reactify-search-ui/commit/62788cd135e424ab81426ad7e1ae7a3d825d4e28))
* render callouts in result list, implement grid ([e8cbaad](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8cbaad07db3daf2c4a59a2de565774db213d0c6))
* renderAfter, renderBefore and renderPagination props on ResultList ([05d2203](https://bitbucket.org/usereactify/reactify-search-ui/commit/05d2203447d4ca9fb7843dc3ad0b8b8f111b67e6))
* renderError prop on ResultList component ([d95d6df](https://bitbucket.org/usereactify/reactify-search-ui/commit/d95d6df10059badc661eeb1041db76531e44231c))
* renderLoading prop on ResultList component with initial search logic ([1568d2e](https://bitbucket.org/usereactify/reactify-search-ui/commit/1568d2e8729b455b50d42c532cb3bcb91c4e4130))
* renderLoadMoreButton prop on ResultList with default button ([bd5af24](https://bitbucket.org/usereactify/reactify-search-ui/commit/bd5af24daf4fe1e3c41afa5749f10df0d8b64bb2))
* renderPaginationNextPrev prop on ResultList with default component ([d587303](https://bitbucket.org/usereactify/reactify-search-ui/commit/d58730367916629770240c8e85549003ce8ddc28))
* result card component, list logic and hooks ([7c419e9](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c419e9fd1a5fdcdb00ed3289fbcf069d3646710))
* result card promo component ([357e228](https://bitbucket.org/usereactify/reactify-search-ui/commit/357e2281b8fa2d3ace97c3ab8e90e55de87e0e02))
* result list component and hook ([78834ce](https://bitbucket.org/usereactify/reactify-search-ui/commit/78834ce65eb804395c3ea303d3e00f4364c0cdfc))
* search components ([e744e5a](https://bitbucket.org/usereactify/reactify-search-ui/commit/e744e5ae38dfb8bc54603eadf9443c0aaddb1f1e))
* search fields from reactify config, redirect to search page on enter ([dc47d45](https://bitbucket.org/usereactify/reactify-search-ui/commit/dc47d45f39f49bdc386006c238fa823c6fc07cbb))
* **search:** allow searchQuery to be provided to submitSearch ([116e5d6](https://bitbucket.org/usereactify/reactify-search-ui/commit/116e5d60084ae20ccac3bb07d9e17d2158888479))
* **search:** onBlur, onFocus events in SearchInput component ([c6799be](https://bitbucket.org/usereactify/reactify-search-ui/commit/c6799be75c667a47c9c6aac84696040ff0f12acc))
* see all results button in instant search results ([90c6596](https://bitbucket.org/usereactify/reactify-search-ui/commit/90c659646423bc871e37f071682c6e5569cb13f9))
* SensorPublished and SensorSearch ([6c735c1](https://bitbucket.org/usereactify/reactify-search-ui/commit/6c735c11525b4d33e9bca611e82b97ec37ec0b8c))
* SensorSortScore component ([a3050f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/a3050f6a7c9f1c727ad920c2b68d8e4fbdf933d9))
* set default reset theme ([0661294](https://bitbucket.org/usereactify/reactify-search-ui/commit/0661294a03eae7e70d29a2415df968da38bb0bf8))
* sort context and sensor ([8dde395](https://bitbucket.org/usereactify/reactify-search-ui/commit/8dde3953cc022723fbc58e67d75cdcf3da95d023))
* sort of _score always be desc ([f51e75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f51e75c4f21acb54fbe64e444b26dd02f452c6ac))
* sort sensor and selector component ([b52bcfa](https://bitbucket.org/usereactify/reactify-search-ui/commit/b52bcfa0e796ed8ed5e7e81a068100162243932e))
* support hidden curation products in SensorSort ([c80a52f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c80a52f6e515bb023dd99caedc20ace38444b725))
* switch to using "handle" instead of "id" for filter and filter option ([a92bdd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a92bdd75e68c6291b9f321cec5e77f3934152a9a))
* switch to using "handle" instead of "id" for sort option ([c2be1fa](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2be1fabd4ba1634e47715393c2065a1ca2ba9e7))
* **theme-debut:** add instant search component ([3017fb2](https://bitbucket.org/usereactify/reactify-search-ui/commit/3017fb2d2b2da6413d148e7853e47f7cdf30e3e2))
* **theme-debut:** add styled filters ([467b255](https://bitbucket.org/usereactify/reactify-search-ui/commit/467b2551723aa413696f4060d9b20777a40298e4))
* **theme-debut:** add styling for product cards ([1397aa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/1397aa193eec73592e17b32a5a12d4806a7f808b))
* **theme-debut:** add tailwind jit styles ([6e28c52](https://bitbucket.org/usereactify/reactify-search-ui/commit/6e28c520a2113f68fb0fc37bbedd2083c39ad39f))
* update build commands ([b084e0c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b084e0cf5574e65d0aa3263ddb862901e0a63a06))
* update callout card for new ElasticCallout type ([8db23b5](https://bitbucket.org/usereactify/reactify-search-ui/commit/8db23b5e5b240b45ada520b30a43ff988bcff9d5))
* update provider to use live config hook ([d898df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/d898df01cb21538d9cd5dada9d343967ad91cdd6))
* use emotion cache key to support multiple apps in page ([07a5067](https://bitbucket.org/usereactify/reactify-search-ui/commit/07a5067b339b7c555e3b939b09758df00598698f))
* useAnalytics hook ([17fb749](https://bitbucket.org/usereactify/reactify-search-ui/commit/17fb749e763870a2888c3cf68ceeff3cd0d8fc21))
* useReactiveBaseProps hook and restructure ReactiveBase inclusion ([51873b3](https://bitbucket.org/usereactify/reactify-search-ui/commit/51873b39abe927115487d24c6140be21a2b82c8a))


### Bug Fixes

* add missing lower case to existing sort sensor ([c7099a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/c7099a0edf57658fa006ac018b017807891a44dd))
* add missing next/prev pagination export ([6b13902](https://bitbucket.org/usereactify/reactify-search-ui/commit/6b139027cc1b5038add4d688a9301add2f6e75ee))
* add missing package "axios" ([eaa4763](https://bitbucket.org/usereactify/reactify-search-ui/commit/eaa476367ef0947b20c24fe4001efb4550184516))
* add missing sensors to result list react prop ([2293c43](https://bitbucket.org/usereactify/reactify-search-ui/commit/2293c4391917a536e31fc3f3fce08703ab134e6b))
* add missing spread operator ([acc51cb](https://bitbucket.org/usereactify/reactify-search-ui/commit/acc51cb5c414831e3d0e919185d076895903d837))
* add nested attribute to collection position sort clause (RS-124) ([0932b3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0932b3e6e64dd2a0ef9df592c431f0dc5340fcc1))
* add timeout to blur event so results remain clickable ([fe98040](https://bitbucket.org/usereactify/reactify-search-ui/commit/fe980407427256246051bee8be152048ae841cf2))
* **app:** fix access to undefined sort option ID ([f93bbce](https://bitbucket.org/usereactify/reactify-search-ui/commit/f93bbcec82ee61d4a0bf19d7ab18894339f8a27c))
* case insensitive redirect match ([#11](https://bitbucket.org/usereactify/reactify-search-ui/issues/11)) ([096c7fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/096c7fb84d77ae0d985b2e7cf4ade6c80e3171e7))
* check compare_at_price exists before formatting in useProductPrice ([acba76c](https://bitbucket.org/usereactify/reactify-search-ui/commit/acba76c6d8e0c5ba1482fe230e20fcd286f11e69))
* check for existence of window before using (node render) ([df7f9b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/df7f9b0c0c29d561e69107c8086a5d0fa023aaee))
* check that window is defined before accessing ([e1ec0aa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1ec0aae22cab7eb86363ed09d47e422d08ff5c1))
* correct logic for calculating search fields when config is empty or updates ([ae67498](https://bitbucket.org/usereactify/reactify-search-ui/commit/ae6749875f295db99f012b54cc06e10346f702d0))
* correct tsconfig ([e82defa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e82defac47d754df543fc276319ba759de607507))
* correct tsconfig ([01a796b](https://bitbucket.org/usereactify/reactify-search-ui/commit/01a796bfc03f4153d8fbbea86a0597ee3c665a94))
* correct typings for selected sort option within context ([f363fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f363fc2c8ea1ad88e65844c382620b927614c403))
* define nested field prop for variants ([2af7ebe](https://bitbucket.org/usereactify/reactify-search-ui/commit/2af7ebe6b33c1ca19faf27ca0f525fe1f1669ced))
* disable scrollOnChange prop ([50592e0](https://bitbucket.org/usereactify/reactify-search-ui/commit/50592e0db250bfae5809b0a92e9c5f72e43b4d88))
* dom warnings ([555fb78](https://bitbucket.org/usereactify/reactify-search-ui/commit/555fb783e44d28261c701e2b5896533b5b2f6743))
* enable typescript inlinesources ([6998e8e](https://bitbucket.org/usereactify/reactify-search-ui/commit/6998e8e3e884d62cf08b08c43ae7dbc73db7f471))
* export Filter and FilterList components ([7a297dd](https://bitbucket.org/usereactify/reactify-search-ui/commit/7a297dd630c3582d5bf9ae32eb6d31663ee2654d))
* filter currentId from all filters in useReactiveReactProp ([645fe73](https://bitbucket.org/usereactify/reactify-search-ui/commit/645fe7373f469255d15bbe12c05d478193053ac6))
* filter styling updates ([c5c3b8f](https://bitbucket.org/usereactify/reactify-search-ui/commit/c5c3b8f9e4fa349fac05e288bd146053802396a1))
* fix request payload ([aa33e95](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa33e95ba1162ec0bbb84cd25c2e4483f3822455))
* fix search event payload ([5bd8c03](https://bitbucket.org/usereactify/reactify-search-ui/commit/5bd8c03e024f3ff0fb14135b9dbc60a9c709fb38))
* force lower case curation search term ([09d73bf](https://bitbucket.org/usereactify/reactify-search-ui/commit/09d73bf8b86942f8fb67d9283e55bee6e99ae963))
* force value to be an array in filter customQuery ([454c110](https://bitbucket.org/usereactify/reactify-search-ui/commit/454c11023a3386c00ad4bcec33ba04d94c86c330))
* include sorting by collection position when using global curation ([141c463](https://bitbucket.org/usereactify/reactify-search-ui/commit/141c46351711c3cff65275e47fdf8e9ed64b0ad8))
* incorrect hasSelected calculation from filter value ([bf1e8c8](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf1e8c84a54164bd905931743356d301b3b2b5d6))
* instant search sorting ([b6ce036](https://bitbucket.org/usereactify/reactify-search-ui/commit/b6ce036d06d72dbbe28fb1892537ee399ee77c0c))
* make collection nodes optional ([3b2afe5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3b2afe5432d50ba2f8ee57f62478708e113d11d1))
* match useAnalytics endpoint signature ([b94a70c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b94a70c9b1a1f8acc9a11bbff8e6414e77d34657))
* normalise collection handle and search term when resolving curation ([6bd1aec](https://bitbucket.org/usereactify/reactify-search-ui/commit/6bd1aecd163743bdcfc9582389f35a4dc579930c))
* only apply inventory sensor to products, not callouts ([7c186b8](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c186b83c9f403f0987ef088714db1438f27cd84))
* only record select event when link clicked ([ba95992](https://bitbucket.org/usereactify/reactify-search-ui/commit/ba959923785837ed54053257012a1cfdcc69be59))
* only request live config if shop is present ([a9ab343](https://bitbucket.org/usereactify/reactify-search-ui/commit/a9ab343aef7790650635517a83fe4889e87427a3))
* only run tracking event when data exists ([a768c19](https://bitbucket.org/usereactify/reactify-search-ui/commit/a768c1931fd52e69d5538760547c50d610bcb8cf))
* pagination spacing ([945f23a](https://bitbucket.org/usereactify/reactify-search-ui/commit/945f23acfb9e9f39b343b51cad53bf57c8df8522))
* pass on nocache url param to config endpoint to bust caching on worker side ([51b9dcf](https://bitbucket.org/usereactify/reactify-search-ui/commit/51b9dcf0b8145691a2dd00aedbe951f71bbc04b3))
* pass render prop to ResultCardCalloutInner, add pagePosition prop ([3983db4](https://bitbucket.org/usereactify/reactify-search-ui/commit/3983db4c471a3172259662f03e400c3f8ed14b0d))
* product card image margins ([8417270](https://bitbucket.org/usereactify/reactify-search-ui/commit/8417270a3a829e525c6d4b330018f893dbe4fb01))
* product card negative margin ([0573eb0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0573eb02b9bdac3ada0c9ad6f58d91980cba4ef2))
* product card text styling and alignment ([ef16770](https://bitbucket.org/usereactify/reactify-search-ui/commit/ef167700cccf3f094fed8329a6c3471f9af54c6a))
* re-pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([77bbe4c](https://bitbucket.org/usereactify/reactify-search-ui/commit/77bbe4cb340d0afc2e76c6f584675db5322fee29))
* react results component to SensorCollection ([c2833e6](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2833e6188958b054c39fce7cceefc154cc3e66c))
* regenerate yarn.lock in release script ([b97ddef](https://bitbucket.org/usereactify/reactify-search-ui/commit/b97ddefae363a25303afc90042044f0692fac3c5))
* remove console error for range filter ([9eeeb5d](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eeeb5df6b1e71ce1d9ce784259b141988c450fb))
* remove name and variant from select_item event ([a87037d](https://bitbucket.org/usereactify/reactify-search-ui/commit/a87037d493a269fd402f68431fa4fd0c90911f70))
* remove problematic css color "inherit" ([aa304bd](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa304bde76528d54722255b0e38ef732f58218b6))
* render issues with expired config cache ([2c408ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/2c408adce950da369db77ff8ece5a8d08cd92d5f))
* result card image alignment ([46aedd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/46aedd030f4e623bee3d28647f97c100648cd8ef))
* **search:** correct type for context submitSearch ([8b3e647](https://bitbucket.org/usereactify/reactify-search-ui/commit/8b3e64716b6d82e14efc8abb88ff1f6013d5c182))
* **search:** include _score in sort state eligible for curations ([2839929](https://bitbucket.org/usereactify/reactify-search-ui/commit/2839929be960aabd0b45e4aad0ce6a5abdc776e6))
* **search:** ssr build issue due to window ([aada68a](https://bitbucket.org/usereactify/reactify-search-ui/commit/aada68a20cdb8cffe43abbde487229819fdd39e3))
* select_item event, add questions in comments ([8693a78](https://bitbucket.org/usereactify/reactify-search-ui/commit/8693a781049ee44d429f4bf6149532737d9ccbb8))
* set base url for docs ([11fca33](https://bitbucket.org/usereactify/reactify-search-ui/commit/11fca33ea6e9a2b137b758f67206194b9b4e574b))
* set product images to large via cdn ([d1c311f](https://bitbucket.org/usereactify/reactify-search-ui/commit/d1c311fcb093b2ba0ec31adf3677da079328c833))
* show all filter options unless displaySize is defined ([6887ed7](https://bitbucket.org/usereactify/reactify-search-ui/commit/6887ed70737b864cf87cf97433626b9e5ecb7c98))
* size and colour filter targets in docs ([735c12e](https://bitbucket.org/usereactify/reactify-search-ui/commit/735c12ed29683d324651382f4fad55c16941e716))
* ssr playground issues ([5cdb445](https://bitbucket.org/usereactify/reactify-search-ui/commit/5cdb4458ca5db1526a3e00eeff99b7e071dd3ae2))
* support missing compare_at_price on price sets ([dbe3fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/dbe3fc20db23e1969f0d0c9ee75e646cce3f1858))
* support v1 indexes in ResultList component ([4c23ef3](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c23ef3757daeb505823f4af6399746a29404f7a))
* switch to reactify sentry org dsn ([cae3cf0](https://bitbucket.org/usereactify/reactify-search-ui/commit/cae3cf02721ee8b40ddf13854e7aa3cea88034f4))
* text styling for filter titles ([7c0d15c](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c0d15cbd05ace9df056b260c2978146109270a7))
* **theme-debut:** browser warning for svg property ([4fd7948](https://bitbucket.org/usereactify/reactify-search-ui/commit/4fd794848b4b15becb601718f635005407db6d79))
* **theme-debut:** fix flex width on results grid ([049da94](https://bitbucket.org/usereactify/reactify-search-ui/commit/049da942aa87c7a887d689f78f77da02b9b63422))
* **theme-debut:** use new SensorStack component ([da59639](https://bitbucket.org/usereactify/reactify-search-ui/commit/da5963989b5012a231df3ea572f02095c4cc8489))
* tighten up instantsearch debounce solution ([6f811ca](https://bitbucket.org/usereactify/reactify-search-ui/commit/6f811ca9350165c8858ae3388eac09af56a32cfd))
* update filter resolution to return first filter if none found with type ([e7990d1](https://bitbucket.org/usereactify/reactify-search-ui/commit/e7990d1727dd019fba19f9a8132109c992a9a57e))
* update provider props to suit merged changes ([f014e87](https://bitbucket.org/usereactify/reactify-search-ui/commit/f014e87e14776577d0f83d24a5c94aa6d3b15818))
* use american color variation ([57f9b0e](https://bitbucket.org/usereactify/reactify-search-ui/commit/57f9b0e6de4482fd451bd36a1b49e47bb8d28f8e))
* use collection for filter for both filter and search until configurable ([007def5](https://bitbucket.org/usereactify/reactify-search-ui/commit/007def512972b88ae1750157184436e11aca2691))
* use commonjs ([b742b8b](https://bitbucket.org/usereactify/reactify-search-ui/commit/b742b8b268c0a663361147efb93bf120a208a708))
* use correct glob pattern for artifacts ([9f35c7c](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f35c7ccea4f957a94b172fe7ac62b53faf73b32))
* use correct names of components and imports ([e180a75](https://bitbucket.org/usereactify/reactify-search-ui/commit/e180a75b0c4be555a61d72e48a54434b7aa27960))
* use docusaurus router for index docs redirect ([167403b](https://bitbucket.org/usereactify/reactify-search-ui/commit/167403b4aa2f53c57f4cbc69c0a00f78fb46afd2))
* use variants.available in SensorInventoryAvailable ([ffaa237](https://bitbucket.org/usereactify/reactify-search-ui/commit/ffaa237663ad3a44ed798539c5c5306f11533ec8))
* use yarn to fix monorepo issues, upgrade docusaurus ([aa4ee48](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa4ee4851b72acc30718974b3642c0932aa56f35))
* white line ([2a5a191](https://bitbucket.org/usereactify/reactify-search-ui/commit/2a5a191872531e8ccdd63ff0135d6474324f3f43))


* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([4a85878](https://bitbucket.org/usereactify/reactify-search-ui/commit/4a858787ad547e87e10d3c3859ce41f87a4c3bbe))

## [5.0.0-beta.15](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.14...v5.0.0-beta.15) (2022-08-05)

## [5.0.0-beta.14](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.13...v5.0.0-beta.14) (2022-08-04)


### Features

* add example components for filter range, filter slider, custom, stats, clear all, filters active ([d5e56fb](https://bitbucket.org/usereactify/reactify-search-ui/commit/d5e56fb114edc3e134c3ce2e90f7f2df67c5d097))
* add example filter list unstyled, radio, checkbox, swatch and box ([a6ebcd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6ebcd0b606f9a8381d1e082dd4935e4a4d83c2f))
* add support for filter option settingsUppercase ([99d822b](https://bitbucket.org/usereactify/reactify-search-ui/commit/99d822b611ec0302fe314cbe0b1c24653b1ae72c))
* improve sorting of filter list options ([c41eedc](https://bitbucket.org/usereactify/reactify-search-ui/commit/c41eedc900f500d3ddf9434cb14a7397c6385707))

## [5.0.0-beta.13](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.12...v5.0.0-beta.13) (2022-08-03)


### Features

* add breadcrumb for ES search result errors ([22ae301](https://bitbucket.org/usereactify/reactify-search-ui/commit/22ae301bbb7618aec14499970ffd2578ab569a7a))
* add breadcrumb for search query update ([6a2e442](https://bitbucket.org/usereactify/reactify-search-ui/commit/6a2e44269396353fced1d597d48502ef5b1a6bb2))


### Bug Fixes

* use correct names of components and imports ([e180a75](https://bitbucket.org/usereactify/reactify-search-ui/commit/e180a75b0c4be555a61d72e48a54434b7aa27960))

## [5.0.0-beta.12](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.11...v5.0.0-beta.12) (2022-07-27)


### Features

* improve performance when debugging by reusing debugger instances ([aa88917](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa88917b0aedf2abe66439d430587f18dcb72aae))

## [5.0.0-beta.11](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.10...v5.0.0-beta.11) (2022-07-27)

## [5.0.0-beta.10](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.9...v5.0.0-beta.10) (2022-07-27)


### Features

* improve classnames ([19fcfa1](https://bitbucket.org/usereactify/reactify-search-ui/commit/19fcfa152c555cb2e78ec62371f1758e2d244557))


### Bug Fixes

* check that window is defined before accessing ([e1ec0aa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1ec0aae22cab7eb86363ed09d47e422d08ff5c1))

## [5.0.0-beta.9](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.8...v5.0.0-beta.9) (2022-07-27)


### Features

* add classnames for styling to default components ([75997e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/75997e25756595401401de42e9aa29b235581645))

## [5.0.0-beta.8](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.7...v5.0.0-beta.8) (2022-07-27)


### Features

* improve default components ([81bb427](https://bitbucket.org/usereactify/reactify-search-ui/commit/81bb427367987125eeea352189dd9f411cb4850f))

## [5.0.0-beta.7](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.6...v5.0.0-beta.7) (2022-07-27)


### Features

* improve sentry stacktraces and logging ([7f34b30](https://bitbucket.org/usereactify/reactify-search-ui/commit/7f34b3064f41ad9a0e5a3ddc3fa071e5981d0141))

## [5.0.0-beta.6](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.5...v5.0.0-beta.6) (2022-07-26)


### Bug Fixes

* enable typescript inlinesources ([6998e8e](https://bitbucket.org/usereactify/reactify-search-ui/commit/6998e8e3e884d62cf08b08c43ae7dbc73db7f471))

## [5.0.0-beta.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.4...v5.0.0-beta.5) (2022-07-26)


### Bug Fixes

* switch to reactify sentry org dsn ([cae3cf0](https://bitbucket.org/usereactify/reactify-search-ui/commit/cae3cf02721ee8b40ddf13854e7aa3cea88034f4))

## [5.0.0-beta.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.3...v5.0.0-beta.4) (2022-07-26)

## [5.0.0-beta.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.2...v5.0.0-beta.3) (2022-07-26)


### Features

* improve sentry release process ([4237771](https://bitbucket.org/usereactify/reactify-search-ui/commit/423777120041228dcb6f819f71a00abd08d43987))

## [5.0.0-beta.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.1...v5.0.0-beta.2) (2022-07-26)


### Bug Fixes

* use correct glob pattern for artifacts ([9f35c7c](https://bitbucket.org/usereactify/reactify-search-ui/commit/9f35c7ccea4f957a94b172fe7ac62b53faf73b32))

## [5.0.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.0...v5.0.0-beta.1) (2022-07-26)

## [5.0.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.4.1...v5.0.0-beta.0) (2022-07-25)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5

* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([4a85878](https://bitbucket.org/usereactify/reactify-search-ui/commit/4a858787ad547e87e10d3c3859ce41f87a4c3bbe))

## [5.0.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.4.1...v5.0.0-beta.1) (2022-07-25)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5

### Features

* add support for beta deployments ([e8249c1](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8249c1e68647ec45a9eb90607bfc6ae8b0f17f2))


* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([f29d15f](https://bitbucket.org/usereactify/reactify-search-ui/commit/f29d15f8d370403fdc72dd642bc5987868db7f15))

## [5.0.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.4.1...v5.0.0-beta.0) (2022-07-25)


### ⚠ BREAKING CHANGES

* separate monolith provider into hooks and context
* align code and docs for v5

* align code and docs for v5 ([e1823a5](https://bitbucket.org/usereactify/reactify-search-ui/commit/e1823a5ab06dcea89b848bde4156f488241dea01))
* separate monolith provider into hooks and context ([f29d15f](https://bitbucket.org/usereactify/reactify-search-ui/commit/f29d15f8d370403fdc72dd642bc5987868db7f15))

### [4.4.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.4.0...v4.4.1) (2022-07-21)


### Bug Fixes

* correct tsconfig ([e82defa](https://bitbucket.org/usereactify/reactify-search-ui/commit/e82defac47d754df543fc276319ba759de607507))

## [4.4.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.4.0-beta.0...v4.4.0) (2022-07-21)

## [4.4.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.3.1...v4.4.0-beta.0) (2022-07-21)


### Bug Fixes

* correct tsconfig ([01a796b](https://bitbucket.org/usereactify/reactify-search-ui/commit/01a796bfc03f4153d8fbbea86a0597ee3c665a94))

### [4.3.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.3.0...v4.3.1) (2022-07-21)

## [4.3.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.3.0-beta.0...v4.3.0) (2022-07-21)

## [4.3.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.7...v4.3.0-beta.0) (2022-07-20)


### Features

* hide callouts when not sorting by "_score" or "collections.position" ([bf2d57a](https://bitbucket.org/usereactify/reactify-search-ui/commit/bf2d57aa5b682375dc06995bb54d90ece330f5e3))

### [4.2.7](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.6...v4.2.7) (2022-07-19)


### Bug Fixes

* only apply inventory sensor to products, not callouts ([7c186b8](https://bitbucket.org/usereactify/reactify-search-ui/commit/7c186b83c9f403f0987ef088714db1438f27cd84))

### [4.2.6](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.5...v4.2.6) (2022-07-11)

### [4.2.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.4...v4.2.5) (2022-07-11)


### Bug Fixes

* remove problematic css color "inherit" ([aa304bd](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa304bde76528d54722255b0e38ef732f58218b6))

### [4.2.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.3...v4.2.4) (2022-06-23)


### Bug Fixes

* add missing spread operator ([acc51cb](https://bitbucket.org/usereactify/reactify-search-ui/commit/acc51cb5c414831e3d0e919185d076895903d837))

### [4.2.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.2...v4.2.3) (2022-06-23)


### Bug Fixes

* include sorting by collection position when using global curation ([141c463](https://bitbucket.org/usereactify/reactify-search-ui/commit/141c46351711c3cff65275e47fdf8e9ed64b0ad8))

### [4.2.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.1...v4.2.2) (2022-06-21)


### Features

* merge global boosting rules with curation when empty ([248cbdd](https://bitbucket.org/usereactify/reactify-search-ui/commit/248cbdda01864c0d536f5e971f81e331de4c827d))


### Bug Fixes

* pass on nocache url param to config endpoint to bust caching on worker side ([51b9dcf](https://bitbucket.org/usereactify/reactify-search-ui/commit/51b9dcf0b8145691a2dd00aedbe951f71bbc04b3))

### [4.2.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.2.0...v4.2.1) (2022-06-14)

## [4.2.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0...v4.2.0) (2022-06-14)


### Features

* allow custom grid styles to be provided and override defaults ([25ce405](https://bitbucket.org/usereactify/reactify-search-ui/commit/25ce405300777b9d55a0e806672787bd552438a6))

## [4.1.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.6...v4.1.0) (2022-06-09)

## [4.1.0-beta.6](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.5...v4.1.0-beta.6) (2022-06-08)

## [4.1.0-beta.5](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.4...v4.1.0-beta.5) (2022-06-08)

## [4.1.0-beta.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.3...v4.1.0-beta.4) (2022-06-08)


### Bug Fixes

* only request live config if shop is present ([a9ab343](https://bitbucket.org/usereactify/reactify-search-ui/commit/a9ab343aef7790650635517a83fe4889e87427a3))
* remove console error for range filter ([9eeeb5d](https://bitbucket.org/usereactify/reactify-search-ui/commit/9eeeb5df6b1e71ce1d9ce784259b141988c450fb))

## [4.1.0-beta.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.2...v4.1.0-beta.3) (2022-06-08)

## [4.1.0-beta.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.1...v4.1.0-beta.2) (2022-06-08)


### Bug Fixes

* only run tracking event when data exists ([a768c19](https://bitbucket.org/usereactify/reactify-search-ui/commit/a768c1931fd52e69d5538760547c50d610bcb8cf))

## [4.1.0-beta.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v4.1.0-beta.0...v4.1.0-beta.1) (2022-06-08)


### Features

* add sentry release tracking to ci ([67c813d](https://bitbucket.org/usereactify/reactify-search-ui/commit/67c813d2c8812cf1b6c4e2ad1ce9b0a38ebce24b))


### Bug Fixes

* update provider props to suit merged changes ([f014e87](https://bitbucket.org/usereactify/reactify-search-ui/commit/f014e87e14776577d0f83d24a5c94aa6d3b15818))

## [4.1.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v5.0.0-beta.0...v4.1.0-beta.0) (2022-06-08)

## [4.0.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.11.0...v4.0.0) (2022-06-08)


### ⚠ BREAKING CHANGES

* switch to using "handle" instead of "id" for sort option
* switch to using "handle" instead of "id" for filter and filter option

### Features

* switch to using "handle" instead of "id" for filter and filter option ([a92bdd7](https://bitbucket.org/usereactify/reactify-search-ui/commit/a92bdd75e68c6291b9f321cec5e77f3934152a9a))
* switch to using "handle" instead of "id" for sort option ([c2be1fa](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2be1fabd4ba1634e47715393c2065a1ca2ba9e7))

## [3.11.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.10.4...v3.11.0) (2022-06-06)


### Features

* **app:** add provider prop "configId" to support multi-config shops ([2ef6af5](https://bitbucket.org/usereactify/reactify-search-ui/commit/2ef6af5a6d46331690bdd3c35ee72193dca8f587))

### [3.10.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.10.3...v3.10.4) (2022-06-02)


### Bug Fixes

* **app:** fix access to undefined sort option ID ([f93bbce](https://bitbucket.org/usereactify/reactify-search-ui/commit/f93bbcec82ee61d4a0bf19d7ab18894339f8a27c))

### [3.10.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.10.2...v3.10.3) (2022-06-02)


### Features

* add support for collections assigned to filter groups ([afe0711](https://bitbucket.org/usereactify/reactify-search-ui/commit/afe071115aa7633f49d5f95673c544989e80f011))

### [3.10.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.10.1...v3.10.2) (2022-06-02)


### Features

* add sort option into url params ([f78ec9b](https://bitbucket.org/usereactify/reactify-search-ui/commit/f78ec9b69555df9cb297d818a06d08d6587889c3))

### [3.10.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.10.0...v3.10.1) (2022-06-02)


### Features

* add onRedirect prop + fix caching issue when searching from search page ([59b32f6](https://bitbucket.org/usereactify/reactify-search-ui/commit/59b32f65c8ede64cbd42d297ed7a4e131b8650eb))

## [3.10.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.9.1...v3.10.0) (2022-06-01)


### Features

* add pagination, sort, filter and promotion analytics events ([5968814](https://bitbucket.org/usereactify/reactify-search-ui/commit/596881486f05189d90823f0ec1b093580d79a786))


### Bug Fixes

* add missing next/prev pagination export ([6b13902](https://bitbucket.org/usereactify/reactify-search-ui/commit/6b139027cc1b5038add4d688a9301add2f6e75ee))

### [3.9.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.9.0...v3.9.1) (2022-06-01)


### Bug Fixes

* re-pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([77bbe4c](https://bitbucket.org/usereactify/reactify-search-ui/commit/77bbe4cb340d0afc2e76c6f584675db5322fee29))

## [3.9.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.8.1...v3.9.0) (2022-06-01)


### Features

* pin @appbaseio/reactivesearch at ^3.14.0 to match advice given to delivery teams ([fb93473](https://bitbucket.org/usereactify/reactify-search-ui/commit/fb93473fa5723a71cfad264f4c4fd09c541ea278))

### [3.8.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.8.0...v3.8.1) (2022-05-31)


### Bug Fixes

* add timeout to blur event so results remain clickable ([fe98040](https://bitbucket.org/usereactify/reactify-search-ui/commit/fe980407427256246051bee8be152048ae841cf2))

## [3.8.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.7.0...v3.8.0) (2022-05-30)


### Features

* add grid gap to result list component props ([0d65bd0](https://bitbucket.org/usereactify/reactify-search-ui/commit/0d65bd032cc834060fca5702195566a36ef8fb33))

## [3.7.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.6.0...v3.7.0) (2022-05-30)


### Features

* add support for additional datasearch input props ([a6c6382](https://bitbucket.org/usereactify/reactify-search-ui/commit/a6c63825f6d3e616d57dc0f0af91895475175cc0))

## [3.6.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.5.0...v3.6.0) (2022-05-28)


### Features

* add support for global curation / default boosting rules ([b476e75](https://bitbucket.org/usereactify/reactify-search-ui/commit/b476e75233202bd3ab190d7806cad0b8077463a8))

## [3.5.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.4.4...v3.5.0) (2022-05-27)


### Features

* add zero results and view product (impressions) events ([db4f310](https://bitbucket.org/usereactify/reactify-search-ui/commit/db4f3102c592bc295534bcb5de9523d1f0b26557))

### [3.4.4](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.4.3...v3.4.4) (2022-05-19)


### Bug Fixes

* render issues with expired config cache ([2c408ad](https://bitbucket.org/usereactify/reactify-search-ui/commit/2c408adce950da369db77ff8ece5a8d08cd92d5f))

### [3.4.3](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.4.2...v3.4.3) (2022-04-11)


### Bug Fixes

* check for existence of window before using (node render) ([df7f9b0](https://bitbucket.org/usereactify/reactify-search-ui/commit/df7f9b0c0c29d561e69107c8086a5d0fa023aaee))

### [3.4.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.4.1...v3.4.2) (2022-04-11)


### Bug Fixes

* correct logic for calculating search fields when config is empty or updates ([ae67498](https://bitbucket.org/usereactify/reactify-search-ui/commit/ae6749875f295db99f012b54cc06e10346f702d0))

### [3.4.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.4.0...v3.4.1) (2022-04-11)


### Bug Fixes

* correct typings for selected sort option within context ([f363fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/f363fc2c8ea1ad88e65844c382620b927614c403))

## [3.4.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v3.3.0...v3.4.0) (2022-04-11)


### Features

* add hook to get cached or remote config ([cb77750](https://bitbucket.org/usereactify/reactify-search-ui/commit/cb77750f4839134e65afb2eee685755169bfcd5d))
* add renderBooting prop for rendering while config is loading ([3da876e](https://bitbucket.org/usereactify/reactify-search-ui/commit/3da876e5a87ecd98f2abe462994461b7cd39acda))
* ignore cached config if "nocache" url search param is set ([38823c2](https://bitbucket.org/usereactify/reactify-search-ui/commit/38823c2272fd0a2ae6d45401df35a2148107fe8e))
* update provider to use live config hook ([d898df0](https://bitbucket.org/usereactify/reactify-search-ui/commit/d898df01cb21538d9cd5dada9d343967ad91cdd6))

## [3.3.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v2.1.1...v3.3.0) (2022-03-28)


### Features

* add shop name in context, ([13b54e2](https://bitbucket.org/usereactify/reactify-search-ui/commit/13b54e28715b7789cc5cf47d82cc205d175ce1ee))
* add warning and prevent tracking request if "shopifyPermanentDomain" is missing ([bcbf378](https://bitbucket.org/usereactify/reactify-search-ui/commit/bcbf378ac057755347556a60bae58684df8f65c1))
* additional result props in ResultList ([821b022](https://bitbucket.org/usereactify/reactify-search-ui/commit/821b02281baa07b844ce3c6dbba1f8a1ef42dee7))
* call track after trigger query in SearhInput ([8a09111](https://bitbucket.org/usereactify/reactify-search-ui/commit/8a0911165ea4484bf7bcf65b12c687a0d8b674ca))
* remove combined results logic, discriminate product and callout ([ee1b38e](https://bitbucket.org/usereactify/reactify-search-ui/commit/ee1b38eeddad8eedec473c5b49364b67fa6ae311))
* remove event handling overrides in useAnalytics track function ([3cb1bc5](https://bitbucket.org/usereactify/reactify-search-ui/commit/3cb1bc50318f64c2d11013697152d187108d603c))
* update callout card for new ElasticCallout type ([8db23b5](https://bitbucket.org/usereactify/reactify-search-ui/commit/8db23b5e5b240b45ada520b30a43ff988bcff9d5))
* useAnalytics hook ([17fb749](https://bitbucket.org/usereactify/reactify-search-ui/commit/17fb749e763870a2888c3cf68ceeff3cd0d8fc21))


### Bug Fixes

* add missing package "axios" ([eaa4763](https://bitbucket.org/usereactify/reactify-search-ui/commit/eaa476367ef0947b20c24fe4001efb4550184516))
* fix request payload ([aa33e95](https://bitbucket.org/usereactify/reactify-search-ui/commit/aa33e95ba1162ec0bbb84cd25c2e4483f3822455))
* fix search event payload ([5bd8c03](https://bitbucket.org/usereactify/reactify-search-ui/commit/5bd8c03e024f3ff0fb14135b9dbc60a9c709fb38))
* match useAnalytics endpoint signature ([b94a70c](https://bitbucket.org/usereactify/reactify-search-ui/commit/b94a70c9b1a1f8acc9a11bbff8e6414e77d34657))
* only record select event when link clicked ([ba95992](https://bitbucket.org/usereactify/reactify-search-ui/commit/ba959923785837ed54053257012a1cfdcc69be59))
* remove name and variant from select_item event ([a87037d](https://bitbucket.org/usereactify/reactify-search-ui/commit/a87037d493a269fd402f68431fa4fd0c90911f70))
* select_item event, add questions in comments ([8693a78](https://bitbucket.org/usereactify/reactify-search-ui/commit/8693a781049ee44d429f4bf6149532737d9ccbb8))
* support v1 indexes in ResultList component ([4c23ef3](https://bitbucket.org/usereactify/reactify-search-ui/commit/4c23ef3757daeb505823f4af6399746a29404f7a))
* white line ([2a5a191](https://bitbucket.org/usereactify/reactify-search-ui/commit/2a5a191872531e8ccdd63ff0135d6474324f3f43))

### [2.1.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v2.1.0...v2.1.1) (2022-03-01)


### Bug Fixes

* add nested attribute to collection position sort clause (RS-124) ([0932b3e](https://bitbucket.org/usereactify/reactify-search-ui/commit/0932b3e6e64dd2a0ef9df592c431f0dc5340fcc1))

## [2.1.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v2.0.2...v2.1.0) (2022-02-28)


### Features

* add image to default result card render ([c50319e](https://bitbucket.org/usereactify/reactify-search-ui/commit/c50319e1011a7465c07b509b5164ea04b9f38684))
* sort of _score always be desc ([f51e75c](https://bitbucket.org/usereactify/reactify-search-ui/commit/f51e75c4f21acb54fbe64e444b26dd02f452c6ac))

### [2.0.2](https://bitbucket.org/usereactify/reactify-search-ui/compare/v2.0.1...v2.0.2) (2021-12-15)


### Bug Fixes

* support missing compare_at_price on price sets ([dbe3fc2](https://bitbucket.org/usereactify/reactify-search-ui/commit/dbe3fc20db23e1969f0d0c9ee75e646cce3f1858))

### [2.0.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v2.0.0...v2.0.1) (2021-12-15)


### Bug Fixes

* check compare_at_price exists before formatting in useProductPrice ([acba76c](https://bitbucket.org/usereactify/reactify-search-ui/commit/acba76c6d8e0c5ba1482fe230e20fcd286f11e69))

## [2.0.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.3.0...v2.0.0) (2021-11-18)


### ⚠ BREAKING CHANGES

* move index to a dedicated prop, make credentials optional and set default

### Features

* move index to a dedicated prop, make credentials optional and set default ([a0696ba](https://bitbucket.org/usereactify/reactify-search-ui/commit/a0696ba239b7b036b37a2764a939ce2f26e992b4))

## [1.3.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.2.0...v1.3.0) (2021-11-18)


### Features

* add support for valuesManual in filter option filtering ([c2cef69](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2cef69e491f9852a1250e7218550ac6c1e1671e))
* add totalSelected to filter render props (RS-16) ([7949200](https://bitbucket.org/usereactify/reactify-search-ui/commit/7949200f0307637dda424875e7ca394aa179cdb6))

## [1.2.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.1.1...v1.2.0) (2021-10-19)


### Features

* adjust page size to accommodate callouts ([0365896](https://bitbucket.org/usereactify/reactify-search-ui/commit/03658965e65ba451a5c419b14599a71296e589c3))

### [1.1.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.1.0...v1.1.1) (2021-10-15)


### Bug Fixes

* pass render prop to ResultCardCalloutInner, add pagePosition prop ([3983db4](https://bitbucket.org/usereactify/reactify-search-ui/commit/3983db4c471a3172259662f03e400c3f8ed14b0d))

## [1.1.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.0.1...v1.1.0) (2021-10-13)


### Features

* disable callouts unless enableCallouts prop is set ([c2ac79a](https://bitbucket.org/usereactify/reactify-search-ui/commit/c2ac79abffc87d050b766b6786977de43920af17))

### [1.0.1](https://bitbucket.org/usereactify/reactify-search-ui/compare/v1.0.0...v1.0.1) (2021-10-13)


### Bug Fixes

* filter currentId from all filters in useReactiveReactProp ([645fe73](https://bitbucket.org/usereactify/reactify-search-ui/commit/645fe7373f469255d15bbe12c05d478193053ac6))

## [1.0.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v0.2.0-beta.0...v1.0.0) (2021-10-13)


### Features

* add additionalComponentIds prop to provider ([1db51a0](https://bitbucket.org/usereactify/reactify-search-ui/commit/1db51a0f905475d2d3399f9de7ff6f025860dce5))

## [0.2.0-beta.0](https://bitbucket.org/usereactify/reactify-search-ui/compare/v0.1.29...v0.2.0-beta.0) (2021-10-08)


### Features

* combined results in result list ([a85f39e](https://bitbucket.org/usereactify/reactify-search-ui/commit/a85f39e8d425c51b8108df488145ade8eec1cdf5))
* render callouts in result list, implement grid ([e8cbaad](https://bitbucket.org/usereactify/reactify-search-ui/commit/e8cbaad07db3daf2c4a59a2de565774db213d0c6))

### [0.1.29](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.28...v0.1.29) (2021-08-05)


### Features

* apply defaultQuery to nested variant filters to check inventory available ([a348522](https://gitlab.com/reactifyapps/reactify-search-ui/commit/a3485220bb3c682d70a6efcd9b520972430a00d7))


### Bug Fixes

* size and colour filter targets in docs ([735c12e](https://gitlab.com/reactifyapps/reactify-search-ui/commit/735c12ed29683d324651382f4fad55c16941e716))

### [0.1.28](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.24...v0.1.28) (2021-07-23)

### Features

- allow provider theme to be configured ([33eb10f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/33eb10f50dbdaaf2f399ea0e95b673222da868b1))
- set default reset theme ([0661294](https://gitlab.com/reactifyapps/reactify-search-ui/commit/0661294a03eae7e70d29a2415df968da38bb0bf8))

### Bug Fixes

- instant search sorting ([b6ce036](https://gitlab.com/reactifyapps/reactify-search-ui/commit/b6ce036d06d72dbbe28fb1892537ee399ee77c0c))

### [0.1.27](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.25...v0.1.27) (2021-07-01)

### Features

- set default reset theme ([0661294](https://gitlab.com/reactifyapps/reactify-search-ui/commit/0661294a03eae7e70d29a2415df968da38bb0bf8))

### [0.1.26](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.25...v0.1.26) (2021-06-28)

### Features

- allow provider theme to be configured ([33eb10f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/33eb10f50dbdaaf2f399ea0e95b673222da868b1))

### [0.1.25](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.24...v0.1.25) (2021-06-28)

### [0.1.24](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.23...v0.1.24) (2021-06-25)

### Bug Fixes

- add missing lower case to existing sort sensor ([c7099a0](https://gitlab.com/reactifyapps/reactify-search-ui/commit/c7099a0edf57658fa006ac018b017807891a44dd))

### [0.1.23](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.22...v0.1.23) (2021-06-25)

### Bug Fixes

- force lower case curation search term ([09d73bf](https://gitlab.com/reactifyapps/reactify-search-ui/commit/09d73bf8b86942f8fb67d9283e55bee6e99ae963))

### [0.1.22](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.21...v0.1.22) (2021-06-24)

### Features

- add support for pinning products in curations that don't exist within the resultset ([3ceadf6](https://gitlab.com/reactifyapps/reactify-search-ui/commit/3ceadf621ab78b216bf355d3e2ec648a6d9fee44))

### [0.1.21](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.20...v0.1.21) (2021-06-18)

### Bug Fixes

- normalise collection handle and search term when resolving curation ([6bd1aec](https://gitlab.com/reactifyapps/reactify-search-ui/commit/6bd1aecd163743bdcfc9582389f35a4dc579930c))

### [0.1.20](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.19...v0.1.20) (2021-06-17)

### Features

- support hidden curation products in SensorSort ([c80a52f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/c80a52f6e515bb023dd99caedc20ace38444b725))

### Bug Fixes

- regenerate yarn.lock in release script ([b97ddef](https://gitlab.com/reactifyapps/reactify-search-ui/commit/b97ddefae363a25303afc90042044f0692fac3c5))
- use yarn to fix monorepo issues, upgrade docusaurus ([aa4ee48](https://gitlab.com/reactifyapps/reactify-search-ui/commit/aa4ee4851b72acc30718974b3642c0932aa56f35))

### [0.1.19](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.18...v0.1.19) (2021-06-14)

### Bug Fixes

- **search:** ssr build issue due to window ([aada68a](https://gitlab.com/reactifyapps/reactify-search-ui/commit/aada68a20cdb8cffe43abbde487229819fdd39e3))

### [0.1.18](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.17...v0.1.18) (2021-06-04)

### Bug Fixes

- use variants.available in SensorInventoryAvailable ([ffaa237](https://gitlab.com/reactifyapps/reactify-search-ui/commit/ffaa237663ad3a44ed798539c5c5306f11533ec8))

### [0.1.17](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.16...v0.1.17) (2021-06-01)

### Bug Fixes

- case insensitive redirect match ([#11](https://gitlab.com/reactifyapps/reactify-search-ui/issues/11)) ([096c7fb](https://gitlab.com/reactifyapps/reactify-search-ui/commit/096c7fb84d77ae0d985b2e7cf4ade6c80e3171e7))

### [0.1.16](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.15...v0.1.16) (2021-05-10)

### Bug Fixes

- **search:** include \_score in sort state eligible for curations ([2839929](https://gitlab.com/reactifyapps/reactify-search-ui/commit/2839929be960aabd0b45e4aad0ce6a5abdc776e6))

### [0.1.15](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.14...v0.1.15) (2021-05-10)

### Bug Fixes

- **search:** correct type for context submitSearch ([8b3e647](https://gitlab.com/reactifyapps/reactify-search-ui/commit/8b3e64716b6d82e14efc8abb88ff1f6013d5c182))

### [0.1.14](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.13...v0.1.14) (2021-05-10)

### Features

- **search:** allow searchQuery to be provided to submitSearch ([116e5d6](https://gitlab.com/reactifyapps/reactify-search-ui/commit/116e5d60084ae20ccac3bb07d9e17d2158888479))
- **search:** onBlur, onFocus events in SearchInput component ([c6799be](https://gitlab.com/reactifyapps/reactify-search-ui/commit/c6799be75c667a47c9c6aac84696040ff0f12acc))

### [0.1.13](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.12...v0.1.13) (2021-05-09)

### Features

- drop SensorStackInstantsearch in favor of single SensorStack with context ([f31a60b](https://gitlab.com/reactifyapps/reactify-search-ui/commit/f31a60b9ba6bf2c61d88529b3ceec0b5aa188e1f))

### [0.1.12](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.11...v0.1.12) (2021-05-07)

### Features

- add SearchInput component and associated instant search support ([98a8af7](https://gitlab.com/reactifyapps/reactify-search-ui/commit/98a8af726f9e781bd03c5ed2dd815c0976b19f21))
- add SensorStackInstantSearch ([e93308f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/e93308f06532e7877300c84820e5019b152c895b))
- **theme-debut:** add instant search component ([3017fb2](https://gitlab.com/reactifyapps/reactify-search-ui/commit/3017fb2d2b2da6413d148e7853e47f7cdf30e3e2))

### [0.1.11](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.10...v0.1.11) (2021-05-07)

### Features

- add debug output to provider ([c45de1a](https://gitlab.com/reactifyapps/reactify-search-ui/commit/c45de1ae681ede827ae899252758171309bac09b))

### Bug Fixes

- add missing sensors to result list react prop ([2293c43](https://gitlab.com/reactifyapps/reactify-search-ui/commit/2293c4391917a536e31fc3f3fce08703ab134e6b))

### [0.1.10](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.9...v0.1.10) (2021-05-07)

### Features

- add debug output to SensorSort ([0b3d125](https://gitlab.com/reactifyapps/reactify-search-ui/commit/0b3d125f29dfe678318d91390d3fbfd13a932947))

### [0.1.9](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.8...v0.1.9) (2021-05-05)

### Bug Fixes

- force value to be an array in filter customQuery ([454c110](https://gitlab.com/reactifyapps/reactify-search-ui/commit/454c11023a3386c00ad4bcec33ba04d94c86c330))

### [0.1.8](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.7...v0.1.8) (2021-05-04)

### Bug Fixes

- **theme-debut:** fix flex width on results grid ([049da94](https://gitlab.com/reactifyapps/reactify-search-ui/commit/049da942aa87c7a887d689f78f77da02b9b63422))

### [0.1.7](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.6...v0.1.7) (2021-05-04)

### Features

- add filterStackId prop to provider ([#2](https://gitlab.com/reactifyapps/reactify-search-ui/issues/2)) ([043e9f0](https://gitlab.com/reactifyapps/reactify-search-ui/commit/043e9f0745ad1d6e15ec99400df099320fecf7cf))
- add ResultPagination component and usePages hook ([#4](https://gitlab.com/reactifyapps/reactify-search-ui/issues/4)) ([e8dcf61](https://gitlab.com/reactifyapps/reactify-search-ui/commit/e8dcf610ec1eac9b328935a713732284a1f4d7f8))
- add ResultStateProvider component for accessing page state ([ec4cc6f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/ec4cc6f08ef2aeae46fe864e61d71ec36e50a0e3))
- add SensorInventoryAvailable and rename SensorStack ([18d7aaa](https://gitlab.com/reactifyapps/reactify-search-ui/commit/18d7aaa4a2d7e82d34014ccc888c624cffba4f03))
- add support for settingsHideUnavailable at filter level for variant nested fields ([5ca63e1](https://gitlab.com/reactifyapps/reactify-search-ui/commit/5ca63e1dcd35c9f6f80c83d49ab6f8bb87ecaa7d))
- rename FilterMatrix to FilterStack ([62788cd](https://gitlab.com/reactifyapps/reactify-search-ui/commit/62788cd135e424ab81426ad7e1ae7a3d825d4e28))
- renderAfter, renderBefore and renderPagination props on ResultList ([05d2203](https://gitlab.com/reactifyapps/reactify-search-ui/commit/05d2203447d4ca9fb7843dc3ad0b8b8f111b67e6))
- renderError prop on ResultList component ([d95d6df](https://gitlab.com/reactifyapps/reactify-search-ui/commit/d95d6df10059badc661eeb1041db76531e44231c))
- renderLoading prop on ResultList component with initial search logic ([1568d2e](https://gitlab.com/reactifyapps/reactify-search-ui/commit/1568d2e8729b455b50d42c532cb3bcb91c4e4130))
- renderLoadMoreButton prop on ResultList with default button ([bd5af24](https://gitlab.com/reactifyapps/reactify-search-ui/commit/bd5af24daf4fe1e3c41afa5749f10df0d8b64bb2))
- renderPaginationNextPrev prop on ResultList with default component ([d587303](https://gitlab.com/reactifyapps/reactify-search-ui/commit/d58730367916629770240c8e85549003ce8ddc28))

### Bug Fixes

- **theme-debut:** browser warning for svg property ([4fd7948](https://gitlab.com/reactifyapps/reactify-search-ui/commit/4fd794848b4b15becb601718f635005407db6d79))
- **theme-debut:** use new SensorStack component ([da59639](https://gitlab.com/reactifyapps/reactify-search-ui/commit/da5963989b5012a231df3ea572f02095c4cc8489))

### [0.1.6](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.5...v0.1.6) (2021-04-28)

### Features

- **theme-debut:** add styling for product cards ([1397aa1](https://gitlab.com/reactifyapps/reactify-search-ui/commit/1397aa193eec73592e17b32a5a12d4806a7f808b))
- add listClassName prop to ResultList component ([f28be3e](https://gitlab.com/reactifyapps/reactify-search-ui/commit/f28be3e4d0f8c74d5e7e20e214aec0ee7867ce7c))
- add renderResultCard prop to ResultList component ([889255f](https://gitlab.com/reactifyapps/reactify-search-ui/commit/889255fc8cb2f1db0a6d86be2159276a0da384b7))

### [0.1.5](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.4...v0.1.5) (2021-04-28)

### Features

- **theme-debut:** add styled filters ([467b255](https://gitlab.com/reactifyapps/reactify-search-ui/commit/467b2551723aa413696f4060d9b20777a40298e4))
- **theme-debut:** add tailwind jit styles ([6e28c52](https://gitlab.com/reactifyapps/reactify-search-ui/commit/6e28c520a2113f68fb0fc37bbedd2083c39ad39f))

### Bug Fixes

- show all filter options unless displaySize is defined ([6887ed7](https://gitlab.com/reactifyapps/reactify-search-ui/commit/6887ed70737b864cf87cf97433626b9e5ecb7c98))

### [0.1.4](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.3...v0.1.4) (2021-04-28)

### Bug Fixes

- export Filter and FilterList components ([7a297dd](https://gitlab.com/reactifyapps/reactify-search-ui/commit/7a297dd630c3582d5bf9ae32eb6d31663ee2654d))

### [0.1.3](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.2...v0.1.3) (2021-04-28)

### Bug Fixes

- define nested field prop for variants ([2af7ebe](https://gitlab.com/reactifyapps/reactify-search-ui/commit/2af7ebe6b33c1ca19faf27ca0f525fe1f1669ced))

### [0.1.2](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.1...v0.1.2) (2021-04-27)

### Bug Fixes

- use commonjs ([b742b8b](https://gitlab.com/reactifyapps/reactify-search-ui/commit/b742b8b268c0a663361147efb93bf120a208a708))

### [0.1.1](https://gitlab.com/reactifyapps/reactify-search-ui/compare/v0.1.0...v0.1.1) (2021-04-27)

## 0.1.0 (2021-04-27)

Initial release.
