import React from "react";

import { SuggestionsProps } from "../../components";
import { useSearch } from "../../hooks";

export type ExampleSuggestionsProps = React.ComponentProps<
  NonNullable<SuggestionsProps["render"]>
>;

export const ExampleSuggestions: React.FC<ExampleSuggestionsProps> = (
  props
) => {
  const { setSearchTerm } = useSearch();

  return (
    <>
      <h3>Suggestions</h3>
      <ul>
        {props.suggestions.map((suggestion) => (
          <li onClick={() => setSearchTerm(suggestion.text)}>
            {suggestion.text}
          </li>
        ))}
      </ul>
    </>
  );
};
