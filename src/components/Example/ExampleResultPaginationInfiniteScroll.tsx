import React from "react";

import { ResultPaginationInfiniteScrollProps } from "../../components";

export type ExampleResultPaginationInfiniteScrollProps = React.ComponentProps<
  NonNullable<ResultPaginationInfiniteScrollProps["render"]>
>;

export const ExampleResultPaginationInfiniteScroll: React.FC<ExampleResultPaginationInfiniteScrollProps> =
  (props) => {
    if (props.loading) {
      return (
        <div className="rs__pagination__infinitescroll-loading">
          {"Loading..."}
        </div>
      );
    }
    return null;
  };
