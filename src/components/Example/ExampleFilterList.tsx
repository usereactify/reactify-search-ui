import React from "react";

import { FilterProps } from "../../components";

export type ExampleFilterListProps = React.ComponentProps<
  NonNullable<FilterProps["renderFilterList"]>
>;

export const ExampleFilterList: React.FC<ExampleFilterListProps> = (props) => {
  return (
    <div className="rs__filter">
      <h3 className="rs__filter__name">{props.filter.name}</h3>
      {props.filter.displayView === "list" ? (
        <ExampleFilterListList {...props} />
      ) : null}
      {props.filter.displayType === "single" &&
      props.filter.displayView === "check" ? (
        <ExampleFilterListRadio {...props} />
      ) : null}
      {props.filter.displayType === "multi" &&
      props.filter.displayView === "check" ? (
        <ExampleFilterListCheckbox {...props} />
      ) : null}
      {props.filter.displayView === "swatch" ? (
        <ExampleFilterListSwatch {...props} />
      ) : null}
      {props.filter.displayView === "box" ? (
        <ExampleFilterListBox {...props} />
      ) : null}
    </div>
  );
};

export const ExampleFilterListList: React.FC<ExampleFilterListProps> = (
  props
) => {
  return (
    <ul className="rs__filter__list">
      {props.filterListProps.options.map((option) => (
        <li
          key={option.key}
          className={`rs__filter__list-item ${
            option.checked ? "rs__filter__list-item-active" : ""
          }`}
        >
          <label
            className="rs__filter__list-item-label"
            onClick={() => props.filterListProps.handleChange(option.key)}
          >
            <span className="rs__filter__list-item-key">{option.label}</span>
            <span className="rs__filter__list-item-count">
              ({option.doc_count})
            </span>
          </label>
        </li>
      ))}
    </ul>
  );
};

export const ExampleFilterListRadio: React.FC<ExampleFilterListProps> = (
  props
) => {
  return (
    <ul className="rs__filter__list">
      {props.filterListProps.options.map((option) => (
        <li
          key={option.key}
          className={`rs__filter__list-item ${
            option.checked ? "rs__filter__list-item-active" : ""
          }`}
        >
          <label className="rs__filter__list-item-label">
            <input
              className="rs__filter__list-item-input"
              value={option.key}
              type="radio"
              checked={option.checked}
              onChange={(event) =>
                props.filterListProps.handleChange(event.target.value)
              }
            />
            <span className="rs__filter__list-item-key">{option.label}</span>
            <span className="rs__filter__list-item-count">
              ({option.doc_count})
            </span>
          </label>
        </li>
      ))}
    </ul>
  );
};

export const ExampleFilterListCheckbox: React.FC<ExampleFilterListProps> = (
  props
) => {
  return (
    <ul className="rs__filter__list">
      {props.filterListProps.options.map((option) => (
        <li
          key={option.key}
          className={`rs__filter__list-item ${
            option.checked ? "rs__filter__list-item-active" : ""
          }`}
        >
          <label className="rs__filter__list-item-label">
            <input
              className="rs__filter__list-item-input"
              value={option.key}
              type="checkbox"
              checked={option.checked}
              onChange={(event) =>
                props.filterListProps.handleChange(event.target.value)
              }
            />
            <span className="rs__filter__list-item-key">{option.label}</span>
            <span className="rs__filter__list-item-count">
              ({option.doc_count})
            </span>
          </label>
        </li>
      ))}
    </ul>
  );
};

export const ExampleFilterListSwatch: React.FC<ExampleFilterListProps> = (
  props
) => {
  return (
    <div
      className="rs__filter__list"
      style={{
        display: "flex",
        flexWrap: "wrap",
      }}
    >
      {props.filterListProps.options.map((option) => (
        <div
          key={option.key}
          className={`rs__filter__list-item ${
            option.checked ? "rs__filter__list-item-active" : ""
          }`}
        >
          <div
            className="rs__filter__list-item-label"
            style={{
              display: "inline-block",
              width: "20px",
              height: "20px",
              marginRight: "2px",
              marginBottom: "2px",
              borderRadius: "50%",
              backgroundColor: option.key,
              border: option.checked
                ? "1px solid black"
                : "1px solid transparent",
            }}
            onClick={() => props.filterListProps.handleChange(option.key)}
          />
        </div>
      ))}
    </div>
  );
};

export const ExampleFilterListBox: React.FC<ExampleFilterListProps> = (
  props
) => {
  return (
    <div
      className="rs__filter__list"
      style={{
        display: "flex",
        flexWrap: "wrap",
      }}
    >
      {props.filterListProps.options.map((option) => (
        <div
          key={option.key}
          className={`rs__filter__list-item ${
            option.checked ? "rs__filter__list-item-active" : ""
          }`}
        >
          <div
            className="rs__filter__list-item-label"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "40px",
              height: "40px",
              marginRight: "2px",
              marginBottom: "2px",
              border: "1px solid black",
              overflow: "hidden",
              cursor: "pointer",
              color: option.checked ? "white" : "black",
              backgroundColor: option.checked ? "black" : "white",
            }}
            onClick={() => props.filterListProps.handleChange(option.key)}
          >
            {option.key}
          </div>
        </div>
      ))}
    </div>
  );
};
