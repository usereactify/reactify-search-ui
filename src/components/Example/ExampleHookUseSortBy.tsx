import React from "react";

import { useSortBy } from "../../hooks";

export const ExampleHookUseSortBy: React.FC = () => {
  const sortByHook = useSortBy();

  const handleSelectChange = React.useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      sortByHook.setSortOption(event.target.value);
    },
    [sortByHook.setSortOption]
  );

  return (
    <select value={sortByHook.sortOption?.handle} onChange={handleSelectChange}>
      {sortByHook.sortOptions.map((sortOption) => (
        <option key={sortOption.handle} value={sortOption.handle}>
          {sortOption.name}
        </option>
      ))}
    </select>
  );
};
