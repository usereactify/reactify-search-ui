import React from "react";

import { ResultPaginationLoadMoreProps } from "../../components";

export type ExampleResultPaginationLoadMoreProps = React.ComponentProps<
  NonNullable<ResultPaginationLoadMoreProps["render"]>
>;

export const ExampleResultPaginationLoadMore: React.FC<ExampleResultPaginationLoadMoreProps> =
  (props) => {
    return (
      <button
        className="rs__pagination__loadmore-button"
        onClick={() => props.handleLoadMore()}
        disabled={!props.hasMore}
      >
        {"Load more"}
      </button>
    );
  };
