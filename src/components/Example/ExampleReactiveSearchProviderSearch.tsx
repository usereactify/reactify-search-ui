import React from "react";

import { ReactifySearchProvider } from "../../components";

export const ExampleReactiveSearchProviderSearch: React.FC = () => (
  <ReactifySearchProvider
    mode="search"
    shopifyPermanentDomain="usereactify-demo.myshopify.com"
  >
    <></>
  </ReactifySearchProvider>
);
