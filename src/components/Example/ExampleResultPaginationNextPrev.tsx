import React from "react";

import { ResultPaginationNextPrevProps } from "../../components";

export type ExampleResultPaginationNextPrevProps = React.ComponentProps<
  NonNullable<ResultPaginationNextPrevProps["render"]>
>;

export const ExampleResultPaginationNextPrev: React.FC<ExampleResultPaginationNextPrevProps> =
  (props) => {
    return (
      <nav className="rs__pagination__nav">
        {props.hasPreviousPage && (
          <a
            className="rs__pagination__prev-link"
            rel="prev"
            onClick={props.handlePreviousPage}
            href={props.buildPagePath(props.actualCurrentPage - 1)}
          >
            {"Prev"}
          </a>
        )}
        {props.hasNextPage && (
          <a
            className="rs__pagination__next-link"
            rel="next"
            onClick={props.handleNextPage}
            href={props.buildPagePath(props.actualCurrentPage + 1)}
          >
            {"Next"}
          </a>
        )}
      </nav>
    );
  };
