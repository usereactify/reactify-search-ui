import React from "react";

import { ClearAllProps } from "../../components";

export type ExampleClearAllProps = React.ComponentProps<
  NonNullable<ClearAllProps["render"]>
>;

export const ExampleClearAll: React.FC<ExampleClearAllProps> = (props) => {
  return <a onClick={props.clearAll}>{"Clear All"}</a>;
};
