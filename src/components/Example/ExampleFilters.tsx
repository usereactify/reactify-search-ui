import React from "react";
import { Filters } from "../Filter";

import { ExampleFilterList } from "./ExampleFilterList";
import { ExampleFilterRange } from "./ExampleFilterRange";
import { ExampleFilterSlider } from "./ExampleFilterSlider";

export const ExampleFilters: React.FC = () => {
  return (
    <div>
      <h3>{"Filters"}</h3>
      <Filters
        renderFilterList={ExampleFilterList}
        renderFilterRange={ExampleFilterRange}
        renderFilterSlider={ExampleFilterSlider}
      />
    </div>
  );
};
