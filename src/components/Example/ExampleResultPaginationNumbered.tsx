import React from "react";

import { ResultPaginationNumberedProps } from "../../components";

export type ExampleResultPaginationNumberedProps = React.ComponentProps<
  NonNullable<ResultPaginationNumberedProps["render"]>
>;

export const ExampleResultPaginationNumbered: React.FC<ExampleResultPaginationNumberedProps> =
  (props) => {
    return (
      <nav className="rs__pagination__nav">
        {props.currentPage > 0 && (
          <a
            className="rs__pagination__prev-link"
            rel="prev"
            href={props.buildPagePath(props.currentPage)}
            onClick={(event) => props.handlePreviousPage(event)}
          >
            {"Prev"}
          </a>
        )}
        {!props.pagesToShow.includes(0) && (
          <a
            className="rs__pagination__pagenumber-link"
            href={props.buildPagePath(1)}
            onClick={(event) => props.handlePageChange(0, event)}
            rel={0 === props.currentPage - 1 ? `prev` : undefined}
          >
            {"1"}
          </a>
        )}
        {!props.pagesToShow.includes(1) && <span>...</span>}
        {props.pagesToShow.map((page) =>
          page === props.currentPage ? (
            <span className="rs__pagination__pagenumber-active" key={page + 1}>
              {page + 1}
            </span>
          ) : (
            <a
              className="rs__pagination__pagenumber-link"
              key={page + 1}
              href={props.buildPagePath(page + 1)}
              onClick={(event) => props.handlePageChange(page, event)}
              rel={
                page === props.currentPage + 1
                  ? `next`
                  : page === props.currentPage - 1
                  ? `prev`
                  : undefined
              }
            >
              {page + 1}
            </a>
          )
        )}
        {!props.pagesToShow.includes(props.totalPages - 2) && <span>...</span>}
        {!props.pagesToShow.includes(props.totalPages - 1) && (
          <a
            className="rs__pagination__pagenumber-link"
            href={props.buildPagePath(props.totalPages)}
            onClick={(event) =>
              props.handlePageChange(props.totalPages - 1, event)
            }
            rel={
              props.totalPages - 1 === props.currentPage + 1
                ? `next`
                : props.totalPages - 1 === props.currentPage - 1
                ? `prev`
                : undefined
            }
          >
            {props.totalPages}
          </a>
        )}
        {props.currentPage + 1 < props.totalPages && (
          <a
            className="rs__pagination__next-link"
            rel="next"
            href={props.buildPagePath(props.currentPage + 2)}
            onClick={(event) => props.handleNextPage(event)}
          >
            {"Next"}
          </a>
        )}
      </nav>
    );
  };
