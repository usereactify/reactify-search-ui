import React from "react";

import { StatsProps } from "../../components";

export type ExampleStatsProps = React.ComponentProps<
  NonNullable<StatsProps["render"]>
>;

export const ExampleStats: React.FC<ExampleStatsProps> = (props) => {
  return (
    <div>
      {"Found "}
      {props.resultStats.numberOfResults}
      {" products"}
    </div>
  );
};
