import React from "react";

import { ResultCardCalloutProps } from "../../components";

export type ExampleResultCardCalloutProps = React.ComponentProps<
  NonNullable<ResultCardCalloutProps["render"]>
>;

export const ExampleResultCardCallout: React.FC<ExampleResultCardCalloutProps> =
  (props) => {
    const styleProp = React.useMemo<React.HTMLAttributes<HTMLElement>["style"]>(
      () => ({
        gridRow: `span ${props.callout.displayRows ?? 1}`,
        gridColumn: `span ${props.callout.displayColumns ?? 1}`,
      }),
      [props.callout]
    );

    return (
      <div
        className="rs__result-card-callout"
        ref={props.itemRef}
        style={styleProp}
      >
        <a
          className="rs__result-card-callout__link"
          onClick={props.handleClick}
          {...(!!props.callout.link && { href: props.callout.link })}
        >
          {props.callout.desktopImage && (
            <img
              className="rs__result-card-callout__image"
              src={props.callout.desktopImage}
              width="100%"
            />
          )}
          <span className="rs__result-card-callout__title">
            {props.callout.title}
          </span>
        </a>
      </div>
    );
  };
