import React from "react";

import { ReactifySearchProvider } from "../../components";

export const ExampleReactiveSearchProviderInstantSearch: React.FC = () => (
  <ReactifySearchProvider
    mode="instant-search"
    shopifyPermanentDomain="usereactify-demo.myshopify.com"
  >
    <></>
  </ReactifySearchProvider>
);
