import React from "react";

import { FiltersSelectedProps } from "../../components";
import { useFilters } from "../../hooks";

export type ExampleFiltersSelectedProps = React.ComponentProps<
  NonNullable<FiltersSelectedProps["render"]>
>;

export const ExampleFiltersSelected: React.FC<ExampleFiltersSelectedProps> = (
  props
) => {
  return (
    <div className="rs__filters-selected">
      <h3 className="rs__filters-selected__name">{"Filters Selected"}</h3>
      <ul className="rs__filters-selected__list">
        {props.selectedFilters.map((selectedFilter) => (
          <ExampleFiltersSelectedItem
            selectedFilter={selectedFilter}
            handleRemove={props.handleRemove}
          />
        ))}
      </ul>
    </div>
  );
};

type ExampleFiltersSelectedItemProps = {
  selectedFilter: ExampleFiltersSelectedProps["selectedFilters"][number];
  handleRemove: ExampleFiltersSelectedProps["handleRemove"];
};

const ExampleFiltersSelectedItem: React.FC<ExampleFiltersSelectedItemProps> = (
  props
) => {
  const filtersHook = useFilters();
  const filter = filtersHook.filters?.find(
    (filter) => filter.handle === props.selectedFilter.key
  );

  return (
    <li
      key={props.selectedFilter.key}
      className="rs__filters-selected__list-item"
    >
      <label className="rs__filters-selected__list-item-label">
        <span
          className="rs__filters-selected__list-item-label"
          onClick={() => props.handleRemove(props.selectedFilter.key)}
        >
          {props.selectedFilter.label}
          {": "}
        </span>

        {filter?.displayType === "single" && filter.displayView === "range" && (
          <span className="rs__filters-selected__list-item-value">
            <span
              onClick={() =>
                props.handleRemove(props.selectedFilter.key, [
                  (props.selectedFilter.value as { label: string }).label,
                ])
              }
            >
              {(props.selectedFilter.value as { label: string }).label}
            </span>
          </span>
        )}
        {filter?.displayType === "single" && filter.displayView !== "range" && (
          <span className="rs__filters-selected__list-item-value">
            <span
              onClick={() =>
                props.handleRemove(props.selectedFilter.key, [
                  props.selectedFilter.value as string,
                ])
              }
            >
              {props.selectedFilter.value as string}
            </span>
          </span>
        )}
        {filter?.displayType === "multi" && filter.displayView === "range" && (
          <span className="rs__filters-selected__list-item-value">
            {(props.selectedFilter.value as Array<{ label: string }>).map(
              (value) => (
                <span
                  onClick={() =>
                    props.handleRemove(props.selectedFilter.key, [value.label])
                  }
                >
                  {value.label}
                </span>
              )
            )}
          </span>
        )}
        {filter?.displayType === "multi" && filter.displayView !== "range" && (
          <span className="rs__filters-selected__list-item-value">
            {(props.selectedFilter.value as Array<string>).map((value) => (
              <span
                onClick={() =>
                  props.handleRemove(props.selectedFilter.key, [value])
                }
              >
                {value}
              </span>
            ))}
          </span>
        )}
        {filter?.displayType === "slider" && (
          <span className="rs__filters-selected__list-item-value">
            <span
              onClick={() => props.handleRemove(props.selectedFilter.key, [""])}
            >
              {`${filter.displaySliderPrefix}${
                (props.selectedFilter.value as any)[0]
              } to ${filter.displaySliderPrefix}${
                (props.selectedFilter.value as any)[1]
              }`}
            </span>
          </span>
        )}
      </label>
    </li>
  );
};
