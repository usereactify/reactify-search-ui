import React from "react";

import { useSearch } from "../../hooks";

export const ExampleHookUseSearch: React.FC = () => {
  const searchHook = useSearch();

  const handleFormSubmit = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      searchHook.submitSearchTerm();
    },
    [searchHook.submitSearchTerm]
  );

  const handleInputChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      searchHook.setSearchTerm(event.target.value);
    },
    [searchHook.setSearchTerm]
  );

  return (
    <form onSubmit={handleFormSubmit}>
      <input
        type="text"
        value={searchHook.searchTerm}
        onChange={handleInputChange}
      />
    </form>
  );
};
