import React from "react";
import ReactSlider from "react-slider";
import { FilterProps } from "../../components";

export type ExampleFilterSliderProps = React.ComponentProps<
  NonNullable<FilterProps["renderFilterSlider"]>
>;

export const ExampleFilterSlider: React.FC<ExampleFilterSliderProps> = (
  props
) => {
  const trackStyleEnds = {
    top: "10px",
    height: "8px",
    background: "rgb(239, 239, 239)",
    border: "1px solid rgb(179, 179, 179)",
    borderRadius: "4px",
  };
  const trackStyleMiddle = {
    ...trackStyleEnds,
    background: "rgb(2, 117, 255)",
    border: "1px solid rgb(74, 117, 187)",
  };

  return (
    <div className="rs__filter">
      <h3 className="rs__filter__name">{props.filter.name}</h3>
      <div
        className="rs__filter__slider"
        style={{
          maxWidth: "300px",
          height: "26px",
          margin: "auto",
          top: "6px",
        }}
      >
        <ReactSlider
          thumbClassName="rs__filter__slider__thumb"
          {...props.filterSliderProps.reactSliderProps}
          renderTrack={(props, state) => (
            <div
              {...props}
              style={{
                ...(state.index === 1 ? trackStyleMiddle : trackStyleEnds),
                ...props.style,
              }}
            ></div>
          )}
          renderThumb={(renderProps, state) => (
            <div
              {...renderProps}
              style={{
                cursor: "grab",
                background: "rgb(2, 117, 255)",
                top: "6px",
                width: "16px",
                height: "16px",
                borderRadius: "100%",
                outline: "none",
                ...renderProps.style,
              }}
            >
              <span
                style={{
                  position: "absolute",
                  top: "-24px",
                  left: "-50%",
                }}
              >
                {props.filter.displaySliderPrefix}
                {state.valueNow}
                {props.filter.displaySliderSuffix}
              </span>
            </div>
          )}
        />
      </div>
    </div>
  );
};
