import React from "react";

import { SearchProps } from "../../components";

export type ExampleSearchProps = React.ComponentProps<
  NonNullable<SearchProps["render"]>
>;

export const ExampleSearch: React.FC<ExampleSearchProps> = (props) => {
  const handleFormSubmit = React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      props.submitSearchTerm();
    },
    [props.submitSearchTerm]
  );

  const handleInputChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      props.setSearchTerm(event.target.value);
    },
    [props.setSearchTerm]
  );

  return (
    <form onSubmit={handleFormSubmit}>
      <input
        type="search"
        placeholder="Search products..."
        value={props.searchTerm}
        onChange={handleInputChange}
      />
    </form>
  );
};
