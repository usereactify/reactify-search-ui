import React from "react";

import { FilterProps } from "../../components";

export type ExampleFilterRangeProps = React.ComponentProps<
  NonNullable<FilterProps["renderFilterRange"]>
>;

export const ExampleFilterRange: React.FC<ExampleFilterRangeProps> = (
  props
) => {
  return (
    <div className="rs__filter">
      <h3 className="rs__filter__name">{props.filter.name}</h3>
      <ul className="rs__filter__list">
        {props.filterRangeProps.options.map((option) => (
          <li key={option.key} className="rs__filter__list-item">
            <label className="rs__filter__list-item-label">
              <input
                className="rs__filter__list-item-input"
                value={option.key}
                type={
                  "multi" === props.filterRangeProps.filter.displayType
                    ? "checkbox"
                    : "radio"
                }
                checked={option.checked}
                onChange={(event) =>
                  props.filterRangeProps.handleChange(event.target.value)
                }
              />
              <span className="rs__filter__list-item-key">{option.label}</span>
            </label>
          </li>
        ))}
      </ul>
    </div>
  );
};
