import React from "react";

import { ReactifySearchProvider, CustomComponent } from "../../components";

export const ExampleCustomComponent: React.FC = () => {
  return (
    <ReactifySearchProvider
      mode="search"
      shopifyPermanentDomain="usereactify-demo.myshopify.com"
      additionalComponentIds={["SensorSpecialProduct"]}
    >
      <CustomComponent
        componentId="SensorSpecialProduct"
        customQuery={() => ({
          query: {
            match: {
              product_type: "Special Product",
            },
          },
        })}
      />
    </ReactifySearchProvider>
  );
};
