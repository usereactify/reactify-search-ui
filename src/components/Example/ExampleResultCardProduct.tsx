import React from "react";

import { ResultCardProductProps } from "../../components";

export type ExampleResultCardProductProps = React.ComponentProps<
  NonNullable<ResultCardProductProps["render"]>
>;

export const ExampleResultCardProduct: React.FC<ExampleResultCardProductProps> =
  (props) => {
    return (
      <div ref={props.itemRef} className="rs__result-card-product">
        <a
          className="rs__result-card-product__link"
          onClick={props.handleClick}
          href={`/products/${props.product.handle}`}
        >
          {props.product.image && (
            <img
              className="rs__result-card-product__image"
              src={props.product.image}
              width="100%"
              height="190px"
            />
          )}
          <span className="rs__result-card-product__title">
            {props.product.title}
          </span>
        </a>
        <span className="rs__result-card-product__price">
          {props.formattedPrice}
        </span>
        {props.onSale && (
          <span
            className="rs__result-card-product__price-sale"
            style={{
              textDecoration: "line-through",
            }}
          >
            {props.formattedCompareAtPrice}
          </span>
        )}
      </div>
    );
  };
