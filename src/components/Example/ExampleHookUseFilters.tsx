import React from "react";

import { useFilters } from "../../hooks";
import { Filter } from "../../components";

export const ExampleHookUseFilters: React.FC = () => {
  const filtersHook = useFilters();

  return (
    <div>
      <h3>{"Filters"}</h3>
      {filtersHook.filters?.map((filter) => (
        <div>
          <p>{filter.name}</p>
          <Filter key={filter.id} filter={filter} />
        </div>
      ))}
    </div>
  );
};
