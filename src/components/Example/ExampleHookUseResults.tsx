import React from "react";

import { useResults } from "../../hooks";
import { ElasticDocumentType } from "../../types";

export const ExampleHookUseResults: React.FC = () => {
  const resultsHook = useResults();

  const collections = resultsHook.results
    .map((result) => {
      if (result.type === ElasticDocumentType.Product) {
        return result.collections;
      }

      return [];
    })
    .flat()
    .filter(
      (item, index, self) =>
        self.findIndex((itemNested) => itemNested?.id === item?.id) === index
    );

  return (
    <ul>
      {collections.map((collection) => (
        <li>
          <a href={`/collections/${collection?.handle}`}>{collection?.title}</a>
        </li>
      ))}
    </ul>
  );
};
