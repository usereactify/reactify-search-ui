import React from "react";

import { ReactifySearchProvider } from "../../components";

export const ExampleReactiveSearchProviderCollection: React.FC = () => (
  <ReactifySearchProvider
    mode="collection"
    shopifyPermanentDomain="usereactify-demo.myshopify.com"
    collectionHandle="example-collection"
  >
    <></>
  </ReactifySearchProvider>
);
