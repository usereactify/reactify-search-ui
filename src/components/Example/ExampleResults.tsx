import React from "react";

import { Results } from "../../components";

import { ExampleResultCardProduct } from "./ExampleResultCardProduct";
import { ExampleResultCardCallout } from "./ExampleResultCardCallout";
import { ExampleResultPaginationNumbered } from "./ExampleResultPaginationNumbered";
import { ExampleResultPaginationLoadMore } from "./ExampleResultPaginationLoadMore";
import { ExampleResultPaginationInfiniteScroll } from "./ExampleResultPaginationInfiniteScroll";
import { ExampleResultPaginationNextPrev } from "./ExampleResultPaginationNextPrev";

export const Component: React.FC = () => {
  return (
    <Results
      listStyle={{
        display: "grid",
        gap: "10px",
        gridTemplateColumns: "repeat(4, minmax(0, 1fr))",
      }}
      renderResultCardProduct={ExampleResultCardProduct}
      renderResultCardCallout={ExampleResultCardCallout}
      renderPaginationNumbered={ExampleResultPaginationNumbered}
      renderPaginationLoadMore={ExampleResultPaginationLoadMore}
      renderPaginationNextPrev={ExampleResultPaginationNextPrev}
      renderPaginationInfiniteScroll={ExampleResultPaginationInfiniteScroll}
    />
  );
};
