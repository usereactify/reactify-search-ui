import React from "react";

import { SortByProps } from "../../components";

export type ExampleSortByProps = React.ComponentProps<
  NonNullable<SortByProps["render"]>
>;

export const ExampleSortBy: React.FC<ExampleSortByProps> = (props) => {
  return (
    <select
      className="rs__sortby__select"
      value={props.sortOption?.handle}
      onChange={(event) => props.setSortOption(event.target.value)}
    >
      {props.sortOptions.map((sortOption) => (
        <option
          key={sortOption.handle}
          className="rs__sortby__option"
          value={sortOption.handle}
        >
          {sortOption.name}
        </option>
      ))}
    </select>
  );
};
