import React from "react";

import { ElasticHit, ReactivesearchError } from "../../types";
import { ResultStateProvider, ExampleStats } from "../../components";

export type StatsProps = {
  /** Render method */
  render?: React.FC<{
    isLoading: boolean;
    hits: {
      hidden: number;
      time: number;
      total: number;
      hits: Array<ElasticHit>;
    };
    resultStats: {
      hidden: number;
      numberOfResults?: number;
      promoted: number;
      time: number;
    };
    error?: ReactivesearchError;
  }>;
};

export const Stats: React.FC<StatsProps> = (props) => {
  const RenderComponent = props.render ?? ExampleStats;

  return (
    <ResultStateProvider
      render={(renderProps) => <RenderComponent {...renderProps} />}
    />
  );
};
