import React from "react";
import SelectedFilters from "@appbaseio/reactivesearch/lib/components/basic/SelectedFilters";

import { ExampleClearAll } from "../../components";
import { useFilters } from "../../hooks";

export type ClearAllProps = {
  /** Render method */
  render?: React.FC<{
    clearAll: () => void;
  }>;
};

export const ClearAll: React.FC<ClearAllProps> = (props) => {
  const RenderComponent = props.render ?? ExampleClearAll;

  const filtersHook = useFilters();

  return (
    <SelectedFilters
      render={(renderProps) => {
        const selectedFilters = Object.values(renderProps.selectedValues)
          .map((value) => value)
          .filter(
            (item: any) =>
              item?.URLParams &&
              item?.showFilter &&
              (!!item?.value?.length || !!item?.value?.label)
          );

        const handleClearAll = () => {
          for (const filter of filtersHook.filters ?? []) {
            if (filter.displayType === "slider") {
              const event = new CustomEvent<{
                handle: string;
                value: [];
              }>(`@usereactify/search:filter:${filter.handle}:update`, {
                detail: {
                  handle: filter.handle,
                  value: [],
                },
              });
              window.dispatchEvent(event);
            }
            if (filter.displayType === "multi") {
              if (filter.displayView === "range") {
                const event = new CustomEvent<{
                  handle: string;
                  value: Array<string>;
                }>(`@usereactify/search:filter:${filter.handle}:update`, {
                  detail: {
                    handle: filter.handle,
                    value: [],
                  },
                });
                window.dispatchEvent(event);
              }
            }
          }
          renderProps.clearValues();
        };

        if (!selectedFilters.length) {
          return null;
        }

        return <RenderComponent clearAll={handleClearAll} />;
      }}
    />
  );
};
