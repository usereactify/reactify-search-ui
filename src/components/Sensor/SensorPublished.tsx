import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";

import { useFlags } from "../../hooks";

export const SensorPublished: React.FC = () => {
  const flagsHook = useFlags();

  if (!flagsHook.flags["reactify-search:flag_sensor_published"]) {
    return null;
  }

  return (
    <ReactiveComponent
      componentId="SensorPublished"
      customQuery={() => ({
        query: {
          match: {
            published: true,
          },
        },
      })}
    />
  );
};
