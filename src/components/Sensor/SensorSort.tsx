import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";

import { ConfigCuration, ConfigSort } from "../../types";
import {
  useReactifySearchContext,
  useSortBy,
  useCuration,
  ReactifySearchMode,
  useFlags,
} from "../../hooks";

export const SensorSort: React.FC = () => {
  const flagsHook = useFlags();
  const { options, config } = useReactifySearchContext();
  const { curation } = useCuration();
  const { sortOption } = useSortBy();

  const { sort, query } = React.useMemo(() => {
    return {
      sort: buildSort({
        mode: options.mode,
        curation,
        sortOption,
        collectionHandle:
          options.mode === "collection" ? options.collectionHandle : undefined,
      }),
      query: buildQuery({
        sortOption,
        curation,
      }),
    };
  }, [options, config, sortOption, curation]);

  if (!flagsHook.flags["reactify-search:flag_sensor_sort"]) {
    return null;
  }

  return (
    <ReactiveComponent
      componentId="SensorSort"
      customQuery={() => ({
        sort,
        query,
      })}
    />
  );
};

const buildSort = (args: {
  mode: ReactifySearchMode;
  curation?: ConfigCuration;
  sortOption?: ConfigSort;
  collectionHandle?: string;
}): any[] => {
  const { mode, curation, sortOption, collectionHandle } = args;

  // curation positions are only applied for the default `collections.position` or `_score` sort
  // if the sort is something else, apply a normal sort which applies what the user has requested
  if (mode !== "instant-search") {
    if (
      sortOption &&
      !["_score", "collections.position"].includes(sortOption.field)
    ) {
      return [{ [sortOption.field]: sortOption.direction }];
    }
  }

  const sorts = [];

  // collection curation pin sorting
  if (mode === "collection" && collectionHandle) {
    if (curation?.collectionHandle) {
      sorts.push({
        "curations.position": {
          unmapped_type: "long",
          order: "asc",
          nested: {
            path: "curations",
            filter: {
              term: {
                [`curations.collectionHandle.keyword`]:
                  curation.collectionHandle,
              },
            },
          },
        },
      });
    }
  }

  // search curation pin sorting
  if (mode === "search" || mode === "instant-search") {
    if (curation?.searchTerm) {
      sorts.push({
        "curations.position": {
          unmapped_type: "long",
          order: "asc",
          nested: {
            path: "curations",
            filter: {
              term: {
                [`curations.searchTerm.keyword`]:
                  curation.searchTerm?.toLowerCase(),
              },
            },
          },
        },
      });
    }
  }

  // curation boost sorting
  if (curation) {
    if (0 < curation.boosting.groupings.length) {
      const groupings = curation.boosting.groupings.sort((a, b) =>
        a.position > b.position ? 1 : -1
      );

      for (const grouping of groupings) {
        try {
          const query = JSON.parse(grouping.query);
          if (query) sorts.push(query);
        } catch {
          console.error(
            `query could not be parsed for boost grouping`,
            grouping
          );
        }
      }
    }

    if (0 < curation.boosting.sortings.length) {
      const sortings = curation.boosting.sortings.sort((a, b) =>
        a.position > b.position ? 1 : -1
      );

      for (const sorting of sortings) {
        try {
          const query = JSON.parse(sorting.query);
          if (query) sorts.push(query);
        } catch {
          console.error(`query could not be parsed for boost sorting`, sorting);
        }
      }
    }
  }

  // default collection sorting
  if (mode === "collection" && collectionHandle) {
    sorts.push(...mapCollectionPositionSortClause(collectionHandle));
  }

  // default search sorting
  if (mode === "search" || mode === "instant-search") {
    sorts.push("_score");
  }

  return sorts;
};

/**
 * Return a sort clause which sorts by position within the given collection.
 */
function mapCollectionPositionSortClause(collectionHandle: string) {
  return [
    {
      "collections.position": {
        order: "asc",
        nested: {
          path: "collections",
          filter: {
            term: {
              "collections.handle.keyword": collectionHandle,
            },
          },
        },
      },
    },
  ];
}

const buildQuery = (args: {
  sortOption?: ConfigSort;
  curation?: ConfigCuration;
}): Record<string, any> | undefined => {
  const { curation, sortOption } = args;

  if (
    !curation ||
    ["global_collection", "global_search"].includes(curation.id)
  ) {
    return undefined;
  }

  return {
    bool: {
      must_not: [
        {
          nested: {
            path: "curations",
            query: {
              bool: {
                must: [
                  {
                    term: {
                      [`curations.${
                        "collection" === curation.type
                          ? "collectionHandle"
                          : "searchTerm"
                      }.keyword`]:
                        "collection" === curation.type
                          ? curation.collectionHandle
                          : curation.searchTerm?.toLowerCase(),
                    },
                  },
                  {
                    term: {
                      "curations.hidden": true,
                    },
                  },
                ],
              },
            },
          },
        },
        // hide callout when not sorting by _score or collections.position
        ...(sortOption &&
        !["_score", "collections.position"].includes(sortOption.field)
          ? [
              {
                term: {
                  type: "callout",
                },
              },
            ]
          : []),
      ],
    },
  };
};
