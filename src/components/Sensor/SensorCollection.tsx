import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";

import { useFlags, useReactifySearchContext } from "../../hooks";

export const SensorCollection: React.FC = () => {
  const flagsHook = useFlags();
  const { options } = useReactifySearchContext();

  if (options.mode !== "collection") {
    return null;
  }

  if (!flagsHook.flags["reactify-search:flag_sensor_collection"]) {
    return null;
  }

  return (
    <ReactiveComponent
      componentId="SensorCollection"
      customQuery={() => ({
        query: {
          bool: {
            should: [
              {
                nested: {
                  path: "collections",
                  query: {
                    term: {
                      "collections.handle.keyword": options.collectionHandle,
                    },
                  },
                },
              },
              {
                nested: {
                  path: "curations",
                  query: {
                    term: {
                      "curations.collectionHandle.keyword":
                        options.collectionHandle,
                    },
                  },
                },
              },
            ],
          },
        },
      })}
    />
  );
};
