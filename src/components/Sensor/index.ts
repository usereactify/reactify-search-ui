export * from "./SensorSort";
export * from "./SensorPublished";
export * from "./SensorCollection";
export * from "./SensorSearchTerm";
export * from "./SensorInventoryAvailable";

export * from "./Sensors";

// this is consumed by the react prop in various components
export const SENSOR_IDS = [
  "SensorSort",
  "SensorPublished",
  "SensorCollection",
  "SensorSearchTerm",
  "SensorInventoryAvailable",
];
