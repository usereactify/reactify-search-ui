import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";

import { useFilters, useFlags, useReactifySearchContext } from "../../hooks";

export const SensorInventoryAvailable: React.FC = () => {
  const { options } = useReactifySearchContext();
  const flagsHook = useFlags();
  const { filterStack } = useFilters();

  if (!filterStack || "show_all" === filterStack.inventoryVisibility) {
    return null;
  }

  if (!flagsHook.flags["reactify-search:flag_sensor_inventoryavailable"]) {
    return null;
  }

  return (
    <ReactiveComponent
      componentId="SensorInventoryAvailable"
      customQuery={() => ({
        query: {
          bool: {
            should: [
              {
                bool: {
                  must: [
                    {
                      term: {
                        type: {
                          value: "product",
                        },
                      },
                    },
                    {
                      nested: {
                        path: "variants",
                        query: {
                          match: { "variants.available": true },
                        },
                      },
                    },
                  ],
                },
              },
              ...(options.mode === "instant-search"
                ? []
                : [
                    {
                      term: {
                        type: {
                          value: "callout",
                        },
                      },
                    },
                  ]),
            ],
            minimum_should_match: "1",
          },
        },
      })}
    />
  );
};
