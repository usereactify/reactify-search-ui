import React from "react";

import { useReactifySearchContext } from "../../hooks";

import { SensorSort } from "./SensorSort";
import { SensorPublished } from "./SensorPublished";
import { SensorCollection } from "./SensorCollection";
import { SensorSearchTerm } from "./SensorSearchTerm";
import { SensorInventoryAvailable } from "./SensorInventoryAvailable";

export type SensorsProps = {
  /** This component does not support any props */
};

export const Sensors: React.FC<SensorsProps> = () => {
  const { options } = useReactifySearchContext();

  if (options.mode === "search" || options.mode === "instant-search") {
    return (
      <>
        <SensorSort />
        <SensorPublished />
        <SensorSearchTerm />
        <SensorInventoryAvailable />
      </>
    );
  }

  if (options.mode === "collection") {
    return (
      <>
        <SensorSort />
        <SensorPublished />
        <SensorCollection />
        <SensorInventoryAvailable />
      </>
    );
  }

  return null;
};
