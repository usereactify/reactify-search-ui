import React from "react";
import { useDebounceFn } from "ahooks";
import DataSearch from "@appbaseio/reactivesearch/lib/components/search/DataSearch";

import {
  useAnalytics,
  useFlags,
  useReactifySearchContext,
  useSearch,
} from "../../hooks";

export const SensorSearchTerm: React.FC = () => {
  const flagsHook = useFlags();
  const { options, config } = useReactifySearchContext();
  const { track } = useAnalytics();
  const { searchTerm } = useSearch();

  const [searchTermDebounced, setSearchTermDebounced] =
    React.useState<string>(searchTerm);

  // ignore search fields only set for instant search
  const searchFields = React.useMemo(() => {
    if (options.mode === "search") {
      return config.fields.filter((field) =>
        ["always_search", "search_page"].includes(field.searchType)
      );
    }

    if (options.mode === "instant-search") {
      return config.fields.filter((field) =>
        ["always_search", "instant_search"].includes(field.searchType)
      );
    }

    return [];
  }, [config.fields]);

  const { run: runDebouncedTriggerQuery, cancel: cancelDebouncedTriggerQuery } =
    useDebounceFn(
      (value: string) => {
        setSearchTermDebounced(value);

        let trimmedSearchTerm = searchTerm.trim();

        if (
          !trimmedSearchTerm ||
          (trimmedSearchTerm && trimmedSearchTerm.length < 3)
        ) {
          return;
        }

        track({
          eventName: "search",
          payload: {
            searchTerm: trimmedSearchTerm,
          },
        });
      },
      {
        wait: 300,
      }
    );

  React.useEffect(() => {
    runDebouncedTriggerQuery(searchTerm);
  }, [searchTerm]);

  if (searchFields.length === 0) {
    return null;
  }

  if (!flagsHook.flags["reactify-search:flag_sensor_searchterm"]) {
    return null;
  }

  return (
    <>
      <DataSearch
        fuzziness={1}
        queryFormat="and"
        autosuggest={false}
        value={searchTermDebounced}
        componentId="SensorSearchTerm"
        style={{ display: "none" }}
        dataField={searchFields.map((field) => field.field)}
        fieldWeights={searchFields.map((field) => field.importance)}
        customQuery={(value, props) => {
          const conditions: Array<unknown> = [];

          if (
            flagsHook.flags["reactify-search:flag_sensor_searchterm_value"] &&
            value
          ) {
            conditions.push({
              nested: {
                path: "curations",
                query: {
                  term: {
                    "curations.searchTerm.keyword": value.toLowerCase(),
                  },
                },
              },
            });
          }

          const fieldsPhrase = props.dataField
            .filter((field: string) =>
              !flagsHook.flags[
                "reactify-search:flag_sensor_searchterm_phrase_synonyms"
              ]
                ? !field.includes("synonym")
                : true
            )
            .map(
              (field: string, index: number) =>
                `${field}^${props.fieldWeights[index]}`
            );
          if (
            flagsHook.flags["reactify-search:flag_sensor_searchterm_phrase"] &&
            fieldsPhrase.length > 0
          ) {
            conditions.push({
              multi_match: {
                query: value,
                fields: fieldsPhrase,
                type: "phrase",
                operator: "and",
              },
            });
          }

          const fieldsPhrasePrefix = props.dataField
            .filter((field: string) =>
              !flagsHook.flags[
                "reactify-search:flag_sensor_searchterm_phrase_prefix_synonyms"
              ]
                ? !field.includes("synonym")
                : true
            )
            .map(
              (field: string, index: number) =>
                `${field}^${props.fieldWeights[index]}`
            )
            .filter((field: string) => !field.includes("."));

          if (
            flagsHook.flags[
              "reactify-search:flag_sensor_searchterm_phrase_prefix"
            ] &&
            fieldsPhrasePrefix.length > 0
          ) {
            conditions.push({
              multi_match: {
                query: value,
                fields: fieldsPhrasePrefix,
                type: "phrase_prefix",
                operator: "and",
              },
            });
          }

          const fieldsCrossFields = props.dataField
            .filter((field: string) =>
              !flagsHook.flags[
                "reactify-search:flag_sensor_searchterm_cross_fields_synonyms"
              ]
                ? !field.includes("synonym")
                : true
            )
            .map(
              (field: string, index: number) =>
                `${field}^${props.fieldWeights[index]}`
            );
          if (
            flagsHook.flags[
              "reactify-search:flag_sensor_searchterm_cross_fields"
            ] &&
            fieldsCrossFields.length > 0
          ) {
            conditions.push({
              multi_match: {
                query: value,
                fields: fieldsCrossFields,
                type: "cross_fields",
                operator: "and",
              },
            });
          }

          const fieldsSpanFirst: any = props.dataField
            .filter((field: string) =>
              !flagsHook.flags[
                "reactify-search:flag_sensor_searchterm_span_first_synonyms"
              ]
                ? !field.includes("synonym")
                : true
            )
            .filter((field: string) => {
              if (field.endsWith(".keyword")) {
                return false;
              }

              return true;
            })
            .map((field: string) => ({
              span_first: {
                match: {
                  span_term: { [`${field}`]: value },
                },
                end: 1,
              },
            }));
          if (
            flagsHook.flags[
              "reactify-search:flag_sensor_searchterm_span_first"
            ] &&
            fieldsSpanFirst.length > 0
          ) {
            conditions.push(...fieldsSpanFirst);
          }

          return {
            query: {
              bool: {
                should: conditions,
                minimum_should_match: "1",
              },
            },
          };
        }}
      />
    </>
  );
};
