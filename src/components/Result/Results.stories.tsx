import React from "react";
import { Story, Meta } from "@storybook/react";

import { Results } from "./Results";

export default {
  title: "Results",
  component: Results,
} as Meta;

const Template: Story<React.ComponentProps<typeof Results>> = (args) => (
  <Results {...args} />
);

export const DefaultResults = Template.bind({});
DefaultResults.args = {};
