import React from "react";

import { ReactivesearchResultProps } from "../../types";
import { usePaginationLoadable } from "../../hooks";
import {
  ResultsProps,
  ExampleResultPaginationLoadMore,
} from "../../components";

export type ResultPaginationLoadMoreProps = ReactivesearchResultProps & {
  render?: ResultsProps["renderPaginationLoadMore"];
};

export const ResultPaginationLoadMore: React.FC<ResultPaginationLoadMoreProps> =
  (props) => {
    const paginationLoadMoreHook = usePaginationLoadable(props);

    const RenderPaginationComponent =
      props.render ?? ExampleResultPaginationLoadMore;

    return <RenderPaginationComponent {...paginationLoadMoreHook} />;
  };
