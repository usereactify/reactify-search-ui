import React from "react";
import { useInView } from "react-intersection-observer";

import { ElasticCallout } from "../../types";
import { useAnalytics } from "../../hooks";
import { ResultsProps, ExampleResultCardCallout } from "..";

export type ResultCardCalloutProps = {
  pagePosition: number;
  document: ElasticCallout;
  callout: ElasticCallout["callout"];
  render?: ResultsProps["renderResultCardCallout"];
  onView?: () => void;
};

export const ResultCardCallout: React.FC<ResultCardCalloutProps> = (props) => {
  const { track } = useAnalytics();
  const { ref, inView } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  const handleClick = React.useCallback(() => {
    track({
      eventName: "clickPromotion",
      payload: {
        link: props.callout.link,
        title: props.callout.title,
      },
    });
  }, [track, props.callout]);

  const handleView = React.useCallback(() => {
    track({
      eventName: "viewPromotion",
      payload: {
        link: props.callout.link,
        title: props.callout.title,
      },
    });
  }, [track, props.callout]);

  React.useEffect(() => {
    if (inView) {
      props.onView?.();
      handleView();
    }
  }, [inView]);

  const RenderComponent = props.render ?? ExampleResultCardCallout;

  return <RenderComponent {...props} itemRef={ref} handleClick={handleClick} />;
};
