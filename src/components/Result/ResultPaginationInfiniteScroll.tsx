import React from "react";

import { ReactivesearchResultProps } from "../../types";
import { usePaginationLoadable } from "../../hooks";
import {
  ResultsProps,
  ExampleResultPaginationInfiniteScroll,
} from "../../components";

export type ResultPaginationInfiniteScrollProps = ReactivesearchResultProps & {
  render?: ResultsProps["renderPaginationInfiniteScroll"];
};

export const ResultPaginationInfiniteScroll: React.FC<ResultPaginationInfiniteScrollProps> =
  (props) => {
    const usePaginationLoadableHook = usePaginationLoadable(props);

    const RenderPaginationComponent =
      props.render ?? ExampleResultPaginationInfiniteScroll;

    return <RenderPaginationComponent {...usePaginationLoadableHook} />;
  };
