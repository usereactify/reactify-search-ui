import React from "react";
import ReactiveList from "@appbaseio/reactivesearch/lib/components/result/ReactiveList";

import { ResultCardCallout } from "./ResultCardCallout";
import { ResultCardProduct } from "./ResultCardProduct";
import { ResultPaginationNumbered } from "./ResultPaginationNumbered";
import { ResultPaginationLoadMore } from "./ResultPaginationLoadMore";
import { ResultPaginationNextPrev } from "./ResultPaginationNextPrev";

import {
  ElasticCallout,
  ElasticProduct,
  ElasticDocumentType,
  ReactivesearchError,
  ReactivesearchResultProps,
  ReactivesearchPaginationProps,
} from "../../types";
import {
  useReactifySearchContext,
  useAnalytics,
  useSearch,
  useFilters,
  useReactiveReactiveListProps,
  useProductPrice,
  usePagination,
  usePaginationLoadable,
  useResults,
} from "../../hooks";
import { debug } from "../../utility";
import { ResultPaginationInfiniteScroll } from "./ResultPaginationInfiniteScroll";

export type ResultProps = ReactivesearchResultProps & {
  callouts: ElasticCallout[];
  products: ElasticProduct[];
};

export type ResultsProps = {
  /** Style prop for the list wrapper */
  listStyle?: React.HTMLAttributes<HTMLElement>["style"];
  /** Classname prop for the list wrapper */
  listClassName?: string;
  /** Render method called when an error occurs */
  renderError?: React.FC<{ error: ReactivesearchError }>;
  /** Render method called while loading for the first time */
  renderLoading?: React.FC;
  /** Render method called when no results are found */
  renderNoResults?: React.FC;
  /** Render method called once for each product result */
  renderResultCardProduct?: React.FC<
    ReturnType<typeof useProductPrice> & {
      pagePosition: number;
      product: ElasticProduct;
      document: ElasticProduct;
      itemRef: (node?: Element | null) => void;
      handleClick: () => void;
    }
  >;
  /** Render method called once for each callout result */
  renderResultCardCallout?: React.FC<{
    pagePosition: number;
    document: ElasticCallout;
    callout: ElasticCallout["callout"];
    itemRef: (node?: Element | null) => void;
    handleClick: () => void;
  }>;
  /** Render method called for pagination type "pagination" */
  renderPaginationNumbered?: React.FC<ReturnType<typeof usePagination>>;
  /** Render method called for pagination type "next_prev" */
  renderPaginationNextPrev?: React.FC<ReturnType<typeof usePagination>>;
  /** Render method called for pagination type "load_more" */
  renderPaginationLoadMore?: React.FC<ReturnType<typeof usePaginationLoadable>>;
  /** Render method called for pagination type "infinite_scroll" */
  renderPaginationInfiniteScroll?: React.FC<
    ReturnType<typeof usePaginationLoadable>
  >;
  /** Define which fields should be included in results data */
  includeFields?: Array<string>;
  /** Define which fields should be excluded from results data */
  excludeFields?: Array<string>;
  /** Advanced Usage: Override the default amount of results per page */
  pageSize?: number;
  /** Advanced Usage: Override the default scrollTarget used to determine when infinite load should be triggered (infinite scroll) */
  infiniteScrollContainer?: React.ComponentProps<
    typeof ReactiveList
  >["scrollTarget"];
  /** Advanced Usage: Provide a specific result position to trigger loading more results (infinite scroll) */
  infiniteScrollPosition?: number;
};

export const Results: React.FC<ResultsProps> = (props) => {
  const reactiveReactiveListProps = useReactiveReactiveListProps({
    pageSize: props.pageSize,
    scrollTarget: props.infiniteScrollContainer,
  });
  const { track } = useAnalytics();
  const { searchTerm } = useSearch();

  const fieldsInclude = React.useMemo(() => {
    return [
      ...(props.includeFields ?? []),
      "id",
      "title",
      "type",
      "handle",
      "image",
      "variants.id",
      "variants.presentment_prices",
      "callout",
    ];
  }, [props.includeFields]);
  const fieldsExclude = React.useMemo(() => {
    return [...(props.excludeFields ?? [])];
  }, [props.excludeFields]);

  return (
    <ReactiveList
      {...reactiveReactiveListProps}
      includeFields={fieldsInclude}
      excludeFields={fieldsExclude}
      render={(reactivesearchResultProps: ReactivesearchResultProps) => (
        <ResultsInner
          {...props}
          reactivesearchResultProps={reactivesearchResultProps}
        />
      )}
      onData={(renderProps) => {
        if (renderProps.resultStats.numberOfResults === 0) {
          let trimmedSearchTerm = searchTerm.trim();

          if (
            !trimmedSearchTerm ||
            (trimmedSearchTerm && trimmedSearchTerm.length < 3)
          ) {
            return;
          }

          track({
            eventName: "zeroResults",
            payload: {
              searchTerm: trimmedSearchTerm,
            },
          });
        }
      }}
      renderPagination={(
        reactivesearchPaginationProps: ReactivesearchPaginationProps
      ) => (
        <ResultsPaginationStack {...props} {...reactivesearchPaginationProps} />
      )}
    />
  );
};

const ResultsRenderErrorComponent: ResultsProps["renderError"] = (props) => {
  return (
    <div>
      {"Error: "}
      {props.error.statusText}
    </div>
  );
};

const ResultsRenderLoadingComponent: ResultsProps["renderLoading"] = () => {
  return <div>{"Loading"}</div>;
};

const ResultsRenderNoResultsComponent: ResultsProps["renderNoResults"] = () => {
  return <div>{"No results"}</div>;
};

const ResultsInner: React.FC<
  ResultsProps & { reactivesearchResultProps: ReactivesearchResultProps }
> = (props) => {
  const context = useReactifySearchContext();
  const filtersHook = useFilters();
  const resultsHook = useResults();
  const initialSearchHasRun = React.useMemo(
    () =>
      "undefined" !==
      typeof props.reactivesearchResultProps.resultStats.numberOfResults,
    [props.reactivesearchResultProps]
  );

  const listStyle = React.useMemo<React.HTMLAttributes<HTMLElement>["style"]>(
    () =>
      props.listStyle ?? {
        display: "grid",
        gap: "8px",
        gridTemplateColumns: "repeat(4, minmax(0, 1fr))",
      },
    [props.listStyle]
  );

  const infiniteScrollPosition = React.useMemo<number>(() => {
    if (!props.infiniteScrollPosition) {
      return NaN;
    }

    const scrollPositionOffset =
      props.reactivesearchResultProps.resultStats.displayedResults -
      (props.pageSize ?? filtersHook.filterStack?.pageSize ?? 0);

    return scrollPositionOffset + (props.infiniteScrollPosition ?? 0);
  }, [
    props.pageSize ?? filtersHook.filterStack?.pageSize,
    props.reactivesearchResultProps.resultStats.displayedResults,
    props.infiniteScrollPosition,
  ]);

  React.useEffect(() => {
    resultsHook.setResults(props.reactivesearchResultProps.data);
    resultsHook.setResultStats(props.reactivesearchResultProps.resultStats);
  }, [
    props.reactivesearchResultProps.resultStats.numberOfResults,
    props.reactivesearchResultProps.data.map((item) => item._id).join(""),
  ]);

  if (props.reactivesearchResultProps.error) {
    const RenderErrorComponent =
      props.renderError ?? ResultsRenderErrorComponent;

    return (
      <RenderErrorComponent error={props.reactivesearchResultProps.error} />
    );
  }

  if (!initialSearchHasRun) {
    const RenderLoadingComponent =
      props.renderLoading ?? ResultsRenderLoadingComponent;
    return <RenderLoadingComponent />;
  }

  if (
    !props.reactivesearchResultProps.loading &&
    !props.reactivesearchResultProps.resultStats.numberOfResults
  ) {
    const RenderNoResultsComponent =
      props.renderNoResults ?? ResultsRenderNoResultsComponent;

    return <RenderNoResultsComponent />;
  }

  if (["search", "instant-search"].includes(context.options.mode)) {
    if (!context.search.searchTerm) {
      const RenderNoResultsComponent =
        props.renderNoResults ?? ResultsRenderNoResultsComponent;

      return <RenderNoResultsComponent />;
    }
  }

  return (
    <>
      <section style={listStyle} className={props.listClassName}>
        {props.reactivesearchResultProps.data.map((item, key) => {
          if (ElasticDocumentType.Product === item.type) {
            return (
              <ResultCardProduct
                key={item._id}
                product={item}
                document={item}
                pagePosition={key + 1}
                render={props.renderResultCardProduct}
                {...(infiniteScrollPosition === key + 1
                  ? {
                      onView: () => props.reactivesearchResultProps.loadMore(),
                    }
                  : {})}
              />
            );
          }

          if (ElasticDocumentType.Callout === item.type) {
            return (
              <ResultCardCallout
                key={item._id}
                document={item}
                pagePosition={key + 1}
                callout={item.callout}
                render={props.renderResultCardCallout}
                {...(infiniteScrollPosition === key + 1
                  ? {
                      onView: () => props.reactivesearchResultProps.loadMore(),
                    }
                  : {})}
              />
            );
          }

          return null;
        })}
      </section>
      <ResultsPaginationStackLoadable
        {...props}
        {...props.reactivesearchResultProps}
      />
    </>
  );
};

/** Handles rendering pagination for types that use "pagination props" */
const ResultsPaginationStack: React.FC<
  ResultsProps & ReactivesearchPaginationProps
> = (props) => {
  const { options } = useReactifySearchContext();
  const { filterStack } = useFilters();

  if (options.mode === "instant-search") {
    return null;
  }

  if ("pagination" === filterStack?.paginationType) {
    return (
      <ResultPaginationNumbered
        {...props}
        render={props.renderPaginationNumbered}
      />
    );
  }

  if ("next_prev" === filterStack?.paginationType) {
    return (
      <ResultPaginationNextPrev
        {...props}
        render={props.renderPaginationNextPrev}
      />
    );
  }

  return null;
};

/** Handles rendering pagination for types that use "result props" */
const ResultsPaginationStackLoadable: React.FC<
  ResultsProps & { reactivesearchResultProps: ReactivesearchResultProps }
> = (props) => {
  const { options } = useReactifySearchContext();
  const { filterStack } = useFilters();

  if (options.mode === "instant-search") {
    return null;
  }

  if ("load_more" === filterStack?.paginationType) {
    return (
      <ResultPaginationLoadMore
        {...props.reactivesearchResultProps}
        render={props.renderPaginationLoadMore}
      />
    );
  }

  if ("infinite_scroll" === filterStack?.paginationType) {
    return (
      <ResultPaginationInfiniteScroll
        {...props.reactivesearchResultProps}
        render={props.renderPaginationInfiniteScroll}
      />
    );
  }

  return null;
};
