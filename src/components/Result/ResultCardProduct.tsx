import React from "react";
import { useInView } from "react-intersection-observer";

import type { ElasticProduct } from "../../types";
import { useProductPrice, useAnalytics } from "../../hooks";
import { ResultsProps, ExampleResultCardProduct } from "../../components";

export type ResultCardProductProps = {
  pagePosition: number;
  product: ElasticProduct;
  document: ElasticProduct;
  render?: ResultsProps["renderResultCardProduct"];
  onView?: () => void;
};

export const ResultCardProduct: React.FC<ResultCardProductProps> = (props) => {
  const productPrice = useProductPrice(props.product);

  const { track } = useAnalytics();
  const { ref, inView } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  const handleClick = React.useCallback(() => {
    track({
      eventName: "clickProduct",
      payload: {
        elasticProduct: {
          id: props.product.id,
          title: props.product.title,
        },
      },
    });
  }, [track, props.product]);

  const handleView = React.useCallback(() => {
    track({
      eventName: "viewProduct",
      payload: {
        elasticProduct: {
          id: props.product.id,
          title: props.product.title,
        },
      },
    });
  }, [track, props.product]);

  React.useEffect(() => {
    if (inView) {
      props.onView?.();
      handleView();
    }
  }, [inView]);

  const RenderComponent = props.render ?? ExampleResultCardProduct;

  return (
    <RenderComponent
      {...productPrice}
      pagePosition={props.pagePosition}
      product={props.product}
      document={props.document}
      itemRef={ref}
      handleClick={handleClick}
    />
  );
};
