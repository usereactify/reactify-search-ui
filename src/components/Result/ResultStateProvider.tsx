import React from "react";
import StateProvider from "@appbaseio/reactivesearch/lib/components/basic/StateProvider";

import { ReactivesearchSearchStatePage } from "../../types";

export type ResultStateProvidedProps = {
  render: React.FC<ReactivesearchSearchStatePage>;
};

export const ResultStateProvider: React.FC<ResultStateProvidedProps> = (
  props
) => (
  <StateProvider
    componentIds={["page"]}
    includeKeys={["isLoading", "hits", "resultStats", "error"]}
    render={(state) => {
      const RenderComponent = props.render;

      return <RenderComponent {...state.searchState.page} />;
    }}
  />
);
