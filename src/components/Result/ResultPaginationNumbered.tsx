import React from "react";

import { ReactivesearchPaginationProps } from "../../types";
import { usePagination } from "../../hooks";
import {
  ResultsProps,
  ExampleResultPaginationNumbered,
} from "../../components";

export type ResultPaginationNumberedProps = ReactivesearchPaginationProps & {
  render?: ResultsProps["renderPaginationNumbered"];
};

export const ResultPaginationNumbered: React.FC<ResultPaginationNumberedProps> =
  (props) => {
    const paginationHook = usePagination(props);

    if (!paginationHook.totalPages) {
      return null;
    }

    const RenderPaginationComponent =
      props.render ?? ExampleResultPaginationNumbered;

    return <RenderPaginationComponent {...paginationHook} />;
  };
