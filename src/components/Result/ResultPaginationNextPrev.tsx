import React from "react";

import { usePagination } from "../../hooks";
import { ReactivesearchPaginationProps } from "../../types";
import {
  ResultsProps,
  ExampleResultPaginationNextPrev,
} from "../../components";

export type ResultPaginationNextPrevProps = ReactivesearchPaginationProps & {
  render?: ResultsProps["renderPaginationNextPrev"];
};

export const ResultPaginationNextPrev: React.FC<ResultPaginationNextPrevProps> =
  (props) => {
    const paginationHook = usePagination(props);

    const RenderPaginationComponent =
      props.render ?? ExampleResultPaginationNextPrev;

    return <RenderPaginationComponent {...paginationHook} />;
  };
