import React from "react";

import {
  CustomComponent,
  ExampleSuggestions,
  SENSOR_IDS,
} from "../../components";
import { useSearch } from "../../hooks";

export type SuggestionsProps = {
  /** The field which should be used for autocompletion */
  field: "title";
  /** Render method */
  render?: React.FC<{
    suggestions: Array<{
      text: string;
    }>;
  }>;
};

export const Suggestions: React.FC<SuggestionsProps> = (props) => {
  const RenderComponent = props.render ?? ExampleSuggestions;

  const { searchTerm } = useSearch();

  return (
    <CustomComponent
      componentId="SensorSuggestions"
      defaultQuery={() => {
        return {
          suggest: {
            suggestions: {
              text: searchTerm,
              term: {
                field: props.field,
                sort: "score",
                suggest_mode: "always",
              },
            },
          },
        };
      }}
      react={{
        and: SENSOR_IDS,
      }}
      render={(renderProps) => {
        const props: React.ComponentProps<
          NonNullable<SuggestionsProps["render"]>
        > = {
          suggestions: [],
        };

        if (renderProps.rawData?.suggest?.suggestions.length) {
          const options =
            renderProps.rawData.suggest.suggestions[
              renderProps.rawData.suggest.suggestions.length - 1
            ]?.options;
          props.suggestions.push(
            ...options.map((option: { text: string; freq: number }) => ({
              text: option.text,
            }))
          );
        }

        return <RenderComponent {...props} />;
      }}
    />
  );
};
