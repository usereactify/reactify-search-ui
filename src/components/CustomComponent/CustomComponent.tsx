import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";

export type CustomComponentProps = React.ComponentProps<
  typeof ReactiveComponent
>;

export const CustomComponent: React.FC<CustomComponentProps> = (props) => {
  return <ReactiveComponent {...props} />;
};
