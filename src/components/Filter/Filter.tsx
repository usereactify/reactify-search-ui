import React from "react";
import ReactiveComponent from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";
import MultiList from "@appbaseio/reactivesearch/lib/components/list/MultiList";
import SingleList from "@appbaseio/reactivesearch/lib/components/list/SingleList";
import SingleRange from "@appbaseio/reactivesearch/lib/components/range/SingleRange";
import MultiRange from "@appbaseio/reactivesearch/lib/components/range/MultiRange";

import type {
  ConfigFilterOption,
  ReactivesearchFilterListProps,
} from "../../types";
import {
  useFilterListProps,
  useFilterRangeProps,
  useFilterSliderProps,
  useReactiveFilterListProps,
  useReactiveFilterRangeProps,
  useReactiveFilterSharedProps,
  useReactiveFilterSliderProps,
} from "../../hooks";
import {
  ExampleFilterList,
  ExampleFilterRange,
  ExampleFilterSlider,
} from "../../components";

export type FilterProps = {
  /** The filter option being rendered */
  filter: ConfigFilterOption;
  /** Render method for List filters */
  renderFilterList?: React.FC<{
    filter: ConfigFilterOption;
    filterListProps: ReturnType<typeof useFilterListProps>;
  }>;
  /** Render method for Range filters */
  renderFilterRange?: React.FC<{
    filter: ConfigFilterOption;
    filterRangeProps: ReturnType<typeof useFilterRangeProps>;
  }>;
  /** Render method for Slider filters */
  renderFilterSlider?: React.FC<{
    filter: ConfigFilterOption;
    filterSliderProps: ReturnType<typeof useFilterSliderProps>;
    // reactSliderProps: ReturnType<typeof useReactSliderProps>;
  }>;
};

export const Filter: React.FC<FilterProps> = (props) => {
  if ("single" === props.filter.displayType) {
    if ("range" === props.filter.displayView) {
      return <FilterRangeSingle {...props} />;
    }
    return <FilterSingleList {...props} />;
  }

  if ("multi" === props.filter.displayType) {
    if ("range" === props.filter.displayView) {
      return <FilterRangeMulti {...props} />;
    }
    return <FilterMultiList {...props} />;
  }

  if ("slider" === props.filter.displayType) {
    return <FilterSlider {...props} />;
  }

  // TODO: Legacy range filter, migrate to new single and multi ranges
  if ("range" === props.filter.displayType) {
    return <FilterRangeSingle {...props} />;
  }

  console.warn(
    `filter with display type "${props.filter.displayType}" not yet supported`
  );

  return null;
};

const FilterSingleList: React.FC<FilterProps> = (props) => {
  const reactiveFilterListProps = useReactiveFilterListProps(props.filter);

  return (
    <SingleList
      {...reactiveFilterListProps}
      render={(reactivesearchFilterProps: ReactivesearchFilterListProps) => (
        <FilterListInner
          {...props}
          reactivesearchFilterProps={reactivesearchFilterProps}
        />
      )}
    />
  );
};

const FilterMultiList: React.FC<FilterProps> = (props) => {
  const reactiveFilterListProps = useReactiveFilterListProps(props.filter);

  return (
    <MultiList
      {...reactiveFilterListProps}
      render={(reactivesearchFilterProps: ReactivesearchFilterListProps) => (
        <FilterListInner
          {...props}
          reactivesearchFilterProps={reactivesearchFilterProps}
        />
      )}
    />
  );
};

const FilterRangeSingle: React.FC<FilterProps> = (props) => {
  const reactiveFilterRangeProps = useReactiveFilterRangeProps(props.filter);

  const filterRangeProps = useFilterRangeProps(props.filter);

  const RenderRangeComponent = props.renderFilterRange ?? ExampleFilterRange;

  return (
    <>
      <div
        style={{
          display: "none",
        }}
      >
        <SingleRange
          {...reactiveFilterRangeProps}
          value={filterRangeProps.values[0]}
          onChange={filterRangeProps.handleChange}
        />
      </div>
      <RenderRangeComponent
        filterRangeProps={filterRangeProps}
        filter={props.filter}
      />
    </>
  );
};

const FilterRangeMulti: React.FC<FilterProps> = (props) => {
  const reactiveFilterRangeProps = useReactiveFilterRangeProps(props.filter);

  const filterRangeProps = useFilterRangeProps(props.filter);

  const RenderRangeComponent = props.renderFilterRange ?? ExampleFilterRange;

  return (
    <>
      <div
        style={{
          display: "none",
        }}
      >
        <MultiRange
          {...reactiveFilterRangeProps}
          value={filterRangeProps.values}
          // @note: onChange handler must be defined for controlled component to work correctly
          onChange={() => {}}
          onValueChange={(values) => {
            if (values.length === 0) {
              filterRangeProps.handleClear();
            }
          }}
        />
      </div>
      <RenderRangeComponent
        filterRangeProps={filterRangeProps}
        filter={props.filter}
      />
    </>
  );
};

const FilterSlider: React.FC<FilterProps> = (props) => {
  const reactiveFilterSharedProps = useReactiveFilterSharedProps(props.filter);

  return (
    <ReactiveComponent
      {...reactiveFilterSharedProps}
      defaultQuery={() => ({
        aggs: {
          min: {
            min: {
              field: props.filter.field,
            },
          },
          max: {
            max: {
              field: props.filter.field,
            },
          },
        },
      })}
      render={(reactivesearchFilterProps) => {
        if (
          reactivesearchFilterProps?.aggregations?.min?.value ===
          reactivesearchFilterProps?.aggregations?.max?.value
        ) {
          return null;
        }

        return (
          <FilterSliderInner
            {...props}
            reactivesearchFilterProps={reactivesearchFilterProps}
          />
        );
      }}
    />
  );
};

// inner component exists only so we can use memos in reactivesearch render functions
const FilterSliderInner: React.FC<
  FilterProps & { reactivesearchFilterProps: any }
> = (props) => {
  const {
    filter,
    renderFilterSlider,
    reactivesearchFilterProps,
    ...otherProps
  } = props;

  const filterSliderProps = useFilterSliderProps(filter);

  React.useEffect(() => {
    if (reactivesearchFilterProps.aggregations) {
      filterSliderProps.handleRange([
        reactivesearchFilterProps.aggregations.min.value,
        reactivesearchFilterProps.aggregations.max.value,
      ]);
    }
  }, [
    reactivesearchFilterProps.aggregations?.min.value,
    reactivesearchFilterProps.aggregations?.max.value,
  ]);

  React.useEffect(() => {
    if (filterSliderProps.value[0] === 0 && filterSliderProps.value[1] === 0) {
      return;
    }

    props.reactivesearchFilterProps.setQuery({
      query: {
        query: {
          bool: {
            must: [
              {
                bool: {
                  must: [
                    {
                      match: {
                        published: true,
                      },
                    },
                    {
                      range: {
                        [props.filter.field]: {
                          gte: filterSliderProps.value[0],
                          lte: filterSliderProps.value[1],
                          boost: 2,
                        },
                      },
                    },
                  ],
                },
              },
            ],
          },
        },
      },
      value: filterSliderProps.value,
    });
  }, [filterSliderProps.value]);

  if (renderFilterSlider) {
    const Component = renderFilterSlider;
    return (
      <Component
        filterSliderProps={filterSliderProps}
        filter={filter}
        {...otherProps}
      />
    );
  }

  return (
    <ExampleFilterSlider
      filterSliderProps={filterSliderProps}
      filter={filter}
      {...otherProps}
    />
  );
};

// inner component exists only so we can use memos in reactivesearch render functions
const FilterListInner: React.FC<
  FilterProps & { reactivesearchFilterProps: ReactivesearchFilterListProps }
> = (props) => {
  const { filter, renderFilterList, reactivesearchFilterProps, ...otherProps } =
    props;

  const filterListProps = useFilterListProps(filter, reactivesearchFilterProps);

  // hide entire filter when no options available
  if (0 === filterListProps.options.length) {
    return null;
  }

  if (renderFilterList) {
    const Component = renderFilterList;
    return (
      <Component
        filterListProps={filterListProps}
        filter={filter}
        {...otherProps}
      />
    );
  }

  return (
    <ExampleFilterList
      filterListProps={filterListProps}
      filter={filter}
      {...otherProps}
    />
  );
};
