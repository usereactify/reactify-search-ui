import React from "react";
import { Story, Meta } from "@storybook/react";

import { Filters } from "./Filters";

export default {
  title: "Filters",
  component: Filters,
} as Meta;

const Template: Story<React.ComponentProps<typeof Filters>> = (args) => (
  <Filters {...args} />
);

export const DefaultFilters = Template.bind({});
DefaultFilters.args = {};
