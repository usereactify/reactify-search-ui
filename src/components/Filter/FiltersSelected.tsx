import React from "react";
import SelectedFilters from "@appbaseio/reactivesearch/lib/components/basic/SelectedFilters";

import { ExampleFiltersSelected } from "../../components";
import { useFilters } from "../../hooks";

export type FiltersSelectedProps = {
  /** Render method called once for all active filters */
  render?: React.FC<{
    selectedFilters: Array<{
      key: string;
      label: string;
      value:
        | Array<string>
        | string
        | { label: string; start: number; end: number }
        | Array<{ label: string; start: number; end: number }>;
    }>;
    handleRemove: (
      filterKey: string,
      filterValues?: Array<string | number>
    ) => void;
  }>;
};

export const FiltersSelected: React.FC<FiltersSelectedProps> = (props) => {
  const filtersHook = useFilters();

  const RenderComponent = props.render ?? ExampleFiltersSelected;

  return (
    <SelectedFilters
      render={({ selectedValues, setValue }) => {
        const selectedFilters = (
          Object.entries(selectedValues) as Array<[string, any]>
        )
          .filter(
            ([key, item]) =>
              item?.URLParams &&
              item?.showFilter &&
              (!!item?.value?.length || !!item?.value?.label)
          )
          .map(([key, item]) => ({
            key: key,
            label: item.label,
            value: item.value,
          }));

        const handleRemove = (
          selectedFilterKey: string,
          selectedFilterValues: Array<string | number> = []
        ) => {
          const filter = filtersHook.filters?.find(
            (filter) => filter.handle === selectedFilterKey
          );
          if (!filter) {
            return;
          }

          if (filter.displayType === "slider") {
            const event = new CustomEvent<{
              handle: string;
              value: [];
            }>(`@usereactify/search:filter:${filter.handle}:update`, {
              detail: {
                handle: filter.handle,
                value: [],
              },
            });
            window.dispatchEvent(event);

            return;
          }

          if (selectedFilterValues.length === 0) {
            setValue(selectedFilterKey, null);

            return;
          }

          if (selectedFilterValues.length > 0) {
            if (filter.displayType === "single") {
              setValue(selectedFilterKey, null);
            }
            if (filter.displayType === "multi") {
              if (filter.displayView === "range") {
                const values = selectedValues[selectedFilterKey].value
                  .filter(
                    (value: { label: string }) =>
                      !selectedFilterValues.includes(value.label)
                  )
                  .map((value: { label: string }) => value.label);

                const event = new CustomEvent<{
                  handle: string;
                  value: Array<string>;
                }>(`@usereactify/search:filter:${filter.handle}:update`, {
                  detail: {
                    handle: filter.handle,
                    value: values,
                  },
                });
                window.dispatchEvent(event);
              }
              if (filter.displayView !== "range") {
                const values = selectedValues[selectedFilterKey].value.filter(
                  (value: string) => !selectedFilterValues.includes(value)
                );
                setValue(selectedFilterKey, values);
              }
            }
          }
        };

        if (!selectedFilters.length) {
          return null;
        }

        return (
          <RenderComponent
            selectedFilters={selectedFilters}
            handleRemove={handleRemove}
          />
        );
      }}
    />
  );
};
