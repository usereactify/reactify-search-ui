import React from "react";

import {
  useFilterListProps,
  useFilterRangeProps,
  useFilters,
  useFilterSliderProps,
} from "../../hooks";
import { Filter } from "../../components";
import { ConfigFilterOption } from "../../types";

export type FiltersProps = {
  /** Render method for List filters */
  renderFilterList?: React.FC<{
    filter: ConfigFilterOption;
    filterListProps: ReturnType<typeof useFilterListProps>;
  }>;
  /** Render method for Range filters */
  renderFilterRange?: React.FC<{
    filter: ConfigFilterOption;
    filterRangeProps: ReturnType<typeof useFilterRangeProps>;
  }>;
  /** Render method for Slider filters */
  renderFilterSlider?: React.FC<{
    filter: ConfigFilterOption;
    filterSliderProps: ReturnType<typeof useFilterSliderProps>;
  }>;
};

export const Filters: React.FC<FiltersProps> = (props) => {
  const filterStackHook = useFilters();

  return (
    <>
      {filterStackHook.filters?.map((filter) => (
        <Filter
          key={filter.id}
          filter={filter}
          renderFilterList={props.renderFilterList}
          renderFilterSlider={props.renderFilterSlider}
          renderFilterRange={props.renderFilterRange}
        />
      ))}
    </>
  );
};
