import React from "react";
import { Story, Meta } from "@storybook/react";

import { SortBy } from "./SortBy";

export default {
  title: "SortBy",
  component: SortBy,
} as Meta;

const Template: Story<React.ComponentProps<typeof SortBy>> = (args) => (
  <SortBy {...args} />
);

export const DefaultSortBy = Template.bind({});
DefaultSortBy.args = {};
