import React from "react";

import { useSortBy } from "../../hooks";
import { ExampleSortBy } from "../../components";

export type SortByProps = {
  /** Render method called once for all sort options */
  render?: React.FC<ReturnType<typeof useSortBy>>;
};

export const SortBy: React.FC<SortByProps> = (props) => {
  const sortByHook = useSortBy();

  const RenderComponent = props.render ?? ExampleSortBy;

  return <RenderComponent {...sortByHook} />;
};
