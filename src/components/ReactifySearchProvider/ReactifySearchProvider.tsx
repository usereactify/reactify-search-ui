import React from "react";
// @ts-expect-error missing types
import * as GaGtag from "ga-gtag";

import pkg from "../../../package.json";

import { ReactifySearchContext, useConfig } from "../../hooks";
import { UtilityAuthenticatedReactiveBase } from "../../components";
import { debug } from "../../utility";
import { ReactivesearchResultProps } from "../../types";

export type ReactifySearchProviderProps = {
  /** Standard react children */
  children?: React.ReactNode | undefined;
  /** The search area the provider will be used in */
  mode: "search" | "collection" | "instant-search";
  /** Shopify store domain used to resolve the site configuration */
  shopifyPermanentDomain: string;
  /** Callback function for redirects, suitable for headless sites to avoid full page refresh */
  onRedirect?: (type: "redirect" | "search", url: string) => void;
  /** Render method to display a component when the config is loading */
  renderBooting?: () => JSX.Element | null;
  /** Advanced usage: Override the default Reactify Search id (for telemetry) */
  clientId?: string;
  /** Advanced usage: Override the default Filters selection logic */
  filtersHandle?: string;
  /** Advanced usage: Array of additional component IDs managed outside of Reactify Search */
  additionalComponentIds?: Array<string>;
  /** Advanced usage: Override the default Elasticsearch index */
  index?: string;
  /** Advanced usage: Override the default Reactify Search config (for multi-instance stores) */
  configId?: string;
  /** Advanced usage: Override the default Elasticsearch credentials */
  credentials?: string;
  /** Advanced usage: Override the default Elasticsearch endpoint */
  endpoint?: string;
  /** Advanced usage: Override the default ReactiveBase theme */
  theme?: Record<string, unknown>;
  /** Advanced usage: Fields to include in the Elasticsearch response e.g. ["title"] */
  includeFields?: Array<string>;
  /** Advanced usage: Fields to exclude from the Elasticsearch response e.g. ["variant_skus", "*price*"] */
  excludeFields?: Array<string>;
  /** Advanced usage: Flags are used to enable or disable specific features within Reactify Search */
  flags?: Record<string, boolean>;
} & (
  | {
      mode: "search";
    }
  | {
      mode: "collection";
      /** Collection object that includes the handle, used to find curations */
      collectionHandle: string;
    }
  | {
      mode: "instant-search";
    }
);

export const ReactifySearchProvider: React.FC<ReactifySearchProviderProps> = (
  props
) => {
  React.useEffect(() => {
    debug.log("ReactifySearchProvider", "props", props);

    GaGtag.install("G-DV00Z0X5VP", {
      cookie_prefix: "_rs",
      user_properties: {
        shop: props.shopifyPermanentDomain,
        version: pkg.version,
      },
    });
  }, [props]);

  return <ReactifySearchInner {...props} />;
};

const ReactifySearchInner: React.FC<ReactifySearchProviderProps> = (props) => {
  const { config } = useConfig(props.shopifyPermanentDomain, props.configId);

  const theme = props.theme ?? {
    typography: {
      fontFamily: "inherit",
      fontSize: "inherit",
    },
  };

  const searchTermFromURL = React.useMemo(() => {
    if (typeof window === "undefined") {
      return;
    }
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("q") ?? undefined;
  }, [typeof window !== "undefined" && window.location.search]);

  const searchSortFromURL = React.useMemo(() => {
    if (typeof window === "undefined") {
      return;
    }
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("sort") ?? undefined;
  }, [typeof window !== "undefined" && window.location.search]);

  const indexFromURL = React.useMemo(() => {
    if (typeof window === "undefined") {
      return;
    }
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get("index") ?? undefined;
  }, [typeof window !== "undefined" && window.location.search]);

  const [searchTerm, setSearchTerm] = React.useState<string>(
    searchTermFromURL ?? ""
  );
  const [showInstantSearchResults, setShowInstantSearchResults] =
    React.useState<boolean>(false);
  const [sortOption, setSortOption] = React.useState<string>(
    searchSortFromURL ?? ""
  );

  const [results, setResults] = React.useState<
    ReactivesearchResultProps["data"]
  >([]);
  const [resultStats, setResultStats] =
    React.useState<ReactivesearchResultProps["resultStats"]>();

  React.useEffect(() => {
    if (searchTermFromURL && searchTerm !== searchTermFromURL) {
      setSearchTerm(searchTermFromURL);
    }
  }, [searchTermFromURL]);

  React.useEffect(() => {
    if (searchSortFromURL && sortOption !== searchSortFromURL) {
      setSortOption(searchSortFromURL);
    }
  }, [searchSortFromURL]);

  const contextValue = React.useMemo(() => {
    if (!config) {
      return;
    }

    return {
      config: config,
      options: {
        ...props,
        clientId: props.clientId ?? "theme",
        index: props.index ?? indexFromURL ?? config.index,
        filtersHandle: props.filtersHandle,
        credentials: props.credentials,
        theme: theme,
      },
      search: {
        searchTerm: searchTerm,
        setSearchTerm: setSearchTerm,
        showInstantSearchResults: showInstantSearchResults,
        setShowInstantSearchResults: setShowInstantSearchResults,
      },
      sortby: {
        sortOption: sortOption,
        setSortOption: setSortOption,
      },
      results: {
        results: results,
        setResults: setResults,
        resultStats: resultStats,
        setResultStats: setResultStats,
      },
    };
  }, [
    config,
    props,
    searchTerm,
    showInstantSearchResults,
    sortOption,
    results,
    resultStats,
  ]);

  debug.hook("ReactifySearchProvider", "context", contextValue);

  if (!contextValue) {
    if (props.renderBooting) {
      return props.renderBooting();
    }
    return null;
  }

  return (
    <ReactifySearchContext.Provider value={contextValue}>
      <UtilityAuthenticatedReactiveBase>
        {props.children}
      </UtilityAuthenticatedReactiveBase>
    </ReactifySearchContext.Provider>
  );
};
