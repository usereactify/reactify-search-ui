import React from "react";

import { useSearch } from "../../hooks";
import { ExampleSearch } from "../../components";

export type SearchProps = {
  /** Render method */
  render?: React.FC<ReturnType<typeof useSearch>>;
};

export const Search: React.FC<SearchProps> = (props) => {
  const searchHook = useSearch();

  const RenderComponent = props.render ?? ExampleSearch;

  return <RenderComponent {...searchHook} />;
};
