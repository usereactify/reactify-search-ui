import React from "react";
import { Story, Meta } from "@storybook/react";

import { Search } from "./Search";

export default {
  title: "Search",
  component: Search,
} as Meta;

const Template: Story<React.ComponentProps<typeof Search>> = (args) => (
  <Search {...args} />
);

export const DefaultSearch = Template.bind({});
DefaultSearch.args = {};
