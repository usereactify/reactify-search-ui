import React from "react";
import { useReactiveBaseProps } from "../../hooks";
import ReactiveBase from "@appbaseio/reactivesearch/lib/components/basic/ReactiveBase";

export type UtilityAuthenticatedReactiveBaseProps = {
  /** Standard react children */
  children?: React.ReactNode | undefined;
};

export const UtilityAuthenticatedReactiveBase: React.FC<UtilityAuthenticatedReactiveBaseProps> =
  (props) => {
    const reactiveBaseProps = useReactiveBaseProps();

    return <ReactiveBase {...reactiveBaseProps}>{props.children}</ReactiveBase>;
  };
