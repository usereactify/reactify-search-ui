import React from "react";
import { Story, Meta } from "@storybook/react";

import {
  ClearAll,
  Filters,
  FiltersSelected,
  Results,
  Sensors,
  SortBy,
  Search,
  Stats,
  Suggestions,
} from "./components";

export default {
  title: "KitchenSink",
  subcomponents: { Filters },
} as Meta;

const Template: Story = () => {
  return (
    <div style={{ display: "flex" }}>
      <Sensors />
      <div style={{ whiteSpace: "nowrap", maxWidth: "210px", padding: "8px" }}>
        <Suggestions field="title" />
        <ClearAll />
        <FiltersSelected />
        <Filters />
      </div>
      <div>
        <div style={{ display: "flex", padding: "8px" }}>
          <Search />
          <div style={{ flex: 1 }} />
          <SortBy />
        </div>
        <div style={{ padding: "8px" }}>
          <Stats />
        </div>
        <div style={{ padding: "8px" }}>
          <Results />
        </div>
      </div>
    </div>
  );
};

export const DefaultKitchenSink = Template.bind({});
DefaultKitchenSink.args = {};
