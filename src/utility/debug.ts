import Debug from "debug";

import { useDebugger } from "../hooks";

export const debuggers: Record<string, Debug.Debugger> = {};

export const debug = {
  namespace: "reactify-search",
  log: (namespace: string, formatter?: string, ...args: Array<any>) => {
    if (!debuggers[namespace]) {
      debuggers[namespace] = Debug(`${debug.namespace}:${namespace}`);
    }

    debuggers[namespace](formatter, ...args);
  },
  hook: useDebugger,
};
