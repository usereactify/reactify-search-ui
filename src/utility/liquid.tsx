import React from "react";

import {
  ReactifySearchProvider,
  ReactifySearchProviderProps,
} from "../components";

export class ReactifySearchLiquidFactory {
  constructor(
    public providerProps: Omit<
      ReactifySearchProviderProps,
      "shopifyPermanentDomain"
    > & {
      shopifyPermanentDomain?: string;
      collectionHandle?: string;
    }
  ) {}

  getProvider() {
    const shopifyPermanentDomain =
      this.providerProps.shopifyPermanentDomain ??
      (window as any)?.Shopify?.shop;

    if (!shopifyPermanentDomain) {
      throw new Error("Cannot determine shopifyPermanentDomain from window");
    }

    const collectionHandle =
      this.providerProps.collectionHandle ??
      window.location.pathname.split("/")?.[2];

    if (this.providerProps.mode === "collection" && !collectionHandle) {
      throw new Error("Cannot determine collectionHandle from URL");
    }

    const provider: React.FC<{
      children?: React.ReactNode | undefined;
    }> = (props) => (
      <ReactifySearchProvider
        {...this.providerProps}
        shopifyPermanentDomain={shopifyPermanentDomain}
        collectionHandle={collectionHandle}
      >
        {props.children}
      </ReactifySearchProvider>
    );

    return provider;
  }

  getMountNode() {
    const mountNode = document.getElementById(
      `reactify-mount-${this.providerProps.mode}`
    );
    if (!mountNode) {
      throw new Error(
        `Cannot find mount node with ID "reactify-mount-${this.providerProps.mode}"`
      );
    }

    return mountNode;
  }

  getData() {
    const dataNode = document.getElementById("reactify-data");
    if (!dataNode) {
      throw new Error(`Cannot find data node with ID "reactify-data"`);
    }

    try {
      const data = JSON.parse(dataNode.innerHTML);

      return data;
    } catch {
      throw new Error(
        `Cannot parse the data node with ID "reactify-data", check your liquid formatting`
      );
    }
  }
}
