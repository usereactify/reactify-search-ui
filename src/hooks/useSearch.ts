import React from "react";

import { useReactifySearchContext } from "../hooks";
import { debug } from "../utility";

export const useSearch = (): {
  /** The current search term */
  searchTerm: string;
  /** Function for changing the current search term */
  setSearchTerm: (searchTerm?: string) => void;
  /** Function for navigating to the search page, includes logic for redirects */
  submitSearchTerm: (searchTerm?: string) => void;
  /** Used to determine if instant search results should be displayed */
  showInstantSearchResults: boolean;
  /** Used to manually set the "showInstantSearchResults" value */
  setShowInstantSearchResults: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const { options, config, search } = useReactifySearchContext();

  const handleSubmitSearchTerm = React.useCallback(
    (searchTerm: string = search.searchTerm) => {
      debug.log("useSearch", "submitSearchTerm[query]", searchTerm);

      if (!searchTerm) {
        return;
      }

      const redirect = config.redirects.find(
        (redirect) =>
          redirect.query.trim().toLowerCase() ===
          searchTerm.trim().toLowerCase()
      );

      if (redirect) {
        debug.log("useSearch", "submitSearchTerm[redirect]", redirect);
      }

      search.setShowInstantSearchResults(false);

      if (options.onRedirect) {
        options.onRedirect(
          redirect ? "redirect" : "search",
          redirect
            ? redirect.url
            : `/search?q=${encodeURIComponent(searchTerm)}`
        );
      } else if (redirect) {
        window.location.href = redirect.url;
      } else {
        window.location.href = `/search?q=${encodeURIComponent(searchTerm)}`;
      }
    },
    [search.searchTerm]
  );

  const handleSetSearchTerm = React.useCallback(
    (searchTerm: string = "") => {
      debug.log("useSearch", "setSearchTerm[searchTerm]", searchTerm);

      search.setSearchTerm(searchTerm);
    },
    [search.searchTerm]
  );

  return {
    searchTerm: search.searchTerm,
    setSearchTerm: handleSetSearchTerm,
    submitSearchTerm: handleSubmitSearchTerm,
    showInstantSearchResults:
      !!search.showInstantSearchResults && !!search.searchTerm,
    setShowInstantSearchResults: search.setShowInstantSearchResults,
  };
};
