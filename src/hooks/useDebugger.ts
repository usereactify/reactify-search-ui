import React from "react";

import { debug } from "../utility";

export const useDebugger = (
  namespace: string,
  message: string,
  value: unknown
) => {
  React.useEffect(() => {
    debug.log(namespace, message, value);
  }, [value]);
};
