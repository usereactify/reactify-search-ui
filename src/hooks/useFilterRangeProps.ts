import React from "react";

import { ConfigFilterOption } from "../types/config";
import { useAnalytics } from "../hooks";
import { debug } from "../utility";

export const useFilterRangeProps = (
  filter: ConfigFilterOption
): {
  filter: ConfigFilterOption;
  options: Array<{
    key: string;
    label: string;
    checked: boolean;
  }>;
  values: string[];
  handleChange: (key: string) => void;
  handleClear: () => void;
} => {
  const { track } = useAnalytics();
  const [filterValues, setFilterValues] = React.useState<string[]>([]);

  const handleChange = React.useCallback(
    (key: string) => {
      debug.log("useFilterRangeMultiProps", "handleChange[key]", key);

      if (filter.displayType === "multi") {
        const selected = filterValues.find((item: string) => item === key)
          ? filterValues.filter((value) => value !== key)
          : [...filterValues, key];

        setFilterValues(selected);
      }
      if (filter.displayType === "single") {
        setFilterValues([key]);
      }

      track({
        eventName: "filterChange",
        payload: {
          name: filter.name,
          value: key,
        },
      });
    },
    [filter, filterValues]
  );

  const handleClear = React.useCallback(() => {
    setFilterValues([]);
  }, []);

  React.useEffect(() => {
    try {
      if (filter.displayView === "range") {
        const url = new URL(window.location.href);
        const value = JSON.parse(url.searchParams.get(filter.handle) ?? "[]");
        setFilterValues(value);
      }
    } catch (error) {
      console.error(error);
    }
  }, []);

  React.useEffect(() => {
    const handler = (
      event: CustomEvent<{ handle: string; value: Array<string> }>
    ) => {
      setFilterValues(event.detail.value);
    };
    window.addEventListener(
      `@usereactify/search:filter:${filter.handle}:update`,
      handler as EventListener
    );

    return () =>
      window.removeEventListener(
        `@usereactify/search:filter:${filter.handle}:update`,
        handler as EventListener
      );
  }, []);

  const filterRangeProps = React.useMemo(() => {
    const options =
      filter.displayRangeOptions?.map((option) => {
        const [key] = option.split(":");
        return {
          key: key,
          label: filter.settingsUppercase ? key.toUpperCase() : key,
          checked: filterValues.includes(key),
        };
      }) ?? [];

    return {
      handleChange,
      handleClear,
      filter,
      values: filterValues,
      options,
    };
  }, [filter, filterValues, handleChange]);

  return filterRangeProps;
};
