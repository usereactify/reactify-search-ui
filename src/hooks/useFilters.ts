import React from "react";
// @ts-expect-error missing types
import { ReactReduxContext } from "@appbaseio/reactivesearch/lib/utils";
// @ts-expect-error missing types
import { setValue } from "@appbaseio/reactivecore/lib/actions";

import { ConfigFilter, ConfigFilterOption } from "../types";
import { useReactifySearchContext } from "../hooks";
import { debug } from "../utility";

export const useFilters = (): {
  /** The currently selected filter stack, based on mode, curation and more */
  filterStack?: ConfigFilter;
  /** All of the available filters within the filter stack */
  filters?: Array<ConfigFilterOption>;
  /** All of the selected filters with their value */
  selected: Array<{
    handle: string;
    value: string | Array<string> | Array<{
      label: string;
      start: number;
      end: number;
    }>;
  }>;
  /** Set filter value by handle */
  setFilterValue: (
    handle: string,
    value: string | Array<string> | [number, number]
  ) => void;
} => {
  const { options, config } = useReactifySearchContext();
  const reactivesearchContext = React.useContext<any>(ReactReduxContext);

  const filterStack = React.useMemo(() => {
    // select filters by type
    const matchingFilterss = config.filters.filter(
      (filter) => filter.type === options.mode
    );

    // select filter stack by handle if provided
    if (options.filtersHandle) {
      const matchingFilters = matchingFilterss.find(
        (filterStack) => options.filtersHandle === filterStack.handle
      );
      if (matchingFilters) {
        return matchingFilters;
      }
    }

    // select filter stack by collection if provided
    if (options.mode === "collection" && options.collectionHandle) {
      const matchingFilters = matchingFilterss.find((filterStack) =>
        filterStack.collections?.includes(options.collectionHandle)
      );
      if (matchingFilters) {
        return matchingFilters;
      }
    }

    // select filter with "default" handle
    let matchingFilters = matchingFilterss.find(
      (filterStack) => filterStack.handle === "default"
    );
    if (matchingFilters) {
      return matchingFilters;
    }

    // select any filter with "default" handle
    matchingFilters = config.filters.find(
      (filterStack) => filterStack.handle === "default"
    );
    if (matchingFilters) {
      return matchingFilters;
    }

    // select any available filter as a last resort
    matchingFilters = config.filters[0];

    return matchingFilters;
  }, [
    config,
    options.mode === "collection" ? options.collectionHandle : undefined,
  ]);

  const filters = React.useMemo(() => {
    const sortedFilters = filterStack?.options.sort((a, b) =>
      a.position > b.position ? 1 : -1
    );

    return sortedFilters;
  }, [filterStack]);

  const selected = React.useMemo(() => {
    return Object.keys(reactivesearchContext.storeState.selectedValues)
      .filter((item) => filters.find((filter) => filter.handle === item))
      .map((item) => ({
        handle: item,
        value: reactivesearchContext.storeState.selectedValues[item].value,
      }));
  }, [filters, reactivesearchContext?.storeState?.selectedValues]);

  const setFilterValue = React.useCallback(
    (
      handle: string,
      value: string | Array<string> | [number, number]
    ): void => {
      debug.log("useFilters", "setFilterValue[handle,value]", handle, value);
      reactivesearchContext.store.dispatch(setValue(handle, value));
    },
    [reactivesearchContext]
  );

  debug.hook("useFilters", "filterStack", filterStack);
  debug.hook("useFilters", "filters", filters);
  debug.hook("useFilters", "selected", selected);

  return {
    filterStack: filterStack,
    filters: filters,
    selected: selected,
    setFilterValue: setFilterValue,
  };
};
