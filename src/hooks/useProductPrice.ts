import React from "react";
import currency from "currency.js";

import { ElasticProduct } from "../types/elastic";

export const useProductPrice = (product: ElasticProduct): {
  price?: currency;
  onSale: boolean;
  currencyCode: string;
  compareAtPrice?: currency;
  formattedPrice?: string;
  formattedCompareAtPrice?: string;
} => {
  return React.useMemo(() => {
    const variant = product.variants?.[0];

    if (!variant) {
      return {
        onSale: false,
        price: undefined,
        currencyCode: "AUD",
        compareAtPrice: undefined,
      };
    }

    // @todo select the appropriate currency
    const priceSet = variant.presentment_prices?.[0];

    if (!priceSet || !priceSet.price) {
      return {
        onSale: false,
        price: undefined,
        currencyCode: "AUD",
        compareAtPrice: undefined,
      };
    }

    const currencyCode = priceSet.price.currency_code ?? "AUD";

    const price = priceSet.price.amount
      ? currency(priceSet.price.amount)
      : undefined;

    const formattedPrice = price ? formatPrice(price, currencyCode) : undefined;

    const compareAtPrice = priceSet.compare_at_price?.amount
      ? currency(priceSet.compare_at_price.amount)
      : undefined;

    const formattedCompareAtPrice = compareAtPrice
      ? formatPrice(compareAtPrice, currencyCode)
      : undefined;

    const onSale =
      !!compareAtPrice && !!price && compareAtPrice.intValue > price.intValue;

    return {
      price,
      onSale,
      currencyCode,
      compareAtPrice,
      formattedPrice,
      formattedCompareAtPrice,
    };
  }, [product]);
};

const formatPrice = (price: currency, currencyCode: string): string => {
  let priceString = new Intl.NumberFormat("en-AU", {
    style: "currency",
    currency: currencyCode,
  }).format(price.value);

  if ("AUD" === currencyCode) {
    priceString = `AU ${priceString}`;
  }

  return priceString;
};
