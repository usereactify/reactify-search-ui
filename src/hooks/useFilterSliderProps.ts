import React from "react";
import { ConfigFilterOption } from "../types/config";
import { useAnalytics } from "../hooks";
import { debug } from "../utility";
import { useReactSliderProps } from "./react-slider/useReactSliderProps";

export const useFilterSliderProps = (
  filter: ConfigFilterOption
): {
  filter: ConfigFilterOption;
  value: [number, number];
  range: [number, number];
  handleChange: (value: [number | string, number | string]) => void;
  handleRange: (value: [number, number]) => void;
  reactSliderProps: ReturnType<typeof useReactSliderProps>;
} => {
  const { track } = useAnalytics();

  const urlSearchParam = new URLSearchParams(window.location.search);
  const [filterValue, setFilterValue] = React.useState<[number, number]>(
    JSON.parse(urlSearchParam.get(filter.handle) ?? "[0,0]")
  );
  const [filterRange, setFilterRange] = React.useState<[number, number]>([
    0, 0,
  ]);

  const handleChange = React.useCallback(
    (value: [number | string, number | string]) => {
      debug.log("useFilterSliderProps", "handleChange[value]", value);

      setFilterValue([parseInt(`${value[0]}`), parseInt(`${value[1]}`)]);

      track({
        eventName: "filterChange",
        payload: {
          name: filter.name,
          value: `${value[0]}:${value[1]}`,
        },
      });
    },
    [filter]
  );

  const handleRange = React.useCallback(
    (value: [number | string, number | string]) => {
      setFilterRange([parseInt(`${value[0]}`), parseInt(`${value[1]}`)]);
    },
    [filter]
  );

  React.useEffect(() => {
    const handler = (
      event: CustomEvent<{ handle: string; value: [number, number] }>
    ) => {
      setFilterValue(event.detail.value);
    };
    window.addEventListener(
      `@usereactify/search:filter:${filter.handle}:update`,
      handler as EventListener
    );

    return () =>
      window.removeEventListener(
        `@usereactify/search:filter:${filter.handle}:update`,
        handler as EventListener
      );
  }, []);

  const reactSliderProps = useReactSliderProps(
    filter,
    filterValue,
    filterRange,
    handleChange
  );

  const filterSliderProps = React.useMemo(() => {
    return {
      handleChange,
      handleRange,
      filter,
      value: filterValue,
      range: filterRange,
      reactSliderProps,
    };
  }, [
    handleChange,
    handleRange,
    filter,
    filterValue,
    filterRange,
    reactSliderProps,
  ]);

  return filterSliderProps;
};
