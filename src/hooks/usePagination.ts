import React from "react";

import { ReactivesearchPaginationProps } from "../types";
import { useAnalytics } from "../hooks";
import { debug } from "../utility";

export const usePagination = (
  input: ReactivesearchPaginationProps
): {
  /** The current page number, indexed from 0 */
  currentPage: number;
  /** The total amount of pages */
  totalPages: number;
  /** The total amount of pages to show within pagination components */
  pagesToShow: Array<number>;
  /** If there is a next page */
  hasNextPage: boolean;
  /** If there is a previous page */
  hasPreviousPage: boolean;
  /** The _actual_ current page number, indexed from 1 */
  actualCurrentPage: number;
  /** Function to build a URL for a specific page number, useful for Href links */
  buildPagePath: (page: number) => string;
  /** Function to change to the next page, if an event is provided the preventDefault() method will be called */
  handleNextPage: (event?: React.SyntheticEvent) => void;
  /** Function to change to the previous page, if an event is provided the preventDefault() method will be called */
  handlePreviousPage: (event?: React.SyntheticEvent) => void;
  /** Function to change to any page, if an event is provided the preventDefault() method will be called */
  handlePageChange: (pageNumber: number, event?: React.SyntheticEvent) => void;
} => {
  const { track } = useAnalytics();

  const buildPagePath = React.useCallback((page: number) => {
    const currentPath = window.location.pathname;

    return currentPath.includes("page=")
      ? currentPath.replace(/page=\d+/g, `page=${page}`)
      : `${currentPath}${currentPath.includes("?") ? "&" : "?"}page=${page}`;
  }, []);

  const pagesToShow = React.useMemo(
    () => getPages(input.pages, input.totalPages, input.currentPage),
    [input.pages, input.totalPages, input.currentPage]
  );

  // default input.currentPage is zero-indexed, actualCurrentPage is one-indexed
  const actualCurrentPage = React.useMemo(
    () => input.currentPage + 1,
    [input.currentPage]
  );

  const hasNextPage = React.useMemo(
    () => actualCurrentPage < input.totalPages,
    [actualCurrentPage, input.totalPages]
  );

  const hasPreviousPage = React.useMemo(
    () => actualCurrentPage > 1,
    [actualCurrentPage]
  );

  const hasPage = React.useCallback(
    (pageNumber: number) => {
      return pageNumber >= 0 && pageNumber < input.totalPages;
    },
    [input.totalPages]
  );

  const handlePageChange = React.useCallback<
    ReturnType<typeof usePagination>["handlePageChange"]
  >(
    (pageNumber, event) => {
      debug.log("usePagination", "handlePageChange[pageNumber]", pageNumber);
      event?.preventDefault();
      if (!hasPage(pageNumber)) {
        return;
      }
      input.setPage(pageNumber);
      track({
        eventName: "paginationChange",
        payload: {
          page: pageNumber,
          source: window.location.pathname.includes("/search")
            ? "search"
            : "collection",
        },
      });
    },
    [input.setPage, track]
  );

  const handleNextPage = React.useCallback<
    ReturnType<typeof usePagination>["handleNextPage"]
  >(
    (event) => {
      debug.log("usePagination", "handleNextPage", input.currentPage + 1);
      handlePageChange(input.currentPage + 1, event);
    },
    [hasNextPage, input.currentPage, handlePageChange]
  );

  const handlePreviousPage = React.useCallback<
    ReturnType<typeof usePagination>["handlePreviousPage"]
  >(
    (event) => {
      debug.log("usePagination", "handlePreviousPage", input.currentPage - 1);
      handlePageChange(input.currentPage - 1, event);
    },
    [hasPreviousPage, input.currentPage, handlePageChange]
  );

  return React.useMemo(
    () => ({
      currentPage: input.currentPage,
      totalPages: input.totalPages,
      pagesToShow,
      hasNextPage,
      buildPagePath,
      hasPreviousPage,
      actualCurrentPage,
      handleNextPage,
      handlePreviousPage,
      handlePageChange,
    }),
    [
      input.currentPage,
      input.totalPages,
      pagesToShow,
      hasNextPage,
      buildPagePath,
      hasPreviousPage,
      actualCurrentPage,
      handleNextPage,
      handlePreviousPage,
      handlePageChange,
    ]
  );
};

// get a list of pages to display buttons for
// @todo this is super hacky and needs to be cleaned up
const getPages = (pages: number, totalPages: number, currentPage: number) => {
  const pagesToShow = pages > totalPages ? totalPages : pages;
  const buffer = Math.floor(pagesToShow / 2);
  let list: number[] = [];
  let start = 0;
  if (totalPages <= 5 || currentPage < buffer + 1) {
    start = 0;
  } else {
    if (totalPages - buffer <= currentPage) {
      start =
        currentPage + 1 === totalPages
          ? currentPage - (pagesToShow - 1)
          : currentPage - (buffer + buffer / 2);
    } else {
      start = currentPage - buffer;
    }
  }
  Array.apply(null, Array(pagesToShow)).map((item, index) => {
    list.push(start + index);
  });
  return list;
};
