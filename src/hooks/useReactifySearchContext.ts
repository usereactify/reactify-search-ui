import React from "react";

import { Config, ReactivesearchResultProps } from "../types";
import { Flags } from "./useFlags";

export type ReactifySearchMode = "search" | "collection" | "instant-search";

export const ReactifySearchContext = React.createContext<
  | {
      config: Config;
      options: {
        clientId: string;
        mode: ReactifySearchMode;
        index: string;
        shopifyPermanentDomain: string;
        filtersHandle?: string;
        credentials?: string;
        onRedirect?: (type: "redirect" | "search", url: string) => void;
        theme: Record<string, unknown>;
        additionalComponentIds?: Array<string>;
        includeFields?: Array<string>;
        excludeFields?: Array<string>;
        flags?: Record<Flags, boolean>;
      } & (
        | {
            mode: "search";
          }
        | {
            mode: "collection";
            collectionHandle: string;
          }
        | {
            mode: "instant-search";
          }
      );
      search: {
        searchTerm: string;
        setSearchTerm: React.Dispatch<React.SetStateAction<string>>;
        showInstantSearchResults: boolean;
        setShowInstantSearchResults: React.Dispatch<
          React.SetStateAction<boolean>
        >;
      };
      sortby: {
        sortOption: string;
        setSortOption: React.Dispatch<React.SetStateAction<string>>;
      };
      results: {
        results: ReactivesearchResultProps["data"];
        setResults: React.Dispatch<
          React.SetStateAction<ReactivesearchResultProps["data"]>
        >;
        resultStats?: ReactivesearchResultProps["resultStats"];
        setResultStats: React.Dispatch<
          React.SetStateAction<
            ReactivesearchResultProps["resultStats"] | undefined
          >
        >;
      };
    }
  | undefined
>(undefined);

export const useReactifySearchContext = () =>
  React.useContext(ReactifySearchContext)!;
