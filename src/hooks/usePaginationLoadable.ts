import React from "react";

import { ReactivesearchResultProps } from "../types";
import { useAnalytics, useFilters } from ".";
import { debug } from "../utility";

export const usePaginationLoadable = (
  input: ReactivesearchResultProps
): {
  loading: boolean;
  hasMore: boolean;
  handleLoadMore: () => void;
  resultStats: {
    time: number;
    hidden: number;
    promoted: number;
    currentPage: number;
    numberOfPages: number;
    numberOfResults?: number;
    displayedResults: number;
  };
} => {
  const analyticsHook = useAnalytics();
  const { filterStack } = useFilters();

  const hasMore =
    input.resultStats.displayedResults !== input.resultStats.numberOfResults;

  const handleLoadMore = React.useCallback(() => {
    debug.log("usePaginationLoadable", "handleLoadMore");

    if (!hasMore) {
      return;
    }

    input.loadMore();
    analyticsHook.track({
      eventName: "paginationChange",
      payload: {
        page:
          Math.ceil(
            input.resultStats.displayedResults / (filterStack?.pageSize || 20)
          ) + 1,
        source: window.location.pathname.includes("/search")
          ? "search"
          : "collection",
      },
    });
  }, [input.loadMore, analyticsHook.track, input.resultStats, filterStack]);

  return React.useMemo(
    () => ({
      loading: input.loading,
      hasMore,
      handleLoadMore,
      resultStats: input.resultStats,
    }),
    [input.loading, hasMore, handleLoadMore, input.resultStats]
  );
};
