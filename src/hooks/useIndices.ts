import React from "react";
import { useReactifySearchContext } from "./useReactifySearchContext";

export type UseIndices = {
  indices: Array<string>;
  indexNameFormat: (indexName: string) => string;
};

export const useIndices = (): UseIndices => {
  const { config, options } = useReactifySearchContext();
  const shopName = options.shopifyPermanentDomain.replace(".myshopify.com", "");

  const [indices, setIndices] = React.useState<UseIndices["indices"]>([]);

  const indexNameFormat: UseIndices["indexNameFormat"] = (indexName) => {
    let indexNameFormatted = indexName;
    try {
      const indexDate = indexName.split(`${shopName}-`)[1];

      indexNameFormatted = `${shopName} ${new Date(
        parseInt(indexDate)
      ).toLocaleString()} (${indexDate})`;
    } catch (error) {
      console.error(error);
    }

    return indexNameFormatted;
  };

  React.useEffect(() => {
    (async () => {
      const response: Array<{ index: string }> = await fetch(
        `${config.endpoint}/_cat/indices?format=json`,
        {
          headers: {
            "x-reactify-shop": options.shopifyPermanentDomain,
            "x-reactify-skip-cache": "true",
          },
        }
      ).then((response) => response.json());

      const regexp = new RegExp(`${shopName}-[0-9]`);

      setIndices(
        response
          .map((item) => item.index)
          .filter((item) => regexp.test(item))
          .sort((a, b) => (a > b ? -1 : 1))
      );
    })();
  }, [config.endpoint]);

  return {
    indices: indices,
    indexNameFormat: indexNameFormat,
  };
};
