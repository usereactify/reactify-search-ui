import React from "react";

import { ReactivesearchResultProps } from "../types";
import { debug } from "../utility";

/** This hook must never be used to render results */
export const useResults = (): {
  /** Equals true until first load has completed */
  loading: boolean;
  /** All of the current results */
  results: ReactivesearchResultProps["data"];
  /** Function for setting the current results */
  setResults: (value: ReactivesearchResultProps["data"]) => void;
  /** All of the current result stats */
  resultStats?: ReactivesearchResultProps["resultStats"];
  /** Function for setting the current result stats */
  setResultStats: (value: ReactivesearchResultProps["resultStats"]) => void;
} => {
  const [results, setResults] = React.useState<
    ReactivesearchResultProps["data"]
  >([]);
  const [resultStats, setResultStats] =
    React.useState<ReactivesearchResultProps["resultStats"]>();

  const loading = React.useMemo(() => {
    return typeof resultStats?.numberOfResults === "undefined";
  }, [resultStats?.numberOfResults]);

  React.useEffect(() => {
    const handler = (
      event: CustomEvent<typeof results>
    ) => {
      setResults(event.detail);
    };
    window.addEventListener(
      `@usereactify/search:results:update`,
      handler as EventListener
    );

    return () =>
      window.removeEventListener(
        `@usereactify/search:results:update`,
        handler as EventListener
      );
  }, []);

  React.useEffect(() => {
    const handler = (
      event: CustomEvent<typeof resultStats>
    ) => {
      setResultStats(event.detail);
    };
    window.addEventListener(
      `@usereactify/search:resultStats:update`,
      handler as EventListener
    );

    return () =>
      window.removeEventListener(
        `@usereactify/search:resultStats:update`,
        handler as EventListener
      );
  }, []);

  const handleSetResults = (value: typeof results) => {
    const event = new CustomEvent<typeof results>(`@usereactify/search:results:update`, {
      detail: value
    });
    window.dispatchEvent(event);
  }

  const handleSetResultStats = (value: typeof resultStats) => {
    const event = new CustomEvent<typeof resultStats>(`@usereactify/search:resultStats:update`, {
      detail: value
    });
    window.dispatchEvent(event);
  }

  debug.hook("useResults", "results", results);
  debug.hook("useResults", "resultStats", resultStats);

  return {
    loading: loading,
    results: results,
    setResults: handleSetResults,
    resultStats: resultStats,
    setResultStats: handleSetResultStats,
  };
};
