import React from "react";

import { ConfigFilterOption } from "../types/config";
import { ReactivesearchFilterListProps } from "../types/reactivesearch";
import { useAnalytics } from "../hooks";
import { debug } from "../utility";

const DEFAULT_NUMERIC_SORT = ["0000", "000", "00", "0"];
const DEFAULT_ALPHABETICAL_SORT = [
  "SINGLE",
  "DOUBLE",
  "QUEEN",
  "KING",
  "SUPER KING",
  "XXXS",
  "XXS",
  "XS",
  "XS/S",
  "S",
  "SM",
  "S/M",
  "M",
  "ML",
  "M/L",
  "L",
  "LXL",
  "L/XL",
  "XL",
  "2L",
  "XXL",
  "3L",
  "XXXL",
  "4L",
  "5L",
  "6L",
  "OS",
  "ONESIZE",
];

// converts reactivesearch props returned in filters to a cleaned up/filtered version
export const useFilterListProps = (
  filter: ConfigFilterOption,
  reactivesearchFilterProps: ReactivesearchFilterListProps
): ReactivesearchFilterListProps & {
  filter: ConfigFilterOption;
  options: Array<{
    doc_count: number;
    key: string;
    label: string;
    checked: boolean;
  }>;
  hasSelected: boolean;
  totalSelected: number;
} => {
  const { track } = useAnalytics();

  const handleChange = React.useCallback(
    (key: string) => {
      debug.log("useFilterListProps", "handleChange[key]", key);

      reactivesearchFilterProps.handleChange(key);
      track({
        eventName: "filterChange",
        payload: {
          name: filter.name,
          value: key,
        },
      });
    },
    [filter]
  );

  const customSortOrder = filter.customSortOrder
    ?.split(",")
    .map((item) => item.trim().toUpperCase());

  const filterListProps = React.useMemo(() => {
    // remove excluded options, sort alphabeticaly, map checked boolean
    const options = reactivesearchFilterProps.data
      .filter(({ key }) => {
        if (filter.valuesExclude?.includes(key)) return false;
        if (!!filter.valuesManual?.length && !filter.valuesManual.includes(key))
          return false;
        return true;
      })
      .sort(
        (a, b) =>
          DEFAULT_NUMERIC_SORT.indexOf(a.key) -
          DEFAULT_NUMERIC_SORT.indexOf(b.key)
      )
      .sort((a, b) =>
        new Intl.Collator("en", { numeric: true, sensitivity: "base" }).compare(
          a.key,
          b.key
        )
      )
      .sort((a, b) => {
        if (!customSortOrder || customSortOrder.length === 0) {
          return (
            DEFAULT_ALPHABETICAL_SORT.indexOf(a.key.toUpperCase()) -
            DEFAULT_ALPHABETICAL_SORT.indexOf(b.key.toUpperCase())
          );
        }

        return (
          customSortOrder?.indexOf(a.key.toUpperCase()) -
          customSortOrder?.indexOf(b.key.toUpperCase())
        );
      })
      .map((option) => ({
        ...option,
        label: filter.settingsUppercase ? option.key.toUpperCase() : option.key,
        checked:
          (typeof reactivesearchFilterProps.value === "object" &&
            reactivesearchFilterProps.value[option.key]) ||
          reactivesearchFilterProps.value === option.key,
      }));

    // work out if at least one option selected
    const hasSelected =
      (typeof reactivesearchFilterProps.value === "object" &&
        0 < Object.keys(reactivesearchFilterProps.value).length) ||
      (typeof reactivesearchFilterProps.value !== "object" &&
        !!reactivesearchFilterProps.value);

    const totalSelected = options.filter((option) => option.checked).length;

    return {
      ...reactivesearchFilterProps,
      handleChange,
      filter,
      options,
      hasSelected,
      totalSelected,
    };
  }, [filter, handleChange, reactivesearchFilterProps]);

  return filterListProps;
};
