import React from "react";

import { ConfigFilterOption } from "../types/config";

export const useFilterCollapsedState = (filter: ConfigFilterOption): {
  collapsed: boolean;
  shouldRender: boolean;
  collapsedShow: () => void;
  collapsedHide: () => void;
  collapsedToggle: () => void;
} => {
  // @todo use settingsCollapsedDesktop and settingsCollapsedMobile with device detection
  const [collapsed, setCollapsed] = React.useState<boolean>(
    filter.settingsCollapsedDesktop
  );

  // when the filter is shown, it needs to stay rendered or when it's collapsed, the filter context
  // will be lost which isn't expected behaviour
  const [shouldRender, setShouldRender] = React.useState<boolean>(!collapsed);

  // enable render when filter is uncollapsed
  React.useEffect(() => {
    if (!collapsed) setShouldRender(true);
  }, [collapsed]);

  return {
    collapsed,
    shouldRender,
    collapsedShow: React.useCallback(() => setCollapsed(true), []),
    collapsedHide: React.useCallback(() => setCollapsed(false), []),
    collapsedToggle: React.useCallback(
      () => setCollapsed((collapsed) => !collapsed),
      []
    ),
  };
};
