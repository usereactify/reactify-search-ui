import React from "react";
import { ConfigFilterOption } from "../../types";
import ReactSlider from "react-slider";

export const useReactSliderProps = (
  filter: ConfigFilterOption,
  filterValue: [number, number],
  filterRange: [number, number],
  handleChange: (value: [number, number]) => void
): Pick<
  React.ComponentProps<typeof ReactSlider>,
  | "value"
  | "defaultValue"
  | "min"
  | "max"
  | "step"
  | "pearling"
  | "minDistance"
  | "onChange"
> => {
  const reactSliderProps = React.useMemo<
    ReturnType<typeof useReactSliderProps>
  >(() => {
    return {
      value: [
        filterValue[0] ? filterValue[0] : filterRange[0],
        filterValue[1] ? filterValue[1] : filterRange[1],
      ],
      defaultValue: [filterRange[0], filterRange[1]],
      min: filterRange[0],
      max: filterRange[1],
      step: parseInt(filter.displaySliderStep ?? "5"),
      pearling: true,
      minDistance: 0,
      onChange: (value) =>
        Array.isArray(value) ? handleChange([value[0], value[1]]) : {},
    };
  }, [filter.displaySliderStep, filterValue, filterRange, handleChange]);

  return reactSliderProps;
};
