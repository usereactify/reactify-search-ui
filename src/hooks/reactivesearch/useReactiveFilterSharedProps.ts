import React from "react";

import type { ConfigFilterOption } from "../../types/config";

import { useReactiveReactProp } from "./useReactiveReactProp";

/**
 * For use with all @appbaseio/reactivesearch filter components
 */
export const useReactiveFilterSharedProps = (filter: ConfigFilterOption) => {
  const reactiveReactProp = useReactiveReactProp(filter.handle, filter);

  const nestedField = React.useMemo(() => {
    const nestedFields = ["variants"];
    const topField = filter.field.split(".")[0];
    if (nestedFields.includes(topField)) return topField;
    return undefined;
  }, [filter]);

  // define a custom query for when settingsHideUnavailable is enabled and the target field is inside variants
  // customQuery defines how this filter affects *other* components like the results component
  const customQuery = React.useMemo(() => {
    if (
      !filter.settingsHideUnavailable ||
      !filter.field.startsWith("variants.")
    )
      return undefined;

    return (value?: string | string[]) => {
      if (!value) return {};

      // reactivesearch sometimes returns string, sometimes string[]
      const valueArray = "string" === typeof value ? [value] : value;

      if (0 === valueArray.length) return {};

      return {
        query: {
          nested: {
            path: "variants",
            query: {
              bool: {
                must: [
                  {
                    match: {
                      "variants.available": "true",
                    },
                  },
                  {
                    terms: {
                      [filter.field]: valueArray,
                    },
                  },
                ],
              },
            },
          },
        },
      };
    };
  }, [filter]);

  // define a default query for when settingsHideUnavailable is enabled and the target field is inside variants
  // defaultQuery defines how this filter affects itself i.e. the options displayed
  const defaultQuery = React.useMemo(() => {
    if (!filter.field.startsWith("variants.")) {
      return undefined;
    }

    // override the aggregation query to filter the results by the variants.available attribute
    return () => {
      return {
        aggs: {
          reactivesearch_nested: {
            nested: { path: "variants" },
            aggs: {
              reactify_nested_outer: {
                filter: {
                  bool: {
                    minimum_should_match: 1,
                    should: [
                      {
                        match: {
                          "variants.available": "true",
                        },
                      },
                      {
                        match: {
                          "variants.available": filter.settingsHideUnavailable
                            ? "true"
                            : "false",
                        },
                      },
                    ],
                  },
                },
                aggs: {
                  [filter.field]: {
                    terms: {
                      field: filter.field,
                      size: 100,
                      order: { _count: "desc" },
                    },
                    aggs: {
                      parent_docs: { reverse_nested: {} },
                    },
                  },
                },
              },
            },
          },
        },
      };
    };
  }, [filter]);

  const reactiveFilterSharedProps = React.useMemo(
    () => ({
      nestedField,
      customQuery,
      defaultQuery,
      URLParams: true,
      componentId: filter.handle,
      dataField: filter.field,
      filterLabel: filter.name,
      react: reactiveReactProp,
    }),
    [filter, reactiveReactProp]
  );

  return reactiveFilterSharedProps;
};
