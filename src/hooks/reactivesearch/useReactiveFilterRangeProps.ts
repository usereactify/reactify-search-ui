import React from "react";

import { ConfigFilterOption } from "../../types";

import { useReactiveFilterSharedProps } from "./useReactiveFilterSharedProps";

/**
 * For use with @appbaseio/reactivesearch SingleRange and MultiRange components
 */
export const useReactiveFilterRangeProps = (filter: ConfigFilterOption) => {
  const reactiveFilterSharedProps = useReactiveFilterSharedProps(filter);

  const data = React.useMemo(() => {
    return (
      filter.displayRangeOptions?.map((option) => {
        const [label, start, end] = option.split(":");
        return {
          label: label,
          start: start ? parseInt(start) : 0,
          end: end ? parseInt(end) : Number.MAX_SAFE_INTEGER,
        };
      }) ?? []
    );
  }, []);

  const reactiveFilterRangeProps = React.useMemo(
    () => ({
      ...reactiveFilterSharedProps,
      queryFormat: filter.settingsFilterLogic ?? "or",
      showFilter: filter.settingsShowFilter ?? true,
      showRadio: false,
      showCheckbox: false,
      data: data,
    }),
    [filter, reactiveFilterSharedProps]
  );

  return reactiveFilterRangeProps;
};
