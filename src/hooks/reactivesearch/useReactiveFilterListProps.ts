import React from "react";

import type { ConfigFilterOption } from "../../types/config";
import { useReactiveFilterSharedProps } from "./useReactiveFilterSharedProps";

/**
 * For use with @appbaseio/reactivesearch SingleList and MultiList components
 */
export const useReactiveFilterListProps = (filter: ConfigFilterOption) => {
  const reactiveFilterSharedProps = useReactiveFilterSharedProps(filter);

  const reactiveFilterListProps = React.useMemo(
    () => ({
      ...reactiveFilterSharedProps,
      size: parseInt(filter.displaySize) || undefined,
      queryFormat: filter.settingsFilterLogic ?? "or",
      showFilter: filter.settingsShowFilter ?? true,
      showLoadMore: filter.settingsShowMore ?? false,
      showSearch: filter.settingsShowSearch ?? false,
      showRadio: false,
      showCheckbox: false,
    }),
    [filter, reactiveFilterSharedProps]
  );

  return reactiveFilterListProps;
};
