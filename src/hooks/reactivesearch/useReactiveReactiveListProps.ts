import React from "react";
import ReactiveList from "@appbaseio/reactivesearch/lib/components/result/ReactiveList";

import { useFilters, useReactifySearchContext } from "../../hooks";
import { useReactiveReactProp } from "./useReactiveReactProp";

export const useReactiveReactiveListProps = (options: {
  pageSize?: number;
  scrollTarget?: React.ComponentProps<typeof ReactiveList>["scrollTarget"];
}): React.ComponentProps<typeof ReactiveList> => {
  const { filterStack } = useFilters();
  const reactiveReactProp = useReactiveReactProp();
  const context = useReactifySearchContext();
  const { includeFields, excludeFields } = context.options;

  const size = React.useMemo(() => {
    return options.pageSize ?? filterStack?.pageSize ?? 20;
  }, [options.pageSize, filterStack]);

  const reactiveReactiveListProps = React.useMemo<
    React.ComponentProps<typeof ReactiveList>
  >(
    () => ({
      size,
      URLParams: true,
      showLoader: false,
      dataField: "title",
      showResultStats: false,
      react: reactiveReactProp,
      includeFields,
      excludeFields,
      scrollTarget: options.scrollTarget,
      componentId: "page", // this sets ?page= in the URL
      scrollOnChange: false, // @todo make this better, it's really janky when enabled
      pagination: filterStack?.paginationType !== "infinite_scroll",
      infiniteScroll: filterStack?.paginationType === "infinite_scroll",
      renderNoResults: () => null, // always use only render, otherwise both are shown
    }),
    [filterStack, reactiveReactProp, options.pageSize]
  );

  return reactiveReactiveListProps;
};
