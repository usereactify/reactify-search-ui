import React from "react";

import type { ConfigFilterOption } from "../../types/config";
import { useReactiveFilterSharedProps } from "./useReactiveFilterSharedProps";

/**
 * For use with @appbaseio/reactivesearch RangeSlider component
 */
export const useReactiveFilterSliderProps = (filter: ConfigFilterOption) => {
  const reactiveFilterSharedProps = useReactiveFilterSharedProps(filter);

  const rangeLabelsFn = React.useCallback(
    (min: number, max: number) => ({
      start: `${filter.displaySliderPrefix ?? ""}${min}${
        filter.displaySliderSuffix ?? ""
      }`,
      end: `${filter.displaySliderPrefix ?? ""}${max}${
        filter.displaySliderSuffix ?? ""
      }`,
    }),
    []
  );

  const reactiveFilterSliderProps = React.useMemo(
    () => ({
      ...reactiveFilterSharedProps,
      showFilter: filter.settingsShowFilter ?? true,
      rangeLabels: rangeLabelsFn,
      stepValue: parseInt(filter.displaySliderStep) ?? undefined,
    }),
    [filter, reactiveFilterSharedProps]
  );

  return reactiveFilterSliderProps;
};
