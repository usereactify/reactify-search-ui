import React from "react";
import { ReactiveBaseProps } from "@appbaseio/reactivesearch/lib/components/basic/ReactiveBase";

import { useReactifySearchContext } from "../../hooks";

import pkg from "../../../package.json";

const replaceDocCountWithParent = (response: any) => {
  Object.entries(response)
    .filter(([key]) => key !== "doc_count")
    .forEach(([key, value]: [string, any]) => {
      value.buckets?.forEach((bucket: any) => {
        if (bucket.parent_docs) {
          bucket.doc_count = bucket.parent_docs.doc_count;
          delete bucket.parent_docs;
        }
      });
    });

  return response;
};

export const useReactiveBaseProps = (): ReactiveBaseProps => {
  const { config, options } = useReactifySearchContext();

  const reactiveBaseProps = React.useMemo<ReactiveBaseProps>(
    () => ({
      app: options.index,
      url: config.endpoint,
      credentials: options.credentials,
      theme: options.theme,
      transformResponse: async (response: any) => {
        // support filtering inside nested aggregations by moving the inner aggregation
        // up one level such that reactivesearch understands it
        //
        // see useReactiveFilterListProps > defaultQuery to see how a filter is being applied
        // to variant aggregations to ensure that only in stock sizes are displayed as filter
        // options, this involves a nested filter, which requires this fix
        //
        // https://github.com/appbaseio/reactivesearch/issues/1530
        if (
          response?.aggregations?.reactivesearch_nested?.reactify_nested_outer
        ) {
          response.aggregations.reactivesearch_nested =
            replaceDocCountWithParent(
              response?.aggregations?.reactivesearch_nested
                ?.reactify_nested_outer
            );
        }

        return response;
      },
      headers: {
        "x-reactify-shop": options.shopifyPermanentDomain,
        "x-reactify-mode": options.mode,
        "x-reactify-client-id": options.clientId,
        "x-reactify-client-version": pkg.version,
      },
    }),
    [options.index, config.endpoint, options.credentials, options.theme]
  );

  return reactiveBaseProps;
};
