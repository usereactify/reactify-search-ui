import React from "react";

import { useReactifySearchContext, useFilters } from "../../hooks";
import { SENSOR_IDS } from "../../components";
import { ConfigFilterOption } from "../../types";

export const useReactiveReactProp = (
  currentHandle?: string,
  filter?: ConfigFilterOption
) => {
  const { options } = useReactifySearchContext();
  const { filterStack } = useFilters();

  const reactProp = React.useMemo(() => {
    if (filter && filter.settingsShowEmptyValues) {
      return {
        and: SENSOR_IDS,
      };
    }

    return {
      and: [
        ...SENSOR_IDS,
        ...(options.additionalComponentIds ?? []),
        ...(filterStack?.options.map((filter) => filter.handle) ?? []),
      ].filter((handle) => handle !== currentHandle),
    };
  }, [filterStack, currentHandle, options.additionalComponentIds]);

  return reactProp;
};
