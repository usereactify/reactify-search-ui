import React from "react";

import { ConfigSort } from "../types";
import { useAnalytics, useReactifySearchContext } from "../hooks";
import { debug } from "../utility";

export const useSortBy = (): {
  /** The currently selected sort option */
  sortOption?: ConfigSort;
  /** All of the available sort options */
  sortOptions: Array<ConfigSort>;
  /** Function for changing the current sort option */
  setSortOption: (
    sortOptionHandle: string,
    ignoreHistoryState?: boolean
  ) => void;
} => {
  const { options, config, sortby } = useReactifySearchContext();
  const { track } = useAnalytics();

  const sortOptions = React.useMemo(() => {
    return config.sort
      .sort((a, b) => {
        const aInt =
          typeof a.position === "string"
            ? parseInt(a.position, 10)
            : a.position;
        const bInt =
          typeof b.position === "string"
            ? parseInt(b.position, 10)
            : b.position;
        return aInt - bInt;
      })
      .filter(({ visibility }) => ["all", options.mode].includes(visibility));
  }, [options.mode, config]);

  const sortOption = React.useMemo(() => {
    return (
      sortOptions.find(({ handle }) => handle === sortby.sortOption) ||
      sortOptions[0]
    );
  }, [sortOptions, sortby.sortOption]);

  const setSortOption = React.useCallback(
    (sortOptionHandle: string, ignoreHistoryState = false) => {
      debug.log(
        "useSortBy",
        "setSortOption[sortOptionHandle]",
        sortOptionHandle
      );

      sortby.setSortOption(sortOptionHandle);

      if (!ignoreHistoryState) {
        const url = new URL(window.location.href);
        if (url.searchParams.has("sort")) {
          url.searchParams.set("sort", sortOptionHandle);
        } else {
          url.searchParams.append("sort", sortOptionHandle);
        }
        window.history.pushState({}, "", url.toString());
      }

      track({
        eventName: "sortChange",
        payload: {
          type: sortOptionHandle,
        },
      });
    },
    [window.location.href]
  );

  debug.hook("useSortBy", "sortOption", sortOption);
  debug.hook("useSortBy", "sortOptions", sortOptions);

  return {
    sortOptions,
    sortOption,
    setSortOption,
  };
};
