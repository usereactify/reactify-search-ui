import React from "react";

import { ConfigCuration } from "../types";
import { useReactifySearchContext, useSearch } from "../hooks";
import { debug } from "../utility";

export const useCuration = (): {
  curation?: ConfigCuration;
} => {
  const { options, config } = useReactifySearchContext();
  const { searchTerm } = useSearch();

  const curation = React.useMemo(() => {
    const handleOrSearchTerm =
      options.mode === "collection" ? options.collectionHandle : searchTerm;

    const normalisedHandleOrSearchTerm = handleOrSearchTerm
      .toLowerCase()
      .trim();

    const globalCuration = config.curations.find((curation) => {
      if (options.mode === "search") {
        return curation.id === "global_search";
      }
      if (options.mode === "instant-search") {
        return curation.id === "global_search";
      }
      if (options.mode === "collection") {
        return curation.id === "global_collection";
      }
      return false;
    });

    const curation = config.curations.find((curation) => {
      const normalisedSearchTerm = curation.searchTerm?.toLowerCase().trim();
      const normalisedCollectionHandle = curation.collectionHandle
        ?.toLowerCase()
        .trim();

      if ("instant-search" === options.mode) {
        return (
          "search" === curation.type &&
          normalisedHandleOrSearchTerm === normalisedSearchTerm
        );
      }

      if ("search" === options.mode) {
        return (
          "search" === curation.type &&
          normalisedHandleOrSearchTerm === normalisedSearchTerm
        );
      }

      if ("collection" === options.mode) {
        return (
          "collection" === curation.type &&
          normalisedHandleOrSearchTerm === normalisedCollectionHandle
        );
      }

      return false;
    });

    const addGlobalBoosting = (curation: ConfigCuration) => {
      if (!globalCuration) {
        return curation;
      }

      const curationIsCollection = curation.type === "collection";
      if (!curationIsCollection) {
        return curation;
      }

      const curationHasRules =
        !!curation.boosting.groupings.length ||
        !!curation.boosting.sortings.length;

      if (curationHasRules) {
        return curation;
      }

      const curationWithGlobalBoosting = {
        ...curation,
        boosting: globalCuration.boosting,
      } as ConfigCuration;

      return curationWithGlobalBoosting;
    };

    return curation
      ? addGlobalBoosting(curation)
      : globalCuration
      ? globalCuration
      : undefined;
  }, [
    config,
    options.mode === "collection" ? options.collectionHandle : undefined,
    searchTerm,
  ]);

  React.useEffect(() => {
    debug.log("useCuration", "curation", curation);
  }, [curation]);

  return {
    curation: curation,
  };
};
