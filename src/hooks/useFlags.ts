import React from "react";
import { useReactifySearchContext } from "./useReactifySearchContext";
import { debug } from "../utility";

export enum Flags {
  SENSOR_SORT = "reactify-search:flag_sensor_sort",
  SENSOR_COLLECTION = "reactify-search:flag_sensor_collection",
  SENSOR_PUBLISHED = "reactify-search:flag_sensor_published",
  SENSOR_INVENTORYAVAILABLE = "reactify-search:flag_sensor_inventoryavailable",
  SENSOR_SEARCHTERM = "reactify-search:flag_sensor_searchterm",
  SENSOR_SEARCHTERM_VALUE = "reactify-search:flag_sensor_searchterm_value",
  SENSOR_SEARCHTERM_PHRASE = "reactify-search:flag_sensor_searchterm_phrase",
  SENSOR_SEARCHTERM_PHRASE_SYNONYMS = "reactify-search:flag_sensor_searchterm_phrase_synonyms",
  SENSOR_SEARCHTERM_PHRASE_PREFIX = "reactify-search:flag_sensor_searchterm_phrase_prefix",
  SENSOR_SEARCHTERM_PHRASE_PREFIX_SYNONYMS = "reactify-search:flag_sensor_searchterm_phrase_prefix_synonyms",
  SENSOR_SEARCHTERM_CROSS_FIELDS = "reactify-search:flag_sensor_searchterm_cross_fields",
  SENSOR_SEARCHTERM_CROSS_FIELDS_SYNONYMS = "reactify-search:flag_sensor_searchterm_cross_fields_synonyms",
  SENSOR_SEARCHTERM_SPAN_FIRST = "reactify-search:flag_sensor_searchterm_span_first",
  SENSOR_SEARCHTERM_SPAN_FIRST_SYNONYMS = "reactify-search:flag_sensor_searchterm_span_first_synonyms",
}

export const useFlags = () => {
  const { options } = useReactifySearchContext();
  const [delta, setDelta] = React.useState<number>(Date.now());

  const getFlag = React.useCallback(
    (flag: Flags): boolean => {
      const flagFromOptions = options.flags?.[flag];
      const flagFromSession =
        sessionStorage.getItem(flag) === "false" ? false : true;

      if (typeof flagFromOptions === "undefined") {
        return flagFromSession;
      }

      return flagFromOptions;
    },
    [delta]
  );

  const setFlag = React.useCallback(
    (flag: Flags, value: boolean) => {
      sessionStorage.setItem(flag, `${value}`);
      setDelta(Date.now());
    },
    [delta]
  );

  const flags = React.useMemo<Record<Flags, boolean>>(() => {
    return Object.values(Flags).reduce<Record<Flags, boolean>>(
      (acc, flag) => ({
        ...acc,
        [flag]: getFlag(flag),
      }),
      {} as Record<Flags, boolean>
    );
  }, [delta]);

  React.useEffect(() => {
    for (const flag of Object.values(Flags)) {
      setFlag(flag, getFlag(flag));
    }
  }, []);

  React.useEffect(() => {
    debug.log("useFlags", "flags", flags);
  }, [flags]);

  return {
    flags: flags,
    setFlag: setFlag,
    getFlag: getFlag,
  };
};
