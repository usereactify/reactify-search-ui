import React from "react";

import { Config } from "../types/config";
import { debug } from "../utility";

export const useConfig = (
  shopifyPermanentDomain: string,
  configId?: string
): {
  config: Config | undefined;
} => {
  // synchronously returns cached and non-expired config from session storage
  const cachedConfig = React.useMemo(() => {
    if (typeof window === "undefined") {
      return;
    }
    debug.log("useConfig", "checking config cache");

    const sessionConfig: {
      config: Config;
    } | null = JSON.parse(
      window.sessionStorage.getItem("reactify-search:config") ?? "null"
    );

    if (sessionConfig) {
      if (sessionConfig.config.cache?.enabled === false) {
        debug.log("useConfig", `cache disabled, ignoring cached config`);
        return;
      }

      debug.log("useConfig", `cache enabled, using cached config`);

      return sessionConfig.config;
    } else {
      debug.log("useConfig", `cached config missing`);
    }

    return;
  }, []);

  const [config, setConfig] = React.useState<Config | undefined>(cachedConfig);

  React.useEffect(() => {
    (async () => {
      debug.log("useConfig", "loading fresh config");
      const searchParams = new URLSearchParams();
      searchParams.set("shop", shopifyPermanentDomain);

      if (configId) {
        searchParams.set("id", configId);
      }

      const json = await fetch(
        `https://config.search.reactify.app/?${searchParams.toString()}`
      ).then((response) => response.json());

      setConfig(json.body);

      debug.log("useConfig", "loaded fresh config");

      window.sessionStorage.setItem(
        "reactify-search:config",
        JSON.stringify({
          config: json.body,
        })
      );
    })();
  }, [shopifyPermanentDomain, configId]);

  React.useEffect(() => {
    debug.log("useConfig", "config", config);
  }, [config]);

  return {
    config,
  };
};
