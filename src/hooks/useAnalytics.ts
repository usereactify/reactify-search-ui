import axios from "axios";

import pkg from "../../package.json";

import { useReactifySearchContext } from "../hooks";
import { debug } from "../utility";

function getCookie(name: string): string {
  try {
    const cookie = `; ${document.cookie}`.split(`; ${name}=`);
    if (cookie.length === 2) {
      return cookie.pop()?.split(";").shift() ?? "";
    }
  } catch (error) {
    console.error(error);
  }

  return "";
}

export const useAnalytics = () => {
  const { options } = useReactifySearchContext();
  const url = "https://analytics.search.reactify.app/record/";
  const track = async (event: TrackEvent) => {
    if (!options.shopifyPermanentDomain) {
      console.warn(
        new Error(
          'Unable to send tracking event, missing value for "shopifyPermanentDomain".'
        )
      );

      return;
    }

    debug.log("useAnalytics", "track", event);

    const events = getTrackEvents(event);
    return axios.post(
      url,
      { events },
      {
        headers: {
          "X-Reactify-Tenant": options.shopifyPermanentDomain,
          "X-Reactify-Client": getCookie("_rs_ga"),
          "X-Reactify-Version": pkg.version,
        },
      }
    );
  };

  return { track };
};

function getTrackEvents(event: TrackEvent): TrackEvent[] {
  const { eventName } = event;
  let events: TrackEvent[] = [];

  switch (eventName) {
    case "search":
      events = [
        {
          eventName,
          payload: {
            ...event.payload,
            searchTerm: event.payload.searchTerm.trim(),
          },
        },
      ];

      break;

    case "zeroResults":
      events = [
        {
          eventName,
          payload: {
            ...event.payload,
            searchTerm: event.payload.searchTerm.trim(),
          },
        },
      ];

      break;

    case "viewProduct":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "clickProduct":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "viewPromotion":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "clickPromotion":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "paginationChange":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "sortChange":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;

    case "filterChange":
      events = [
        {
          eventName,
          payload: event.payload,
        },
      ];

      break;
  }

  return events;
}

export type TrackEvent =
  | TrackEvent.SearchEvent
  | TrackEvent.ZeroResultsEvent
  | TrackEvent.ViewProductEvent
  | TrackEvent.ClickProductEvent
  | TrackEvent.ViewPromotionEvent
  | TrackEvent.ClickPromotionEvent
  | TrackEvent.PaginationChangeEvent
  | TrackEvent.SortChangeEvent
  | TrackEvent.FilterChangeEvent;

export namespace TrackEvent {
  export interface SearchEvent {
    eventName: "search";
    payload: SearchEvent.Payload;
  }

  export namespace SearchEvent {
    export interface Payload {
      searchTerm: string;
    }
  }

  export interface ZeroResultsEvent {
    eventName: "zeroResults";
    payload: ZeroResultsEvent.Payload;
  }

  export namespace ZeroResultsEvent {
    export interface Payload {
      searchTerm: string;
    }
  }

  export interface ViewProductEvent {
    eventName: "viewProduct";
    payload: ViewProductEvent.Payload;
  }

  export namespace ViewProductEvent {
    export interface Payload {
      elasticProduct: ElasticProduct;
    }
  }

  export interface ClickProductEvent {
    eventName: "clickProduct";
    payload: ClickProductEvent.Payload;
  }

  export namespace ClickProductEvent {
    export interface Payload {
      elasticProduct: ElasticProduct;
    }
  }

  export interface ViewPromotionEvent {
    eventName: "viewPromotion";
    payload: ViewPromotionEvent.Payload;
  }

  export namespace ViewPromotionEvent {
    export interface Payload {
      link: string;
      title: string;
    }
  }

  export interface ClickPromotionEvent {
    eventName: "clickPromotion";
    payload: ClickPromotionEvent.Payload;
  }

  export namespace ClickPromotionEvent {
    export interface Payload {
      link: string;
      title: string;
    }
  }

  export interface PaginationChangeEvent {
    eventName: "paginationChange";
    payload: PaginationChangeEvent.Payload;
  }

  export namespace PaginationChangeEvent {
    export interface Payload {
      page: number;
      source: "search" | "collection";
    }
  }

  export interface SortChangeEvent {
    eventName: "sortChange";
    payload: SortChangeEvent.Payload;
  }

  export namespace SortChangeEvent {
    export interface Payload {
      type: string;
    }
  }

  export interface FilterChangeEvent {
    eventName: "filterChange";
    payload: FilterChangeEvent.Payload;
  }

  export namespace FilterChangeEvent {
    export interface Payload {
      name: string;
      value: string;
    }
  }

  export interface ElasticProduct {
    id: number;
    title: string;
  }
}
