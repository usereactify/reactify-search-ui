// @todo this is so we don't need to import firebase
type Timestamp = string;

import { ShopifyProduct } from "./shopify";
import { ElasticDataType } from "./elastic";

export interface Log {
  id: string;
  keywords: string[];
  message: string;
  platform: "reactify" | "shopify" | "elastic";
  percentCompleted: number;
  action?: {
    type: "retry";
    label: string;
    task: Task["taskId"];
  };
  step:
    | "download"
    | "transform"
    | "merge"
    | "upload"
    | "cleanup"
    | "complete"
    | "sync";
  title: string;
  type: "bulk";
  data?: string; // JSON sringified data
  progress: "new" | "error" | "complete" | "partiallyComplete";
  status: "success" | "info" | "warning" | "critical"; // polaris banner status
  createdAt: Timestamp;
  updatedAt?: Timestamp;
  finishedAt?: Timestamp;
}

export interface Task {
  logId: string;
  taskId:
    | "download"
    | "transform"
    | "merge"
    | "upload"
    | "cleanup"
    | "refreshDefinedFields"
    | "install";
  firstRun: boolean;
  createdAt: Timestamp;
  updatedAt?: Timestamp;
}

export interface TaskDownload extends Task {
  taskId: "download";
  type: "collection" | "product";
  productUrl?: string;
  collectionUrl?: string;
}

export interface TaskTransform extends Task {
  taskId: "transform";
  productUrl: string;
  collectionUrl: string;
}

export interface TaskMerge extends Task {
  taskId: "merge";
  productCount: number;
  collectionCount: number;
  startAfterId?: string;
}

export interface TaskUpload extends Task {
  taskId: "upload";
  productCount: number;
  collectionCount: number;
  startAfterId?: string;
}

export interface TaskCleanup extends Task {
  taskId: "cleanup";
}

export interface TaskRefreshDefinedFields extends Task {
  taskId: "refreshDefinedFields";
}

export interface TaskInstall extends Task {
  taskId: "install";
}

export interface Lock {
  logId: string;
  taskType: "sync";
  createdAt: Timestamp;
}

export interface SettingsInstance {
  index: string;
  cloudId: string;
  endpoint: string;
  password: string;
  username: string;
  syncFrequency: string;
  syncThreshold: number;
  provider: "elastic" | "aws";
  storefrontAccessToken: string;
  customMappings: string; // JSON sringified data
}

export interface SettingsProduct {
  tagChild?: string;
  tagKeys?: string[];
  tagParent?: string;
  syncObjects: string[];
  tagExclude?: string[];
  tagSeparator?: string;
  metafieldWhitelist?: string[];
}

export interface SettingsCurations {
  moreInfo?: boolean;
  pageSize?: number;
  gridColumns?: number;
}

export interface Field {
  id: string;
  field: string;
  enabled: boolean;
  importance: number;
  searchType: "always_search" | "search_page" | "instant_search";
}

export interface DefinedField {
  label: string;
  value: string;
  type: ElasticDataType;
}

export interface Sort {
  id: string;
  name: string;
  handle: string;
  field: string;
  position: number;
  enabled: boolean;
  direction: "desc" | "asc";
  visibility: "all" | "search" | "collection";
}

export interface Filter {
  id: string;
  name: string;
  handle: string;
  enabled: boolean;
  pageSize: number;
  keywords: string[];
  defaultSort: string;
  collections: string[];
  type: "search" | "collection";
  paginationType: "pagination" | "load_more" | "next_prev" | "infinite_scroll";
  inventoryVisibility:
    | "show_all"
    | "hide_products"
    | "hide_variants"
    | "hide_all";
}

export interface FilterOption {
  customSortOrder?: string;
  displaySize: string;
  displaySliderInterval: string;
  displaySliderStep: string;
  displaySliderPrefix?: string;
  displaySliderSuffix?: string;
  displayRangeOptions?: Array<string>; // formatted like "<label>:<start>:<end>"
  displayType: "multi" | "single" | "range" | "slider";
  displayView: "list" | "check" | "swatch" | "range" | "box";
  enabled: boolean;
  field: string;
  id: string;
  name: string;
  handle: string;
  position: number;
  settingsCollapsedDesktop: boolean;
  settingsCollapsedMobile: boolean;
  settingsShowFilter: boolean;
  settingsShowMore: boolean;
  settingsShowSearch: boolean;
  settingsShowEmptyValues: boolean;
  settingsUppercase: boolean;
  settingsHideUnavailable: boolean;
  settingsFilterLogic: "and" | "or";
  valuesExclude: Array<string>;
  valuesManual: Array<string>;
  valuesShow: "all" | "manual";
}

export interface Redirect {
  id: string;
  url: string;
  query: string;
  enabled: boolean;
  keywords: string[];
}

export interface Curation {
  id: string;
  title: string;
  status: CurationStatus;
  published: boolean;
  keywords: string[];
  searchTerm?: string;
  longRunningTask?: boolean;
  collectionHandle?: string;
  type: "collection" | "search";
  boosting?: {
    groupings?: BoostGrouping[];
    sortings?: BoostSorting[];
  };
  sort?: CurationSort;
}

export interface CurationSort {
  sortTag?: string;
  sortOption: string;
  sortDirection: "asc" | "desc";
  sortVariantOption?: "option1" | "option2" | "option3";
}

export enum CurationStatus {
  Draft = "draft",
  Publishing = "publishing",
  Published = "published",
  Unpublishing = "unpublishing",
  Resetting = "resetting",
  Pinning = "pinning",
  Sorting = "sorting",
}

export interface CurationHiding {
  productId: number;
  productHandle: string;
}

export interface BoostGrouping {
  key: string;
  value: string;
  field: string;
  position: number;
  operation: string;
}

export interface BoostSorting {
  key: string;
  field: string;
  query?: string;
  position: number;
  direction: "asc" | "desc";
}

export type Pin = PinProduct | PinCallout;

export enum PinType {
  Product = "product",
  Callout = "callout",
}

export interface PinProduct {
  key: string;
  type: PinType.Product;
  position: number;
  productId: number;
  productHandle: string;
}

export interface PinCallout {
  key: string;
  type: PinType.Callout;
  position: number;
  calloutId: string;
}

export interface Callout {
  id: string;
  link: string;
  title: string;
  content: string;
  enabled: boolean;
  textColor: string;
  keywords: string[];
  mobileImage: string;
  mobileVideo: string;
  desktopVideo: string;
  desktopImage: string;
  backgroundColor: string;
  visibility: "desktop" | "mobile" | "all";
  displayColumns: string; // @todo this should be a number
  displayRows: string; // @todo this should be a number
  selectedDates?: {
    end: Timestamp;
    start: Timestamp;
  };
}

export interface Synonym {
  id: string;
  name: string;
  enabled: boolean;
  keywords: string[];
  synonyms: string[];
  type: "group" | "oneway";
}

export interface MergedValue {
  id: string;
  name: string;
  field: string;
  keywords: string[];
  values: string[];
}

export interface Collection {
  title: string;
  handle: string;
  shopifyId: string;
  productIds: string[];
  storefrontId: string;
  legacyResourceId: number;
}

// product is the shopify product with edges and nodes normalised
export type Product = Omit<
  ShopifyProduct,
  "variants" | "images" | "media" | "metafields"
> & {
  variants: (Omit<
    ShopifyProduct["variants"]["edges"][0]["node"],
    "presentmentPrices"
  > & {
    images?: ShopifyProduct["images"]["edges"][0]["node"][]; // from child product if this variant is from a child
    presentmentPrices: ShopifyProduct["variants"]["edges"][0]["node"]["presentmentPrices"]["edges"][0]["node"][];
  })[];
  images: ShopifyProduct["images"]["edges"][0]["node"][];
  media: ShopifyProduct["media"]["edges"][0]["node"][];
  metafields: ShopifyProduct["metafields"]["edges"][0]["node"][];
};
