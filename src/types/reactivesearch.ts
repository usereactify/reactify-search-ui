import { ElasticDocument, ElasticHit } from "./elastic";

// props passed to SingleList/MultiList.render
export interface ReactivesearchFilterListProps {
  loading: boolean;
  error: Error | null;
  handleChange: (key: string) => void;
  data: { doc_count: number; key: string }[];
  value: string | { [key: string]: boolean };
}

// props passed to ReactiveList.renderPagination
export interface ReactivesearchPaginationProps {
  pages: number;
  showEndPage: false;
  totalPages: number;
  currentPage: number;
  setPage: (page: number) => void;
}

// props passed to ReactiveList.render
// @todo complete missing types
export interface ReactivesearchResultProps {
  error?: ReactivesearchError;
  loading: boolean;
  data: Array<Omit<ElasticHit, "_source"> & ElasticDocument>;
  loadMore: () => void;
  customData: unknown;
  promotedData: unknown;
  aggregationData: unknown;
  rawData?: {
    _shards: {
      failed: number;
      skipped: number;
      successful: number;
      total: number;
    };
    took: number;
    status: number;
    timed_out: boolean;
    hits: ElasticHit[];
  };
  resultStats: {
    time: number;
    hidden: number;
    promoted: number;
    currentPage: number;
    numberOfPages: number;
    numberOfResults?: number;
    displayedResults: number;
  };
  streamData: unknown;
  triggerAnalytics: unknown;
  triggerClickAnalytics: unknown;
}

// state for the "page" component returned by StateProvider when given the keys: ["isLoading", "hits", "resultStats", "error"]
export interface ReactivesearchSearchStatePage {
  isLoading: boolean;
  hits: {
    hidden: number;
    time: number;
    total: number;
    hits: ElasticHit[];
  };
  resultStats: {
    hidden: number;
    numberOfResults?: number;
    promoted: number;
    time: number;
  };
  error?: ReactivesearchError;
}

export interface ReactivesearchError {
  status: number;
  statusText: string;
}
