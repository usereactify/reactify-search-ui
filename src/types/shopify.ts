import { ProductQuery, BulkQueryProductsQuery } from "./graphql";

// these are the types from the single "product" query, and is what we store in firestore
export type ShopifyProduct = NonNullable<ProductQuery["product"]>;
export type ShopifyVariant = ShopifyProduct["variants"]["edges"][0]["node"];

// these are the types from the bulk "product" query, which should match the above, but we
// always type on the single query in firestore
export type BulkShopifyProduct =
  BulkQueryProductsQuery["products"]["edges"][0]["node"];
export type BulkShopifyVariant =
  BulkShopifyProduct["variants"]["edges"][0]["node"];

// these are types for the individual "export lines" which shopify exports in the bulk export file
export type ShopifyExportProduct = Omit<
  BulkShopifyProduct,
  "variants" | "images" | "media" | "metafields"
>;
export type ShopifyExportProductVariant = Omit<
  BulkShopifyVariant,
  "presentmentPrices"
>;
export type ShopifyExportPresentmentPrice =
  BulkShopifyVariant["presentmentPrices"]["edges"][0]["node"];
export type ShopifyExportImage =
  BulkShopifyProduct["images"]["edges"][0]["node"];
export type ShopifyExportMedia =
  BulkShopifyProduct["media"]["edges"][0]["node"];
export type ShopifyExportMetafield =
  BulkShopifyProduct["metafields"]["edges"][0]["node"];

export interface ShopifyExportLine {
  __parentId?: string;
  __typename: string;
}

export function isProduct(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportProduct {
  return "Product" === exportLine.__typename;
}

export function isProductVariant(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportProductVariant {
  return "ProductVariant" === exportLine.__typename;
}

export function isPresentmentPrice(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportPresentmentPrice {
  return "ProductVariantPricePair" === exportLine.__typename;
}

export function isImage(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportImage {
  return "Image" === exportLine.__typename;
}

export function isMedia(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportMedia {
  return ["Video", "Model3d", "MediaImage", "ExternalVideo"].includes(
    exportLine.__typename
  );
}

export function isMetafield(
  exportLine: ShopifyExportLine
): exportLine is ShopifyExportMetafield {
  return "Metafield" === exportLine.__typename;
}
