import {
  Sort,
  Filter,
  FilterOption,
  Redirect,
  Field,
  Curation,
} from "./firestore";

// when exposed as config, most resources have the `enabled` and `keywords` attributes removed
// curation boosting groupings and sortings both have a json encoded elastic query added to them
export interface Config {
  index: string;
  endpoint: string;
  version: string;
  cache: {
    enabled: boolean;
    seconds: number;
  };
  sort: Array<ConfigSort>;
  fields: Array<ConfigField>;
  filters: Array<ConfigFilter>;
  redirects: Array<ConfigRedirect>;
  curations: Array<ConfigCuration>;
}

export type ConfigSort = Omit<Sort, "enabled">;
export type ConfigField = Omit<Field, "enabled">;
export type ConfigRedirect = Omit<Redirect, "enabled" | "keywords">;
export type ConfigFilter = Omit<Filter, "enabled" | "keywords"> & {
  options: Omit<FilterOption, "enabled">[];
};
export type ConfigFilterOption = Omit<FilterOption, "enabled">;
export type ConfigCuration = Omit<
  Curation,
  "keywords" | "boosting" | "longRunningTask" | "callouts"
> & {
  boosting: {
    groupings: (NonNullable<
      NonNullable<Curation["boosting"]>["groupings"]
    >[0] & { query: string })[];
    sortings: (NonNullable<NonNullable<Curation["boosting"]>["sortings"]>[0] & {
      query: string;
    })[];
  };
};
