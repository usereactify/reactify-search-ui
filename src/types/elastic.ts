import { Callout } from "./firestore";
import {
  Maybe,
  ProductVariantInventoryPolicy,
  MetafieldValueType,
} from "./graphql";

export enum ElasticDocumentType {
  Callout = "callout",
  Product = "product",
}

export type ElasticDocument = ElasticCallout | ElasticProduct;

export interface ElasticCallout {
  type: ElasticDocumentType.Callout;
  key: string;
  callout: Omit<Callout, "keywords">;
  /** elastic callouts are always published */
  published: true;
  /** elastic callouts are always attached to one curation */
  curations: [ElasticCuration];
}

export interface ElasticProduct {
  type: ElasticDocumentType.Product;
  /** the log id from which this product was last uploaded */
  lastLogId: string;
  /** legacy resource id */
  id: number;
  storefrontId: string;
  title: string;
  description: string;
  vendor: string;
  product_type: string;
  handle: string;
  url: string;
  tags: Array<string>;
  collection_titles: Array<string>;
  /** tag prefixes defined in tagKeys are split to their own attributes */
  [key: `tags_${string}`]: Array<string> | undefined;
  /** decimal number e.g. 99.95 */
  price_min: number;
  /** decimal number e.g. 99.95 */
  price_max: number;
  /** array of option names e.g. ["Colour", "Size"] */
  options: Array<string>;
  /** @todo it would be better to use ProductStatus directly, which has uppercase */
  status: "active" | "archived" | "draft";
  published: boolean;
  /** ISO 8601 datetime */
  published_at: Maybe<string>;
  /** ISO 8601 datetime */
  updated_at: string;
  /** ISO 8601 datetime */
  created_at: string;
  tracks_inventory: boolean;
  /** originalSrc URL for featured image */
  image: Maybe<string>;
  /** array of images (undefined if images are disabled in sync settings) */
  images?: Array<ElasticImage>;
  /** array of variants attached to product (undefined if variants are disabled in sync settings) */
  variants?: Array<ElasticVariant>;
  /** price ranges from presentment prices (undefined if presentment prices or variants are disabled in sync settings) */
  presentment_price_ranges?: {
    min_variant_price: Array<ElasticPresentmentPrice>;
    max_variant_price: Array<ElasticPresentmentPrice>;
  };
  /** array of variant skus (undefined if variants are disabled in sync settings) */
  variant_skus?: Array<string>;
  /** combined array of option values from all option types (undefined if variants are disabled in sync settings) */
  variant_options?: Array<string>;
  /** array of collections which this product belongs to (undefined if collections are disabled in sync settings) */
  collections?: Array<ElasticCollection>;
  /** array of whitelisted metafields (undefined if metafields are disabled in sync settings) */
  metafields?: Array<ElasticMetafield>;
  /** decimal number e.g. 99.95 (undefined if variants are disabled in sync settings) */
  discount_amount?: number;
  /** combined inventory for all variants (undefined if variants are disabled in sync settings) */
  inventory_total?: number;
  /** number of days since published, null if unpublished */
  published_days: Maybe<number>;
  /** collection of curations that this product is attached to */
  curations?: Array<ElasticCuration>;
  /** related products **/
  related?: Array<ElasticProductRelated>;
}

export interface ElasticProductRelated {
  createdAt: string;
  description: string;
  featuredImage: {
    originalSrc: "https://cdn.shopify.com/s/files/1/0614/3977/0777/p…71d6-e7fd-4f5f-aa99-c722e2832d01.jpg?v=1660776345";
  };
  handle: string;
  id: string;
  images: Array<ElasticImage>;
  legacyResourceId: string;
  metafields?: Array<ElasticMetafield>;
  options: Array<{
    name: string;
  }>;
  priceRangeV2: {
    maxVariantPrice: {
      amount: string;
    };
    minVariantPrice: {
      amount: string;
    };
  };
  productType: string;
  publishedAt: string;
  status: "ACTIVE" | "ARCHIVED" | "DRAFT";
  storefrontId: string;
  tags: Array<string>;
  title: string;
  tracksInventory: boolean;
  updatedAt: string;
  variants: Array<ElasticVariant>;
  vendor: string;
  __typename: "Product";
}

export interface ElasticVariant {
  /** legacy resource id */
  id: number;
  storefrontId: string;
  title: string;
  sku: Maybe<string>;
  barcode: Maybe<string>;
  /** array of presentment prices (undefined if presentment prices are disabled in sync settings) */
  presentment_prices?: Array<{
    price: ElasticPresentmentPrice;
    compare_at_price: Maybe<ElasticPresentmentPrice>;
  }>;
  /** decimal number e.g. 99.95 */
  price: number;
  /** decimal number e.g. 99.95 */
  compare_at_price: Maybe<number>;
  /** value string for option 1 e.g. Blue */
  option1: Maybe<string>;
  /** value string for option 2 e.g. Medium */
  option2: Maybe<string>;
  /** value string for option 3 */
  option3: Maybe<string>;
  /** sort position within the product */
  position: number;
  inventory_policy: ProductVariantInventoryPolicy;
  inventory_quantity: number;
  available: boolean;
  /** array of images (only available if this variant has been merged from another product and if images are enabled in sync settings) */
  images?: Array<ElasticImage>;
}

export interface ElasticCollection {
  /** legacy resource id */
  id: number;
  storefrontId: string;
  handle: string;
  title: string;
  position: number;
}

export interface ElasticMetafield {
  key: string;
  type: string;
  value: string;
  namespace: string;
  /**
   * Legacy valueType attribute which is no longer provided by Shopify. We still
   * populate this field to prevent any frontends breaking which rely on it.
   *
   * @see https://shopify.dev/apps/metafields/definitions/types
   */
  value_type: MetafieldValueType;
}

export interface ElasticPresentmentPrice {
  /** decimal number e.g. 99.95 */
  amount: number;
  currency_code: string;
}

export interface ElasticImage {
  /** originalSrc URL */
  src: string;
  /** alt text */
  alt: Maybe<string>;
}

export interface ElasticCuration {
  id: string;
  hidden: boolean;
  position?: number;
  searchTerm?: string;
  collectionHandle?: string;
}

// non-exhaustive type for bulk request
// https://www.elastic.co/guide/en/elasticsearch/reference/7.x/docs-bulk.html
export type ElasticBulkOperationUpdate = [
  { update: { _index: string; _id: string } },
  {
    script?: {
      lang: "painless";
      source: string;
      params?: Record<string, any>;
    };
    upsert?: Partial<ElasticProduct>;
    scripted_upsert?: true;
    doc?: Partial<ElasticProduct>;
    doc_as_upsert?: true;
  }
];

export type ElasticBulkOperationIndex = [
  { index: { _index: string; _id: string } },
  ElasticDocument
];

export type ElasticBulkOperation =
  | ElasticBulkOperationUpdate
  | ElasticBulkOperationIndex;

export type ElasticSearchResult<T = ElasticDocument> = {
  hits: {
    hits: Array<ElasticHit<T>>;
  };
};

export type ElasticHit<T = ElasticDocument> = {
  _id: string;
  _source: T;
  _score: number | null;
};

export enum ElasticDataType {
  Text = "text",
  Long = "long",
  Date = "date",
  Float = "float",
  Nested = "nested",
  Keyword = "keyword",
  Boolean = "boolean",
}

export interface ElasticProperty {
  analyzer?: string;
  type: ElasticDataType;
  ignore_above?: number;
  fields?: { [key: string]: ElasticField };
  properties?: { [key: string]: ElasticProperty };
}

export interface ElasticField {
  type: ElasticDataType;
  ignore_above?: number;
}
