import React from "react";

import { Flags, ReactifySearchProvider, Sensors } from "../src";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    source: {
      language: "tsx",
      format: "typescript",
      dark: false,
    },
  },
  previewTabs: {
    canvas: {
      hidden: true,
    },
  },
  viewMode: "docs",
};

export const decorators = [
  (Story) => (
    <ReactifySearchProvider
      mode="search"
      shopifyPermanentDomain="usereactify-demo.myshopify.com"
      flags={{
        [Flags.SENSOR_SEARCHTERM]: true,
      }}
    >
      <Story />
    </ReactifySearchProvider>
  ),
];
