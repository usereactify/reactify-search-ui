export default {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
  babel: async (options: any) => ({
    ...options,
    plugins: ["@babel/plugin-transform-typescript", ...options.plugins],
  }),
};
