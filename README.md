# Reactify Search UI

This library provides React components and hooks for building UI's for the [Reactify Search](https://bitbucket.org/usereactify/reactify-search) app.

The aim of this library is not to build a search and filter interface from the ground up. The aim of this library is to abstract the complexities of Reactify Search, whilst providing the developer a wide range of approaches and escape hatches to build in the way they want to. To do this, we wrap a mature Elasticsearch UI library called [Reactivesearch](https://github.com/appbaseio/reactivesearch) (similar name, but completely unaffiliated with Reactify Search).

## Documentation

All end-user documentation is available at https://search.docs.reactify.com.au which is powered by GitBooks.

## Contributing

```sh
# install dependencies
npm install

# watch and build the package
npm run watch

# serve storybook to interact with components
npm run storybook

# release a version
npm run release

# release a beta version
npm run release:beta
```
